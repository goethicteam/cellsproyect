'use strict';

(function () {
  Polymer({
    is: 'link-example',
    _goToAnother: function _goToAnother() {
      this.dispatchEvent(new CustomEvent('param-url-event', {
        bubbles: true,
        composed: true,
        detail: { 'idToSend': 12345 }
      }));
    },
    _goToRouterExample: function _goToRouterExample() {
      this.dispatchEvent(new CustomEvent('router-event', {
        bubbles: true,
        composed: true,
        detail: { 'prettyUrlToSend': 'user-pretty-url', 'queryString': 1234, 'otherQueryString': 5678 }
      }));
    }
  });
})();