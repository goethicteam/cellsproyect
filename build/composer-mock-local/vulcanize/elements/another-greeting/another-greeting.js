'use strict';

(function () {
  Polymer({
    is: 'another-greeting',
    properties: {
      anotherGreeting: {
        type: String
      }
    }
  });
})();