'use strict';

(function () {
  Polymer({
    is: 'my-greeting',

    properties: {
      greeting: {
        type: String,
        value: 'Bienvenido',
        notify: true
      }
    }

  });
})();