'use strict';

(function () {
  Polymer({
    is: 'router-example',
    properties: {

      propertyQueryStringToPrettyUrl: {
        type: String
      },
      propertyQueryStringForUseInRouterPage: {
        type: Number
      },
      propertyOtherqueryStringForUseInRouterPage: {
        type: Number
      },
      user: {
        type: Array,
        value: {
          "firstName": 'María Rodriguez Gutierrez'
        }
      }
    },
    toggle: function toggle() {
      this.$.men.togglePanel();
    },
    _goToHome: function _goToHome() {
      this.fire('home-event');
    }
  });
})();