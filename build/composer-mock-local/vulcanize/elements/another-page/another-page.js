'use strict';

(function () {
  Polymer({
    is: 'another-page',
    properties: {

      propertyForUseId: {
        type: Number
      }, /**
         * Collection of icons that appear in the background
         */
      coronitaIcons: {
        type: Array,
        observer: '_initializeRandomIcons',
        value: ['coronita:euro', 'coronita:dollar', 'coronita:creditcard', 'coronita:digitalcard', 'coronita:mobilecashdollar', 'coronita:wallet', 'coronita:mobilecasheuro', 'coronita:place', 'coronita:exploration', 'coronita:nearme', 'coronita:search', 'coronita:graphics', 'coronita:mobile', 'coronita:desktop', 'coronita:calculator', 'coronita:support', 'coronita:videoplayline', 'coronita:communicationpublic', 'coronita:weather', 'coronita:entretainment', 'coronita:shopping', 'coronita:sales', 'coronita:discount', 'coronita:promotion', 'coronita:supermarket', 'coronita:shop', 'coronita:florist', 'coronita:coffeshop', 'coronita:restaurant', 'coronita:retirement']
      },

      /**
       * Collection of random icons that appear in the background
       */
      randomIcons: Array
    },

    /**
     * Fire event to go to the next view
     */

    _nextView: function _nextView() {
      this.fire('next-view', true);
    },

    _getRandomRoundNum: function _getRandomRoundNum(min, max) {
      return Math.floor(this._getRandomNum(min, max));
    },

    _getRandomEvenNum: function _getRandomEvenNum(min, max) {
      var rand = this._getRandomRoundNum(min, max);
      return rand % 2 === 0 ? rand : rand - 1;
    },

    _getRandomNum: function _getRandomNum(min, max) {
      return Math.random() * (max - min) + min;
    },

    _initializeRandomIcons: function _initializeRandomIcons() {
      var maxNumIcons = 50;
      var aux = [];
      var randomNum = void 0;
      var randomSize = void 0;
      var x = void 0;
      var y = void 0;
      var randomDelay = void 0;
      for (var i = 0; i < maxNumIcons; i++) {
        randomNum = this._getRandomRoundNum(0, this.coronitaIcons.length);

        randomSize = this._getRandomEvenNum(8, 34);
        x = this._getRandomNum(0, this.clientWidth);

        y = this._getRandomNum(0, this.clientHeight);
        randomDelay = this._getRandomNum(0, 2);
        aux[i] = {
          'icon': this.coronitaIcons[randomNum],
          'size': randomSize.toString(),
          'xCoord': x.toString(),
          'yCoord': y.toString(),
          'delay': randomDelay.toString()
        };
      }
      this.set('randomIcons', aux);
    },

    _goToHome: function _goToHome() {
      this.fire('home-event');
    }

  });
})();