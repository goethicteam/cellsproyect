'use strict';

Polymer({
  is: 'example-data-manager',
  properties: {
    users: {
      type: Array,
      notify: true,
      value: function value() {
        return {
          'userId': 1,
          'id': 1,
          'title': 'Payload enviado por el DM'
        };
      }
    }
  }
});