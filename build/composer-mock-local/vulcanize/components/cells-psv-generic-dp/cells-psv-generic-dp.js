'use strict';

var _typeof = typeof Symbol === "function" && typeof Symbol.iterator === "symbol" ? function (obj) { return typeof obj; } : function (obj) { return obj && typeof Symbol === "function" && obj.constructor === Symbol && obj !== Symbol.prototype ? "symbol" : typeof obj; };

(function () {
  'use strict';

  Polymer({

    is: 'cells-psv-generic-dp',

    behaviors: [CellsBehaviors.CellsAjaxBehavior],

    properties: {

      /**
       * Service endpoint path.
       * @type  {String}
       */
      path: {
        observer: '_pathChanged',
        type: String
      },

      /**
       * Query params to be sent into URI request.
       * @type  {Object}
       */
      queryParams: {
        type: Object
      },

      /**
       * HTTP Request Method<br/>https://www.w3.org/Protocols/rfc2616/rfc2616-sec9.html
       * @type {String}
       */
      method: {
        type: String,
        value: 'GET'
      },

      /**
       * Request payload to be sent.
       * @type  {Object}
       */
      body: {
        type: Object
      },

      /**
       * Headers to be sent as part of XMLHttpRequest
       * @type  {Object}
       */
      headers: {
        type: Object,
        value: function value() {
          return null;
        }
      },

      /**
       * Request headers map. Override from <em>cells-ajax-behavior</em>.
       * @property  _headers
       * @type      {Object}
       * @private
       * @override
       */
      _headers: {
        type: Object,
        value: function value() {
          return {
            'Accept': 'application/json, text/javascript, */*; q=0.01',
            'Content-Type': 'application/x-www-form-urlencoded'
          };
        }
      },

      /**
       * Override from <em>cells-ajax-behavior</em>
       * @type      {String}
       * @override
       * @private
       */
      _serviceURL: {
        type: String
      },

      /**
       * Stores the latest AJAX XHR response value.
       * @type  {Object}
       * @private
       */
      _lastResponse: {
        type: Object,
        value: null
      }
    },

    /**
     * Observer callback for <em>path</em> property.
     * Will store {newPath} into <em>_serviceURL</em>.
     *
     * @param   newPath {String} New service endpoint path.
     * @private
     */
    _pathChanged: function _pathChanged(newPath) {
      if (newPath) {
        this.set('_serviceURL', newPath);
      }
    },

    /**
     * Transforms <em>queryParams</em> value into {Object}
     * @private
     */
    _parseRequestParams: function _parseRequestParams() {
      var _params = this.get('queryParams') || {};
      if (typeof _params === 'string') {
        try {
          _params = JSON.parse(_params);
        } catch (e) {
          _params = null;
        }
      }
      return _params;
    },

    /**
     * Adds tsec header to current <em>headers</em>.
     * Will also merge <em>_headers</em>.
     *
     * @returns {Object}
     * @private
     */
    _parseRequestHeaders: function _parseRequestHeaders() {
      var headers = this.get('headers') || {};
      if (typeof headers === 'string' && headers.charAt(0) === '{') {
        try {
          headers = JSON.parse(headers);
        } catch (e) {
          headers = {};
        }
      }
      var _headers = this.get('_headers') || {};
      headers = Object.assign(_headers, headers);
      this.set('headers', headers);
      var tsec = sessionStorage.getItem('tsec');
      if (tsec) {
        headers.tsec = tsec;
      }
      return headers;
    },

    /**
     * If is a POST request, transforms body payload into {String} format.
     *
     * @returns {*}
     * @private
     */
    _parseRequestBody: function _parseRequestBody() {
      var body = this.body || {};

      var contentType = this.headers && this.headers['Content-Type'].split(';')[0];
      if (contentType && typeof contentType === 'string') {
        switch (contentType) {
          case 'text/plain':
            body = this.body;
            if (typeof body !== 'string') {
              try {
                if ((typeof body === 'undefined' ? 'undefined' : _typeof(body)) === 'object') {
                  body = JSON.stringify(body);
                } else {
                  body = body.toString();
                }
              } catch (error) {
                /* istanbul ignore next */
                body = null;
              }
            }
            break;
          case 'application/json':
            if (_typeof(this.body) === 'object') {
              body = JSON.stringify(this.body);
            } else {
              try {
                body = JSON.parse(this.body);
              } catch (error) {
                body = null;
              } finally {
                if (body !== null) {
                  body = this.body;
                }
              }
            }
            break;
          default:
            body = Object.keys(this.body).map(function (key) {
              return key + '=' + this.body[key];
            }, this).join('&');
        }
      }
      return body;
    },

    /**
     * Returns request properties to apply into <em>cells-ajax</em>. Overrides <em>cells-ajax-behavior</em>.
     * @override
     * @returns {{method, headers: (*|Object), params: *, body: *}}
     * @private
     */
    _getRequestProperties: function _getRequestProperties() {
      return {
        method: this.method.toUpperCase(),
        headers: this._parseRequestHeaders(),
        params: this._parseRequestParams(),
        body: this._parseRequestBody(),
        success: this._setSuccessResponse.bind(this),
        error: this._setErrorResponse.bind(this)
      };
    },

    /**
     * Callback for successful request.
     * @param request {Object} Peformed request.
     */
    _setSuccessResponse: function _setSuccessResponse(request) {
      this._saveLastResponse(request);
    },

    /**
     * Callback for failed requests.
     * @param error {Event} Fired error event.
     */
    _setErrorResponse: function _setErrorResponse(error) {
      this._saveLastResponse(error);
    },

    /**
     * Sets the value of <em>_lastResponse</em> to latest AJAX XHR response value.
     * @param   value {*} Latest AJAX XHR response
     * @private
     */
    _saveLastResponse: function _saveLastResponse(value) {
      var lastResponse;
      if (value) {
        if (value instanceof HTMLElement && value.nodeName === 'IRON-REQUEST') {
          value = value.xhr;
        }
        if (value instanceof Event) {
          value = value.srcElement;
        }
        if (value instanceof XMLHttpRequest) {
          lastResponse = value.response;
        }
      }
      if (!lastResponse) {
        lastResponse = this.cellsAjax && this.cellsAjax.lastRequest && this.cellsAjax.lastRequest.xhr && this.cellsAjax.lastRequest.xhr.response;
      }
      if (lastResponse) {
        this.set('_lastResponse', lastResponse);
      }
    },

    /**
     * Returns the value of latest AJAX XHR response.
     * @returns {*}
     */
    getLastResponse: function getLastResponse() {
      return this._lastResponse;
    }

  });
})();