
document.addEventListener('WebComponentsReady', function() {
  var myTemplate = this.querySelectorAll('cells-template-paper-header-panel');
  var paperTabs = document.querySelector('paper-tabs');
  var contentTabs = document.querySelectorAll('div[data-tab]');

  for (var i = 0; i < myTemplate.length; i++) {
    myTemplate[i].state = 'active';
  }

  paperTabs.addEventListener('iron-select', function(e) {
    var tab = e.currentTarget.selected;
    [].forEach.call(contentTabs, function(contentTab) {
      contentTab.classList.toggle('hidden', contentTab.dataset.tab !== tab);
    });
  });
});
