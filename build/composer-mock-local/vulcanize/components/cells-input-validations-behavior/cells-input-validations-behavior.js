'use strict';

window.CellsBehaviors = window.CellsBehaviors || {};
/**
 * `Cells.InputValidationsBehavior` is a behavior to validate
 * and format a valid input user, password login field
 *
 * @polymerBehavior Cells.InputValidationsBehavior
 * @demo demo/index.html
 */
window.CellsBehaviors.InputValidationsBehavior = {
  properties: {
    /**
     * Regex pattern for each validation type.
     */
    regexps: {
      type: Object,
      value: {
        rut: '\\d{3}.\\d{3}.\\d{3}-k|\\d{3}.\\d{3}.\\d{3}-\\d{1}|\\d{9}?k|\\d{10}',
        mxPhone: '\\d{2}\\s\\d{8}|\\d{10}'
      }
    },
    /**
     * Translation keys for each validation type.
     */
    messages: {
      type: Object,
      value: {
        rutMsg: 'cells-rut-error-message',
        mxPhoneMsg: 'cells-mx-phone-error-message'
      }
    },
    /**
     * Mask formats for each validation type.
     */
    masks: {
      type: Object,
      value: {
        rut: ['', '', '', '.', '', '', '', '.', '', '', '', '-', ''],
        mxPhone: ['', '', ' ', '', '', '', '', '', '', '', ''],
        peAccountNumber: ['', '', '', '', '-', '', '', '', '', '-', '', '', '', '', '', '', '', '', '', '']
      }
    }
  },

  _getPattern: function _getPattern(value) {
    return this.regexps[value] || value || false;
  },

  //TODO: do it in credentials form ?
  _getErrorMessages: function _getErrorMessages(value) {
    return this.t(this.messages[value] || value) || '';
  },

  _getMasked: function _getMasked(value, type, maskEnabled) {
    if (value && type && maskEnabled) {
      var pattern = this.masks[type];
      var aux = value.split('').slice(0, pattern.length);
      var mask = [];
      var acc = 0;
      aux.some(function (item) {
        if (!pattern[acc]) {
          mask.push(item);
        } else {
          if (pattern[acc] !== item) {
            // copy
            mask.push(pattern[acc], item);
            acc++;
          } else {
            // write
            mask.push(item);
          }
        }
        acc++;
      });
      return mask.join('').toString();
    }
    return value;
  },

  _getUnMasked: function _getUnMasked(value, type) {
    if (value && type) {
      var aux = value.split('');
      var pattern = this.masks[type];
      var input = '';
      var acc = 0;
      aux.some(function (item) {
        if (!pattern[acc] || pattern[acc] !== item) {
          input += item;
        }
        acc++;
      });
      return input;
    }
  }
};