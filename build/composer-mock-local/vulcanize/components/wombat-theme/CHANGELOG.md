# Changelog

<!-- Document the breaking changes when updating the component as major version.
As well as the necessary changes to upgrade the component (migration guide type).
-->

## 1.0.2
- Included container-xl and container-lg mixings to be shared on every component

## 1.0.1
- Use ~9.0.0 version of cells-coronita-theme, which fixes fonts not loading on chrome

## 1.0.0
- Initial Version
