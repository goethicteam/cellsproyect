'use strict';

var fs = require('fs');
var nunjucks = require('nunjucks');

var colors = JSON.parse(fs.readFileSync('colors.json'));

nunjucks.render('cells-coronita-theme-color.njk', colors, function (err, result) {
  if (err) {
    return console.log(err);
  }

  fs.writeFile('./cells-coronita-theme-color.html', result, function (err) {
    if (err) {
      console.log(err);
    }
    console.log('Color theme saved!');
  });
});