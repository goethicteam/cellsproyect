# cells-coronita-theme

[![Certificated](https://img.shields.io/badge/certificated-yes-brightgreen.svg)](http://bbva-files.s3.amazonaws.com/cells/bbva-catalog/index.html)

This theme offers you _Coronita_ style guide in a set of variables and mixins to start using it for your components.

## Updating colors

1. Install node dependencies: `npm install`
2. **Important:** edit `colors.json` instead of `cells-coronita-theme-color.html`. The second file is **autogenerated**.
3. Update cells-coronita-theme-color.html by running one of these commands:    
    
    `node index.js` or `npm run build:color`


## Extend it with project theme

1. Create your `project-theme`
1. Import `cells-coronita-theme`:
  ```json
  {
    "name": "example-project-theme",
    "version": "1.0.0",
    "description": "Extends cells-coronita-theme and includes shared-styles of components",
    "main": [
      "example-project-theme.html"
    ],
    "dependencies": {
      "polymer": "Polymer/polymer#^1.2.0",
      "cells-coronita-theme": "^1.0.0"
    }
  }
  ```

You are ready to go!

> Check [an example](https://globaldevtools.bbva.com/bitbucket/projects/CELLSLABS/repos/example-project-theme/browse)
