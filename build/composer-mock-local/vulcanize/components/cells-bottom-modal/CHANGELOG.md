# CHANGELOG

## v6.0.0

### Breaking changes

- `closeOnClick` property removed. ([commit](https://globaldevtools.bbva.com/bitbucket/projects/BGCM/repos/cells-bottom-modal/commits/c5a14832ab1f3347e146c57f472c9e49e110c9ff))

## v2.0.0

### Breaking changes

- `noCloseIcon` property removed.
