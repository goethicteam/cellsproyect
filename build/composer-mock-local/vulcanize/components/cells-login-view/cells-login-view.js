'use strict';

(function () {

  'use strict';

  Polymer({

    is: 'cells-login-view',

    behaviors: [CellsBehaviors.DeviceBehavior],

    properties: {

      /**
       * User to login
       * {
       *  userId: userId,
       *  username: username,
       *  password: password
       * }
       */
      user: {
        type: Object,
        notify: true
      },

      /**
       * Selected Document type.
       */
      documentType: {
        type: Object,
        observer: '_onDocumentTypeSelected'
      },

      /**
       Checks when image thumb must be smaller
      */
      smallThumb: {
        type: String,
        computed: '_computeSmallThumb(documentType)'
      },

      /**
       * Shows a register button
       */
      allowRegistration: {
        type: Boolean,
        value: false
      },

      /**
       * Header image URL
       */
      headerImage: {
        type: String,
        value: function value() {
          return this.resolveUrl('/images/bg-login.jpg');
        }
      },

      /**
       * Alt text for header image
       */
      headerImageAlt: {
        type: String,
        value: ''
      },

      /**
       * Defines if the login is maximized (full screen). This will be set
       * when the inputs are focused.
       */
      maximized: {
        type: Boolean
      },

      /*
       * Defines if username is currently stored
       */
      isLogged: {
        type: Boolean,
        computed: '_IsLogged(user.username)'
      },

      /**
       * Defines the loading state.
       */
      loading: {
        type: Boolean,
        value: false
      },

      /**
       * Loading process in spinner
       */
      loadingProcess: {
        type: Boolean,
        value: false
      },

      /**
       * Number of current process
       */
      loadingCurrentProcess: Number,

      /**
       * Delay of first process in spinner
       */
      loadingDelay: {
        type: Number,
        value: 1500
      },

      /*
       * Ends loading login
       */
      loadingEnd: {
        type: Boolean,
        value: false
      },

      /*
       * Start icon for spinner
       */
      startIcon: {
        type: String
      },

      /*
       * End icon for spinner
       */
      endIcon: {
        type: String
      },

      /*
       * Clear icon for ID field
       */
      clearIdIcon: {
        type: String
      },

      /*
       * Clear icon for Password field
       */
      clearPwdIcon: {
        type: String
      },

      /*
       * Toggle icon for Password field
       */
      togglePwdIcon: {
        type: String
      },

      /*
       * Toggle icon for Password field when it's toggled
       */
      togglePwdIconToggled: {
        type: String
      },

      /**
       * If true, autofocus on first available input
       */
      autofocus: {
        type: Boolean,
        value: false
      },

      /**
      * Cells molecule input validate options
      */

      inputUserOptions: {
        type: Object,
        value: function value() {
          return {
            autoValidate: false,
            inputStatusValidate: false,
            autoMask: true,
            inputType: '',
            allowedValue: '',
            errorMessage: '',
            errorMessageIcon: '',
            mask: '',
            maxLength: ''
          };
        }
      },

      /**
      * Delay before navigation on login success, to show the animation
      */
      delayNavigation: {
        type: Number
      },

      coronitaIcons: {
        type: Array,
        observer: '_initializeRandomIcons',
        value: ['coronita:euro', 'coronita:dollar', 'coronita:creditcard', 'coronita:digitalcard', 'coronita:mobilecashdollar', 'coronita:wallet', 'coronita:mobilecasheuro', 'coronita:place', 'coronita:exploration', 'coronita:nearme', 'coronita:search', 'coronita:graphics', 'coronita:mobile', 'coronita:desktop', 'coronita:calculator', 'coronita:support', 'coronita:videoplayline', 'coronita:communicationpublic', 'coronita:weather', 'coronita:entretainment', 'coronita:shopping', 'coronita:sales', 'coronita:discount', 'coronita:promotion', 'coronita:supermarket', 'coronita:shop', 'coronita:florist', 'coronita:coffeshop', 'coronita:restaurant', 'coronita:retirement']
      },

      /**
       * Collection of random icons that appear in the background
       */
      randomIcons: {
        type: Array
      }
    },

    /*
     * Reset login
     */
    reset: function reset() {
      this.loading = false;
      this.user = {};
      this.$.credentialsUserPwd.reset();
      if (this.documentType) {
        this.set('user.documentType', this.documentType);
      }
      this.loadingEnd = false;
      this.maximized = false;
      this.$.spinner.reset();
    },

    /**
     * On login error event
     */
    loginError: function loginError() {
      this.loading = false;
      this.set('user.password', '');
    },

    /*
     * On login event
     */
    _doLogin: function _doLogin(e) {
      e.stopPropagation();
      this.cancelDebouncer('restore');
      this.set('user', e.detail);
      this.loading = true;
      this.dispatchEvent(new CustomEvent('login', {
        bubbles: true,
        composed: true,
        detail: e.detail
      }));

      if (this.loadingProcess) {
        this.$.spinner.startProcess();
      }

      if (this.loadingProcess) {
        this.$.spinner.startProcess();
      }

      /**
       * @event login
       * Fired on login submit
       */
    },

    /*
     * The user data is correct and the login has occurred
     */
    loadingSuccess: function loadingSuccess(data) {
      this.loadingEnd = true;

      if (this.loadingProcess) {
        this.$.spinner.nextProcess();
      }

      if (this.loadingProcess) {
        this.$.spinner.nextProcess();
      }

      this._cacheUser(data);

      this.async(function () {
        this.dispatchEvent(new CustomEvent('navigate-from-login', {
          bubbles: true,
          composed: true
        }));
      }, this.delayNavigation);

      /**
       * @event navigate-from-login
       * Fired when login has occurred
       */
    },

    loadingProcessNextStep: function loadingProcessNextStep() {
      this.$.spinner.nextProcess();
    },

    /**
     * Save username. Next time user only needs to set the password.
     */
    _cacheUser: function _cacheUser(data) {
      if (data) {
        this.dispatchEvent(new CustomEvent('register-device', {
          bubbles: true,
          composed: true,
          detail: {
            customerId: this.user.userId,
            firstName: data.firstName
          }
        }));
      }
    },

    /*
     * Animate on blur input
     */
    _onInputBlur: function _onInputBlur(e) {
      this.debounce('restore', function () {
        this.maximized = false;
      }, 100);
    },

    /*
     * Animate on focus input
     */
    _onInputFocus: function _onInputFocus(e) {
      this.cancelDebouncer('restore');
      this.maximized = true;
    },

    /*
     * Checked username
     */
    _checkedMaximized: function _checkedMaximized(maximized) {
      return maximized ? 'maximized' : '';
    },

    /*
     * Checked username
     */
    _checkedUsername: function _checkedUsername(username) {
      return username ? 'is-logged' : '';
    },

    /*
     * Checks if device is IOS and returns className
     */
    _checkIOS: function _checkIOS(isIOS) {
      return isIOS ? 'non-move-up' : 'move-up';
    },

    /*
     * Checked username and set isLogged
     */
    _IsLogged: function _IsLogged(username) {
      return !!username;
    },

    /**
     * Sets document type in the user object
     */
    _onDocumentTypeSelected: function _onDocumentTypeSelected(detail) {
      this.$.credentialsUserPwd.set('documentType', detail);
    },

    /**
     * Computed method to check if the image thumb must be smaller.
     */
    _computeSmallThumb: function _computeSmallThumb(documentType) {
      return documentType ? 'small-thumb' : '';
    },

    _getRandomRoundNum: function _getRandomRoundNum(min, max) {
      return Math.floor(this._getRandomNum(min, max));
    },

    _getRandomEvenNum: function _getRandomEvenNum(min, max) {
      var rand = this._getRandomRoundNum(min, max);
      return rand % 2 === 0 ? rand : rand - 1;
    },

    _getRandomNum: function _getRandomNum(min, max) {
      return Math.random() * (max - min) + min;
    },

    _initializeRandomIcons: function _initializeRandomIcons() {
      var maxNumIcons = 50;
      var aux = [];
      var randomNum = void 0;
      var randomSize = void 0;
      var x = void 0;
      var y = void 0;
      var randomDelay = void 0;
      for (var i = 0; i < maxNumIcons; i++) {
        randomNum = this._getRandomRoundNum(0, this.coronitaIcons.length);

        randomSize = this._getRandomEvenNum(8, 34);
        x = this._getRandomNum(0, this.clientWidth);

        y = this._getRandomNum(0, this.clientHeight);
        randomDelay = this._getRandomNum(0, 2);
        aux[i] = {
          'icon': this.coronitaIcons[randomNum],
          'size': randomSize.toString(),
          'xCoord': x.toString(),
          'yCoord': y.toString(),
          'delay': randomDelay.toString()
        };
      }
      this.set('randomIcons', aux);
    }

  });
})();