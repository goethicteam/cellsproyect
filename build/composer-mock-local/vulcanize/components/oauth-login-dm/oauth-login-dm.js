'use strict';

(function () {
  'use strict';

  var tokenProperty = 'token';
  var userIdProperty = 'userId';

  Polymer({

    /**
     * The event indicating a login was performed successfully.
     * @event login-success
     * @param {string} userId - The userId of the user logged.
     */ /**
        * The event indicating a login have failed.
        * @event login-error
        * @param {object} data - The error data returned by the server.
        * @param {object} userId - The userId of the user logged.
        * @param {string} consumerId - The {@link consumerId} object with consumer Id.
        * @param {string} authenticationType - The {@link authenticationType} object with authentication type.
        * @param {string} idAuthenticationData - The {@link idAuthenticationData} object with id authentication data.
        */ /**
           * The event indicating a logout was performed successfully.
           * @event logout-success
           * @param {string} userId - The userId of the user logged out.
           */ /**
              * The event indicating a that userId was changed.
              * @event user-id-changed
              * @param {string} userId - The new userId
              */

    is: 'oauth-login-dm',

    properties: {

      /**
       * The base URL for the api to request
       * @type {String}
       */
      baseUrl: {
        type: String,
        value: ''
      },

      /**
       * The base Path to the resource
       * @type {String}
       */
      basePath: {
        type: String,
        value: ''
      },

      /**
       * The login type to be used to login the user. There are 2 values supported:
       * `webseal` that uses JWT + Granting Ticket and 'gt' that uses only Granting Ticket
       * @type {string}
       * @default "password"
       */
      loginType: {
        type: String,
        value: 'password'
      },
      /**
       * Authentication type.
       * Required to login.
       * @type {string}
       * @default 'password'
       */
      authenticationType: {
        type: String,
        value: 'password'
      },

      /**
       * The user id of the user logged
       * @type {string}
       */
      userId: {
        type: Object
      },

      /**
       * JWT payload request must be sent as a plain text
       * @type {object}
       * @private
       */
      _jwtRequestHeaders: {
        type: Object,
        readOnly: true,
        value: function value() {
          return {
            'Accept': 'application/json, text/javascript, */*; q=0.01',
            'Content-Type': 'application/x-www-form-urlencoded'
          };
        }
      }
    },

    /**
     * Function that removes the tsec and userId stored in the session store
     * and also set the userId to null, which will fire the event of userId changed
     * @emits user-id-changed
     */
    resetUserId: function resetUserId() {

      // Remove the TSEC stored in SessionStorage
      sessionStorage.removeItem(tokenProperty);

      // Remove userId stored in SessionStorage
      sessionStorage.removeItem(userIdProperty);

      this._notifyUserLogged(null);
    },

    _notifyUserLogged: function _notifyUserLogged(userId) {
      this.set('userId', userId);
      this.fire('user-logged', {
        userId: userId
      });
    },

    /**
     * Generate the payload to send in the request message to the login service
     * according to the {@link grantingTicketType}
     * @param {object} user - The {@link user} object with userId and password
     * @param {string} consumerId - The {@link consumerId} object with consumer id
     * @param {string} authenticationType - The {@link authenticationType} object with authentication type
     * @param {string} idAuthenticationData - The {@link idAuthenticationData} object with id authentication data.
     * @param {Payload} The payload to be sent to the login server with the user information
     * @private
     */
    _generatePayload: function _generatePayload(user, consumerId, authenticationType, idAuthenticationData) {
      var data = {
        password: user.password,
        username: user.userId,
        grant_type: "password",
        client_id: null,
        client_secret: null
      };
      return data;
    },

    /**
     * Function that depending on the {@link loginType} will call the JWT + Granting Ticket server to login the user
     * or only the Granting ticket.It will send all necessary information like user credentials and additional required info like
     * {@link consumerId},  {@link authenticationType} and {@link idAuthenticationData}
     * @param {object} user - The {@link user} object with userId and password (or token). Will override the default
     * @param {string} consumerId - The {@link consumerId} object with consumer Id. Will override the default
     * @param {string} authenticationType - The {@link authenticationType} object with authentication type. Will override the default
     * @param {string} idAuthenticationData - The {@link idAuthenticationData} object with id authentication data. Will override the default
     */
    login: function login(user, password, authenticationType) {

      user = user || this.user;
      var password = user.password;
      authenticationType = "password";
      this._autenticate(user, password, authenticationType);
    },

    /**
     * Function will call Granting ticket to get a valid TSEC.
     * @param {object} user - The {@link user} object with userId and password (or token).
     * @param {string} consumerId - The {@link consumerId} object with consumer Id.
     * @param {string} authenticationType - The {@link authenticationType} object with authentication type.
     * @param {string} idAuthenticationData - The {@link idAuthenticationData} object with id authentication data.
    */
    _autenticate: function _autenticate(user, consumerId, authenticationType, idAuthenticationData) {

      // Prepare GT request payload
      var requestData = this._generatePayload(user, user.password, authenticationType);
      //
      var gtDP = this.$.gtRequest;

      this._request(gtDP, {
        provider: 'gt',
        base: this.baseUrl,
        path: '' + this.basePath,
        data: requestData,
        method: 'POST',
        success: function (data, response) {
          this._onLoginSuccess(data, response, user.userId);
        }.bind(this),
        error: function (data) {
          this._onLoginError(data, user.userId, user.password, authenticationType);
        }.bind(this)
      });
    },

    /**
     * A function called when a Granting Ticket is performed with success. It is also responsible to store the
     * TSEC returned into SessionStorage
     * @param {object} data - The data returned by the server.
     * @param {object} response - The response XHR used to get the TSEC from the header response.
     * @param {string} userId - The userId of the user logged.
     * @emits login-success
     * @emits user-id-changed
     * @private
     */
    _onLoginSuccess: function _onLoginSuccess(data, response, userId) {

      var token = data.access_token;
      // Store the Token into the session storage
      sessionStorage.setItem(token, token);

      // Store userId into the session storage
      sessionStorage.setItem(userIdProperty, userId);

      // Set the userId
      this._notifyUserLogged(userId);

      this.fire('login-success', userId);
    },

    /**
     * A function called when the DM had an error during the granting ticket..
     * @param {object} data - The error data returned by the server.
     * @param {string} userId - The userId of the user logged.
     * @param {string} consumerId - The {@link consumerId} object with consumer Id.
     * @param {string} authenticationType - The {@link authenticationType} object with authentication type.
     * @param {string} idAuthenticationData - The {@link idAuthenticationData} object with id authentication data.
     * @emits login-error
     * @private
     */
    _onLoginError: function _onLoginError(data, userId, password, authenticationType) {
      this.resetUserId();
      this.fire('login-error', {
        password: password,
        userId: userId,
        authenticationType: authenticationType,
        data: data
      });
    },

    /**
     * Will perform the call to the logout server to logout the user.
     */
    logout: function logout() {
      var user = this.user;

      // Call  Logout
      var gtDP = this.$.gtRequest;

      this._request(gtDP, {
        base: this.baseUrl,
        path: '' + this.basePath,
        method: 'DELETE',
        success: function (data) {
          this._onLogoutSuccess(data, user.userId);
        }.bind(this)
      });
    },

    /**
     * A function called when a logout is performed with success.
     * @param {string} userId - The userId of the user logged out.
     * @emits logout-success
     * @emits user-id-changed
     * @private
     */
    _onLogoutSuccess: function _onLogoutSuccess(data, userId) {

      this.resetUserId();

      // Clean user after logout success
      this.user = {};

      this._logoutJWT(userId);
    },

    /**
     * A function will call JWT logout.
     * @param {object} data - The data returned by the server.
     * @param {string} userId - The userId of the user logged out.
     * @emits logout-success
     * @private
     */
    _logoutJWT: function _logoutJWT(userId) {
      // Emit an event saying logout via JWT is done.
      this.fire('logout-success', userId);
    },

    /**
     * Given a config object it generates a request and do the entire flow associated
     * like fire status events, catch errors and execute given callbacks
     * @param {RequestConfig} config - The configuration object passed to generate the request.
     * @emits request-start
     * @private
     */
    _request: function _request(dp) {
      var config = arguments.length > 1 && arguments[1] !== undefined ? arguments[1] : {};


      config.data = config.data || {};

      this._prepareDataProvider(dp, config);

      var request = this._generateRequest(dp, config);
      this.fire('request-start', {
        request: request,
        config: config
      });
    },

    /**
     * Updates the **data provider** properties to perform the request.
     * @param {cells-psv-generic-dp} dp - `<cellls-psv-generic-dp>` used to perform the request.
     * @param {RequestConfig} config - The configuration object passed to generate the request.
     * @private
     */
    _prepareDataProvider: function _prepareDataProvider(dp, config) {
      dp.endpoint = config.base;
      dp.path = config.path;
      dp.queryParams = config.search;
      dp.body = config.data;
      dp.method = config.method || dp.method;
    },

    /**
     * Gets the request when it is done and manage the response
     * @param {iron-request} resp - The `<iron-request>` element used to perform the request.
     * @param {RequestConfig} config - The configuration object passed to generate the request.
     * @private
     */
    _manageResponse: function _manageResponse(resp, config, isError) {
      var respData = resp.parseResponse();

      var callback = isError ? config.error : config.success;
      if (callback && typeof callback === 'function') {
        callback(respData, resp.xhr);
      }
      if (config.complete && typeof config.complete === 'function') {
        config.complete(resp.xhr);
      }
      (isError ? this._onRequestError : this._onRequestSuccess).call(this, resp.xhr, respData);
      this._onRequestComplete(resp.xhr);
    },

    /**
     * Use the DP to create the request with the given config
     * @param {cells-psv-generic-dp} dp - `<cellls-psv-generic-dp>` used to perform the request.
     * @param {RequestConfig} config - The configuration object passed to generate the request.
     * @return {iron-request} The final `<iron-request>` element that performs the request.
     * @private
     */
    _generateRequest: function _generateRequest(dp, config) {
      var request = dp.generateRequest();
      request.then(function (resp) {
        this._manageResponse(resp, config, false);
      }.bind(this), function (error) {
        var lastResponse = dp.lastRequest;
        this._manageResponse(lastResponse, config, true);
      }.bind(this));
      return request;
    },

    /**
     * Callback invoked on request returns with a success code (2XX)
     * @param {XMLHttpRequest} request - The XMLHttpRequest used to perform the request.
     * @param {object} data - The parsed data from the request.
     * @emits request-success
     * @private
     */
    _onRequestSuccess: function _onRequestSuccess(request, data) {
      this.fire('request-success', {
        resp: request,
        data: data
      });
    },

    /**
     * Callback invoked on request returns with a error code (4XX or 5XX). It verifies if it is a authorization
     * request error (403) and if so, emits the event
     * @param {XMLHttpRequest} request - The XMLHttpRequest used to perform the request.
     * @param {object} data - The parsed data from the request.
     * @emits request-error
     * @private
     */
    _onRequestError: function _onRequestError(request, data) {

      this.fire('request-error', {
        resp: request,
        data: data
      });
    },

    /**
     * Callback invoked on request completes
     * @param {XMLHttpRequest} request - The XMLHttpRequest used to perform the request.
     * @emits request-complete
     * @private
     */
    _onRequestComplete: function _onRequestComplete(request) {
      this.fire('request-complete', {
        resp: request
      });
    }
  });
})();