'use strict';

(function () {
  'use strict';

  // this would be the only `paper-drawer-panel` in
  // the whole app that can be in `dragging` state

  var sharedPanel = null;

  function classNames(obj) {
    var classes = [];
    for (var key in obj) {
      if (obj.hasOwnProperty(key) && obj[key]) {
        classes.push(key);
      }
    }

    return classes.join(' ');
  }

  Polymer({

    is: 'paper-drawer-panel',

    behaviors: [Polymer.IronResizableBehavior],

    /**
     * Fired when the narrow layout changes.
     *
     * @event paper-responsive-change {{narrow: boolean}} detail -
     *     narrow: true if the panel is in narrow layout.
     */

    /**
     * Fired when the a panel is selected.
     *
     * Listening for this event is an alternative to observing changes in the `selected` attribute.
     * This event is fired both when a panel is selected.
     *
     * @event iron-select {{item: Object}} detail -
     *     item: The panel that the event refers to.
     */

    /**
     * Fired when a panel is deselected.
     *
     * Listening for this event is an alternative to observing changes in the `selected` attribute.
     * This event is fired both when a panel is deselected.
     *
     * @event iron-deselect {{item: Object}} detail -
     *     item: The panel that the event refers to.
     */
    properties: {

      /**
       * The panel to be selected when `paper-drawer-panel` changes to narrow
       * layout.
       */
      defaultSelected: {
        type: String,
        value: 'main'
      },

      /**
       * If true, swipe from the edge is disabled.
       */
      disableEdgeSwipe: {
        type: Boolean,
        value: false
      },

      /**
       * If true, swipe to open/close the drawer is disabled.
       */
      disableSwipe: {
        type: Boolean,
        value: false
      },

      /**
       * Whether the user is dragging the drawer interactively.
       */
      dragging: {
        type: Boolean,
        value: false,
        readOnly: true,
        notify: true
      },

      /**
       * Width of the drawer panel.
       */
      drawerWidth: {
        type: String,
        value: '256px'
      },

      /**
       * How many pixels on the side of the screen are sensitive to edge
       * swipes and peek.
       */
      edgeSwipeSensitivity: {
        type: Number,
        value: 30
      },

      /**
       * If true, ignore `responsiveWidth` setting and force the narrow layout.
       */
      forceNarrow: {
        type: Boolean,
        value: false
      },

      /**
       * Whether the browser has support for the transform CSS property.
       */
      hasTransform: {
        type: Boolean,
        value: function value() {
          return 'transform' in this.style;
        }
      },

      /**
       * Whether the browser has support for the will-change CSS property.
       */
      hasWillChange: {
        type: Boolean,
        value: function value() {
          return 'willChange' in this.style;
        }
      },

      /**
       * Returns true if the panel is in narrow layout.  This is useful if you
       * need to show/hide elements based on the layout.
       */
      narrow: {
        reflectToAttribute: true,
        type: Boolean,
        value: false,
        readOnly: true,
        notify: true
      },

      /**
       * Whether the drawer is peeking out from the edge.
       */
      peeking: {
        type: Boolean,
        value: false,
        readOnly: true,
        notify: true
      },

      /**
       * Max-width when the panel changes to narrow layout.
       */
      responsiveWidth: {
        type: String,
        value: '768px'
      },

      /**
       * If true, position the drawer to the right.
       */
      rightDrawer: {
        type: Boolean,
        value: false
      },

      /**
       * The panel that is being selected. `drawer` for the drawer panel and
       * `main` for the main panel.
       *
       * @type {string|null}
       */
      selected: {
        reflectToAttribute: true,
        notify: true,
        type: String,
        value: null
      },

      /**
       * The CSS selector for the element that should receive focus when the drawer is open.
       * By default, when the drawer opens, it focuses the first tabbable element. That is,
       * the first element that can receive focus.
       *
       * To disable this behavior, you can set `drawerFocusSelector` to `null` or an empty string.
       *
       */
      drawerFocusSelector: {
        type: String,
        value: 'a[href]:not([tabindex="-1"]),' + 'area[href]:not([tabindex="-1"]),' + 'input:not([disabled]):not([tabindex="-1"]),' + 'select:not([disabled]):not([tabindex="-1"]),' + 'textarea:not([disabled]):not([tabindex="-1"]),' + 'button:not([disabled]):not([tabindex="-1"]),' + 'iframe:not([tabindex="-1"]),' + '[tabindex]:not([tabindex="-1"]),' + '[contentEditable=true]:not([tabindex="-1"])'
      },

      /**
       * Whether the transition is enabled.
       */
      _transition: {
        type: Boolean,
        value: false
      }

    },

    listeners: {
      tap: '_onTap',
      track: '_onTrack',
      down: '_downHandler',
      up: '_upHandler'
    },

    observers: ['_forceNarrowChanged(forceNarrow, defaultSelected)', '_toggleFocusListener(selected)'],

    ready: function ready() {
      // Avoid transition at the beginning e.g. page loads and enable
      // transitions only after the element is rendered and ready.
      this._transition = true;
      this._boundFocusListener = this._didFocus.bind(this);
      console.warn(this.is, 'is deprecated. Please use app-layout instead!');
    },

    /**
     * Toggles the panel open and closed.
     *
     * @method togglePanel
     */
    togglePanel: function togglePanel() {
      if (this._isMainSelected()) {
        this.openDrawer();
      } else {
        this.closeDrawer();
      }
    },

    /**
     * Opens the drawer.
     *
     * @method openDrawer
     */
    openDrawer: function openDrawer() {
      requestAnimationFrame(function () {
        this.toggleClass("transition-drawer", true, this.$.drawer);
        this.selected = 'drawer';
      }.bind(this));
    },

    /**
     * Closes the drawer.
     *
     * @method closeDrawer
     */
    closeDrawer: function closeDrawer() {
      requestAnimationFrame(function () {
        this.toggleClass("transition-drawer", true, this.$.drawer);
        this.selected = 'main';
      }.bind(this));
    },

    _onTransitionEnd: function _onTransitionEnd(e) {
      var target = Polymer.dom(e).localTarget;
      if (target !== this.$.drawer) {
        // ignore events coming from the light dom
        return;
      }
      if (e.propertyName === 'left' || e.propertyName === 'right') {
        this.notifyResize();
      }
      if (e.propertyName === 'transform') {
        requestAnimationFrame(function () {
          this.toggleClass("transition-drawer", false, this.$.drawer);
        }.bind(this));
        if (this.selected === 'drawer') {
          var focusedChild = this._getAutoFocusedNode();
          focusedChild && focusedChild.focus();
        }
      }
    },

    _computeIronSelectorClass: function _computeIronSelectorClass(narrow, transition, dragging, rightDrawer, peeking) {
      return classNames({
        dragging: dragging,
        'narrow-layout': narrow,
        'right-drawer': rightDrawer,
        'left-drawer': !rightDrawer,
        transition: transition,
        peeking: peeking
      });
    },

    _computeDrawerStyle: function _computeDrawerStyle(drawerWidth) {
      return 'width:' + drawerWidth + ';';
    },

    _computeMainStyle: function _computeMainStyle(narrow, rightDrawer, drawerWidth) {
      var style = '';

      style += 'left:' + (narrow || rightDrawer ? '0' : drawerWidth) + ';';

      if (rightDrawer) {
        style += 'right:' + (narrow ? '' : drawerWidth) + ';';
      }

      return style;
    },

    _computeMediaQuery: function _computeMediaQuery(forceNarrow, responsiveWidth) {
      return forceNarrow ? '' : '(max-width: ' + responsiveWidth + ')';
    },

    _computeSwipeOverlayHidden: function _computeSwipeOverlayHidden(narrow, disableEdgeSwipe) {
      return !narrow || disableEdgeSwipe;
    },

    _onTrack: function _onTrack(event) {
      if (sharedPanel && this !== sharedPanel) {
        return;
      }
      switch (event.detail.state) {
        case 'start':
          this._trackStart(event);
          break;
        case 'track':
          this._trackX(event);
          break;
        case 'end':
          this._trackEnd(event);
          break;
      }
    },

    _responsiveChange: function _responsiveChange(narrow) {
      this._setNarrow(narrow);
      this.selected = this.narrow ? this.defaultSelected : null;
      this.setScrollDirection(this._swipeAllowed() ? 'y' : 'all');
      this.fire('paper-responsive-change', { narrow: this.narrow });
    },

    _onQueryMatchesChanged: function _onQueryMatchesChanged(event) {
      this._responsiveChange(event.detail.value);
    },

    _forceNarrowChanged: function _forceNarrowChanged() {
      // set the narrow mode only if we reached the `responsiveWidth`
      this._responsiveChange(this.forceNarrow || this.$.mq.queryMatches);
    },

    _swipeAllowed: function _swipeAllowed() {
      return this.narrow && !this.disableSwipe;
    },

    _isMainSelected: function _isMainSelected() {
      return this.selected === 'main';
    },

    _startEdgePeek: function _startEdgePeek() {
      this.width = this.$.drawer.offsetWidth;
      this._moveDrawer(this._translateXForDeltaX(this.rightDrawer ? -this.edgeSwipeSensitivity : this.edgeSwipeSensitivity));
      this._setPeeking(true);
    },

    _stopEdgePeek: function _stopEdgePeek() {
      if (this.peeking) {
        this._setPeeking(false);
        this._moveDrawer(null);
      }
    },

    _downHandler: function _downHandler(event) {
      if (!this.dragging && this._isMainSelected() && this._isEdgeTouch(event) && !sharedPanel) {
        this._startEdgePeek();
        // cancel selection
        event.preventDefault();
        // grab this panel
        sharedPanel = this;
      }
    },

    _upHandler: function _upHandler() {
      this._stopEdgePeek();
      // release the panel
      sharedPanel = null;
    },

    _onTap: function _onTap(event) {
      var targetElement = Polymer.dom(event).localTarget;
      var isTargetToggleElement = targetElement && this.drawerToggleAttribute && targetElement.hasAttribute(this.drawerToggleAttribute);

      if (isTargetToggleElement) {
        this.togglePanel();
      }
    },

    _isEdgeTouch: function _isEdgeTouch(event) {
      var x = event.detail.x;

      return !this.disableEdgeSwipe && this._swipeAllowed() && (this.rightDrawer ? x >= this.offsetWidth - this.edgeSwipeSensitivity : x <= this.edgeSwipeSensitivity);
    },

    _trackStart: function _trackStart(event) {
      if (this._swipeAllowed()) {
        sharedPanel = this;
        this._setDragging(true);

        if (this._isMainSelected()) {
          this._setDragging(this.peeking || this._isEdgeTouch(event));
        }

        if (this.dragging) {
          this.width = this.$.drawer.offsetWidth;
          this._transition = false;
        }
      }
    },

    _translateXForDeltaX: function _translateXForDeltaX(deltaX) {
      var isMain = this._isMainSelected();

      if (this.rightDrawer) {
        return Math.max(0, isMain ? this.width + deltaX : deltaX);
      } else {
        return Math.min(0, isMain ? deltaX - this.width : deltaX);
      }
    },

    _trackX: function _trackX(event) {
      if (this.dragging) {
        var dx = event.detail.dx;

        if (this.peeking) {
          if (Math.abs(dx) <= this.edgeSwipeSensitivity) {
            // Ignore trackx until we move past the edge peek.
            return;
          }
          this._setPeeking(false);
        }

        this._moveDrawer(this._translateXForDeltaX(dx));
      }
    },

    _trackEnd: function _trackEnd(event) {
      if (this.dragging) {
        var xDirection = event.detail.dx > 0;

        this._setDragging(false);
        this._transition = true;
        sharedPanel = null;
        this._moveDrawer(null);

        if (this.rightDrawer) {
          this[xDirection ? 'closeDrawer' : 'openDrawer']();
        } else {
          this[xDirection ? 'openDrawer' : 'closeDrawer']();
        }
      }
    },

    _transformForTranslateX: function _transformForTranslateX(translateX) {
      if (translateX === null) {
        return '';
      }
      return this.hasWillChange ? 'translateX(' + translateX + 'px)' : 'translate3d(' + translateX + 'px, 0, 0)';
    },

    _moveDrawer: function _moveDrawer(translateX) {
      this.transform(this._transformForTranslateX(translateX), this.$.drawer);
    },

    _getDrawerSlot: function _getDrawerSlot() {
      return Polymer.dom(this.$.drawerSlot).getDistributedNodes()[0];
    },

    _getAutoFocusedNode: function _getAutoFocusedNode() {
      return this.drawerFocusSelector ? Polymer.dom(this._getDrawerSlot()).querySelector(this.drawerFocusSelector) : null;
    },

    _toggleFocusListener: function _toggleFocusListener(selected) {
      if (selected === 'drawer') {
        this.addEventListener('focus', this._boundFocusListener, true);
      } else {
        this.removeEventListener('focus', this._boundFocusListener, true);
      }
    },

    _didFocus: function _didFocus(event) {
      var autoFocusedNode = this._getAutoFocusedNode();
      if (!autoFocusedNode) {
        return;
      }

      var path = Polymer.dom(event).path;
      var focusedChild = path[0];
      var drawerSlot = this._getDrawerSlot();
      var focusedChildCameFromDrawer = path.indexOf(drawerSlot) !== -1;

      if (!focusedChildCameFromDrawer) {
        event.stopPropagation();
        autoFocusedNode.focus();
      }
    },

    _isDrawerClosed: function _isDrawerClosed(narrow, selected) {
      return !narrow || selected !== 'drawer';
    }
  });
})();