'use strict';

const fs = require('fs');
const nunjucks = require('nunjucks');

const colors = JSON.parse(fs.readFileSync('colors.json'));

nunjucks.render('cells-coronita-theme-color.njk', colors, (err, result) => {
  if (err) { return console.log(err); }

  fs.writeFile('./cells-coronita-theme-color.html', result, (err) => {
    if (err) { console.log(err); }
    console.log('Color theme saved!');
  });
});