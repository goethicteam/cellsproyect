var menuMock = [
  {
    key: 'my-account',
    link: true,
    href: 'https://www.bancomer.com',
    target: '_blank',
    icon: {
      code: 'A01',
      size: 24
    },
    subMenus: [
      {
        title: 'This is a test',
        items: [
          {
            key: 'my-account',
            link: true,
            icon: {
              code: 'banking:B47',
              size: 8
            },
            href: 'https://www.bancomer.com',
            target: '_blank'
          },
          {
            key: 'my-account',
            link: true,
            icon: {
              code: 'banking:B47',
              size: 8
            },
            href: 'https://www.bancomer.com',
            target: '_blank'
          }
        ]
      },
      {
        title: 'This is a test',
        items: [
          {
            key: 'my-account',
            link: true,
            icon: {
              code: 'banking:B47',
              size: 8
            },
            href: 'https://www.bancomer.com',
            target: '_blank'
          },
          {
            key: 'my-account',
            link: true,
            icon: {
              code: 'banking:B47',
              size: 8
            },
            href: 'https://www.bancomer.com',
            target: '_blank'
          }
        ]
      }
    ]
  },
  {
    key: 'my-movements',
    href: 'urlToMovements',
    icon: {
      code: 'banking:A02',
      size: 24
    }
  },
  {
    key: 'people',
    link: true,
    href: 'https://www.bancomer.com',
    icon: {
      code: 'banking:A03',
      size: 24
    }
  },
  {
    key: 'awards',
    link: true,
    href: 'https://www.bancomer.com',
    icon: {
      code: 'banking:A05',
      size: 24
    },
    subMenus: [
      {
        items: [
          {
            key: 'awards',
            link: true,
            icon: {
              code: 'banking:A05',
              size: 10
            },
            href: 'https://www.bancomer.com',
            target: '_blank'
          },
          {
            key: 'awards',
            link: true,
            icon: {
              code: 'banking:A05',
              size: 10
            },
            href: 'https://www.bancomer.com',
            target: '_blank'
          }
        ]
      }
    ]
  },
  {
    key: 'broker',
    link: true,
    href: 'https://www.bancomer.com',
    icon: {
      code: 'banking:A04',
      size: 24
    }
  }
];

document.addEventListener('WebComponentsReady', function() {
  var myEl = document.querySelector('#myEl');
  var myEl2 = document.querySelector('#myEl2');
  var myEl3 = document.querySelector('#myEl3');

  var paperTabs = document.querySelector('paper-tabs');
  var contentTabs = document.querySelectorAll('div[data-tab]');

  paperTabs.addEventListener('click', function(e) {
    var tab = e.target.parentElement.dataset.tab;
    var toast = document.querySelector('#toast-' + tab.split('-')[1]);

    if (toast) {
      toast.open();
    }

    [].forEach.call(contentTabs, function(contentTab) {
      contentTab.classList.toggle('hidden', contentTab.dataset.tab !== tab);
    });
  });

  myEl2.addEventListener('on-click-nav-button-mobile', function() {
    var toast = document.querySelector('#toast');
    toast.open();
  });

  myEl.set('items', menuMock);
  myEl2.set('items', menuMock);
  myEl3.set('items', menuMock);
});
