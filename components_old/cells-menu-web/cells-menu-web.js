(function() {

  'use strict';

  Polymer({

    is: 'cells-menu-web',

    behaviors: [
      CellsBehaviors.i18nBehavior
    ],

    properties: {
      /**
      * Items to render in menu
      * @type {Array}
      * @public
      *```
      * [
      *   {
      *     key: 'my-account',
      *     link: true,
      *     href: 'https://www.bancomer.com',
      *     target: '_blank',
      *     icon: {
      *       code: 'A01',
      *       size: 24
      *     },
      *     subMenus: [
      *       {
      *         title: 'This is a test',
      *         items: [
      *           {
      *             key: 'my-account',
      *             link: true,
      *             href: 'https://www.bancomer.com',
      *             target: '_blank',
      *             icon: {
      *               code: 'icons:B47',
      *               size: 8
      *             }
      *           },
      *           {
      *             key: 'my-account',
      *             href: 'urlToMovements'
      *             icon: {
      *               code: 'icons:B47',
      *               size: 8
      *             }
      *           }
      *         ]
      *       }
      *     ]
      *   }
      * ]
      *```
      */
      items: {
        type: Array,
        value: function() {
          return [];
        }
      },
      /**
      * Select item default of items
      * @type {Number}
      * @public
      */
      selected: {
        type: Number
      },
      imgSrc: {
        type: String
      },
      /**
      * Opened in mobile version
      * @type {Boolean}
      */
      opened: {
        type: Boolean,
        value: false
      },
      /**
      * Opened item
      * @type {Number}
      * @public
      */
      openedChild: {
        type: Number
      },
      /**
      * Disable class to item selected
      * @type {Boolean}
      * @public
      */
      disableSelected: {
        type: Boolean,
        value: false
      },
      /**
      * Icon menu mobile, you can use cells-icons
      * @type {String}
      */
      iconMobileLeft: {
        type: String,
        value: 'icons:B34'
      },
      /**
      * Icon menu mobile, you can use cells-icons
      * @type {String}
      */
      iconMobileRight: {
        type: String,
        value: ''
      },
      /**
      * Icon menu size
      * @type {Number}
      */
      iconMobileSize: {
        type: Number,
        value: 24
      },
      /**
      * Icon view more in button, of mobile version.
      * @type {String}
      */
      iconViewMore: {
        type: String,
        value: 'icons:B39'
      },
      /**
      * Icon view more size
      * @type {Number}
      */
      iconViewMoreSize: {
        type: Number,
        value: 24
      },
      /**
      * Checked it's mobile device
      * @type {Boolean}
      */
      isMobile: {
        type: Boolean,
        value: false,
        observer: '_setOptionsMobile'
      },
      /**
      * Media query that divide the range of devices
      * @type {String}
      */
      mediaQuery: {
        type: String,
        value: '(max-width: 48rem)'
      },
      /**
      * Name of event when you click in button mobile
      * @type {String}
      */
      notifyEventMobileLeft: {
        type: String,
        value: 'on-click-left-button-mobile'
      },
      /**
      * Name of event when you click in button mobile
      * @type {String}
      */
      notifyEventMobileRight: {
        type: String,
        value: 'on-click-right-button-mobile'
      },
      /**
      * Default Action to on click in button mobile
      * @type {Boolean}
      */
      disableActionMobile: {
        type: Boolean,
        value: false
      },
      /**
       * Internationalization key for greeting
       */
      greetingKey: String,
      /**
       * User to show
       */
      user: {
        type: Object,
        observer: '_setUserProperties'
      },
      /**
       * Avatar url
       */
      _userAvatarUrl: {
        type: String,
        value: ''
      },
      /**
       * Avatar alternative text
       */
      _userAvatarAltText: {
        type: String,
        value: ''
      },
      /**
       * User name
       */
      _userName: {
        type: String,
        value: ''
      }

    },
    /*
    * Event on click in button menu mobile
    */
    _onClickMobileLeft: function() {
      if (!this.disableActionMobile) {
        this.set('opened', !this.opened);
      }

      this.fire(this.notifyEventMobileLeft);
    },
    /*
    * Event on click in button menu mobile
    */
    _onClickMobileRight: function() {
      if (!this.disableActionMobile) {
        this.set('opened', !this.opened);
      }

      this.fire(this.notifyEventMobileRight);
    },
    /*
    * Checked opened property and return class
    */
    _setOpened: function(opened) {
      return (opened) ? 'active' : '';
    },
    /*
    * Set config mobile by default
    */
    _setOptionsMobile: function(isMobile) {
      this.$.navBar.classList.remove('active');
      this.set('openedChild', null);
    },
    /*
    * View sub menu on click in view-more element
    */
    _viewMore: function(e) {
      var index = e.model.index;
      this.set('openedChild', (index !== this.openedChild) ? index : null);
    },
    /*
    * Checked item if has submenu and is mobile
    */
    _checkedHasSubMenu: function(isMobile, item) {
      return !(isMobile && item.subMenus && item.subMenus.length);
    },
    /*
    * Checked if item it's opened
    */
    _checkedOpened: function(opened, index) {
      return (opened === index) ? 'opened' : '';
    },
    /*
    * Checked if item has sub items
    */
    _checkedSubMenus: function(subMenus) {
      return (subMenus && subMenus.length);
    },
    /*
    * Checked if item it's a selected item
    */
    _checkedSelected: function(item, index) {
      return (item === index && !this.disableSelected) ? 'active' : '';
    },
    /*
    * Checked value to Boolean
    */
    _checkedValue: function(val) {
      return Boolean(val);
    },
    /*
    * Checked isMobile it's true, and returned class
    */
    _checkedMobile: function(isMobile) {
      return (isMobile) ? 'nav--mobile' : '';
    },

    _onClickSidebarIcon: function() {
      this.fire('open-sidebar');
    },
    /**
     * Set _userAvatarUrl and _userAvatarAltText.
     */
    _setUserProperties: function() {
      if (this.user && this.user.avatars && this.user.avatars.url) {
        this._userAvatarUrl = this.user.avatars.url;
      } else {
        this._userAvatarUrl = '';
      }

      if (this.user && this.user.firstName && this.user.lastName) {
        this._userAvatarAltText = this.user.firstName.concat(' ' + this.user.lastName);
      } else {
        this._userAvatarAltText = '';
      }

      if (this.user && this.user.firstName) {
        this._userName = this.user.firstName;
      }
      if (this.greetingKey === undefined) {
        this.set('greetingKey', 'cells-avatar-actions-header-default-greeting');
      }
    }
  });
}());
