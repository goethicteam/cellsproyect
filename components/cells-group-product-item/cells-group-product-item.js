(function() {

  'use strict';

  Polymer({

    is: 'cells-group-product-item',

    properties: {
      /**
       * Heading of cells-group-product-item.
       */
      heading: {
        type: String,
        value: null
      },
      /**
       * Link of cells-group-product-item.
       */
      headingLink: {
        type: String
      },
      /**
       * aria level of cells-group-product-item header.
       */
      headingLevel: {
        type: Number,
        value: 1
      },
      /**
       * List of transactions to show.
       */
      transactions: {
        type: Array,
        value: function() {
          return [];
        }
      },

      /**
       * Property name for transaction date.
       */
      transactionDatePropName: {
        type: String,
        value: 'availableDate'
      },

      /**
       * Date format to show transaction date.
       */
      transactionDateFormat: {
        type: String,
        value: 'DD MMMM'
      },

      /**
       * Maximum number of transactions to show
       */
      limit: {
        type: Number,
        value: 10
      },

      /**
       * If true, a year separator will be shown
       **/
      showYearSeparator: {
        type: Boolean,
        value: false
      },

      /**
       * If true, a link will be shown beside the day
       **/
      showDayLink: {
        type: Boolean,
        value: false
      },

      /**
       * Day link text to be shown
       **/
      dayLinkText: {
        type: String,
        value: 'Info'
      },

      _transactionsGroups: {
        type: Array,
        value: function() {
          return [];
        }
      }
    },

    observers: [
      '_onAddOrRemoveTransactions(transactions.splices)',
      '_transactionsObserver(transactions, limit, transactionDateFormat, transactionDatePropName)',
      '_headingLevelControl(headingLevel, heading, showYearSeparator)'
    ],

    _onAddOrRemoveTransactions: function(changeRecord) {
      if (changeRecord) {
        var transactionsGroups = this._generateTransactions(this.transactions, this.limit, this.transactionDateFormat, this.transactionDatePropName);
        this._transactionsGroups = transactionsGroups;
      }
    },

    _transactionsObserver: function(transactions, limit, transactionDateFormat, transactionDatePropName) {
      var transactionsGroups = this._generateTransactions(transactions, limit, transactionDateFormat, transactionDatePropName);
      this._transactionsGroups = transactionsGroups;
    },

    _generateTransactions: function(transactions, limit, transactionDateFormat, transactionDatePropName) {
      var transactionsGroups = [];

      if (transactions.length && limit > 0 && transactionDateFormat && transactionDatePropName) {
        limit = limit ? Math.min(transactions.length, limit) : transactions.length;
        var transactionsGroupedByYearAndDate = {};
        var transactionDate;
        var date;
        var year;
        var _sortKeys = function(a, b) {
          return b.localeCompare(a);
        };

        /* global moment */
        for (var i = 0; i < limit; i++) {
          transactionDate = transactions[i][transactionDatePropName];

          if (transactionDate) {
            transactionDate = moment(transactionDate.value || transactionDate);
            date = transactionDate.format('YYYYMMDD');
            year = transactionDate.format('YYYY');

            if (!transactionsGroupedByYearAndDate[year]) {
              transactionsGroupedByYearAndDate[year] = {};
            }

            if (!transactionsGroupedByYearAndDate[year][date]) {
              transactionsGroupedByYearAndDate[year][date] = [];
            }

            transactionsGroupedByYearAndDate[year][date].push(transactions[i]);
          }
        }

        transactionsGroups = Object.keys(transactionsGroupedByYearAndDate).sort(_sortKeys).map(function(yearKey) {
          var transactionsGroupedByDate = transactionsGroupedByYearAndDate[yearKey];

          var transactionGroupYear = Object.keys(transactionsGroupedByDate).sort(_sortKeys).map(function(dateKey) {
            var transactionsGroupDate = transactionsGroupedByDate[dateKey];

            transactionsGroupDate.date = moment(dateKey, 'YYYYMMDD').format(transactionDateFormat);

            return transactionsGroupDate;
          });
          transactionGroupYear.year = yearKey;

          return transactionGroupYear;
        });
      }

      return transactionsGroups;
    },

    _computedHeadingYear: function(heading, headingLevel) {
      if (heading) {
        return headingLevel + 1;
      } else {
        return headingLevel;
      }
    },
    _computedHeadingDate: function(heading, showYearSeparator, headingLevel) {
      if (heading && showYearSeparator) {
        return headingLevel + 2;
      } else if (heading || showYearSeparator) {
        return headingLevel + 1;
      } else {
        return headingLevel;
      }
    },

    /**
     * Fire an event when day link clicked
     */
    _dayLinkAction: function(e) {
      var model = e.model;
      this.fire('day-link-clicked', model.item);
    },

    /**
     * Fire an event when headingLink is clicked
     */
    _headingLink: function() {
      this.fire('heading-link-clicked');
    },

    /**
     * Fire an event when a transaction link is clicked
     */
    _transactionLinkAction: function(e) {
      var model = e.model;
      this.fire('transaction-link-action', model.item);
    }

  });
}());
