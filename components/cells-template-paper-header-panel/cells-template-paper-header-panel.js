'use strict';

(function () {

  'use strict';

  Polymer({
    is: 'cells-template-paper-header-panel',

    behaviors: [Polymer.CellsTemplateAnimationBehavior],

    properties: {
      /**
       * Controls header and scrolling behavior. Options are 'standard', 'seamed', 'transparent', 'waterfall', 'waterfall-tall', 'scroll' and 'cover'. Default is `standard`. It's necessary to paper-header-panel component.
       * @type {String}
       */
      mode: {
        type: String,
        value: 'standard',
        reflectToAttribute: true
      },

      /**
       * The class used in waterfall-tall mode. Change this if the header accepts a different class for toggling height, e.g. "medium-tall". It's necessary to paper-header-panel component.
       * @type {String}
       */
      tallClass: {
        type: String,
        value: 'tall'
      },

      /**
       * Property to set the header transparent with a gradient.
       * @type {Boolean}
       */
      gradientTransparent: {
        type: String,
        reflectToAttribute: true
      },
      /**
       * Footer height in px.
       * Can be set either by using a custom CSS property (--app__footer-height) or by setting
       * footerHeight. Default value for --app__footer-height is 60px.
       */
      footerHeight: {
        type: Number,
        observer: '_updateFooterHeight'
      },
      /**
       * Set to true if the template has footer or coexists with a external footer
       */
      hasFooter: {
        type: Boolean,
        value: false,
        reflectToAttribute: true
      }
    },

    _updateFooterHeight: function _updateFooterHeight(footerHeight) {
      this.updateStyles({ '--app__footer-height': footerHeight + 'px' });
    }

  });
})();