(function() {

  'use strict';

  Polymer({
    is: 'cells-product-card',

    behaviors: [
      Polymer.IronResizableBehavior
    ],

    properties: {
      /**
       * Product title.
       */
      heading: String,

      /**
       * Allows to define an aria-level (from 1 to 6) for the main header text.
       * If 0 is provided, main header text won't be treated as a heading.
       */
      headingLevel: {
        type: Number,
        value: 3
      },

      /**
       * Char used to mask the product IBAN.
       */
      maskChars: {
        type: String,
        value: '•'
      },

      /**
       * `src` attribute of the product image.
       */
      imageSrc: {
        type: String,
        value: ''
      },

      /**
       * Array with the label and amount for each balance.
       *
       * ```js
       * [
       *   { label: 'Utilizado', amount: 23231 },
       *   { label: 'Disponible', amount: 2331 }
       * ]
       * ```
       */
      balances: Array,

      /**
       * ISO 4217 code for the balances currency.
       */
      currencyCode: {
        type: String,
        value: 'EUR'
      },

      /**
       * ISO 4217 code for th local currency.
       */
      localCurrency: {
        type: String,
        value: 'EUR'
      },

      /**
       * Whether to hide decimals in amounts.
       */
      decimalsHidden: {
        type: Boolean,
        value: false
      },

      /**
       * Product IBAN
       */
      productIban: {
        type: String,
        value: ''
      },

      /**
       * Additional information for cards.
       */
      additionalInfo: {
        type: String,
        value: ''
      },

      /**
       * Row layout used when the card width is equal or greater than `minWidthForRowLayout'.
       */
      rowLayout: {
        type: Boolean,
        value: false,
        reflectToAttribute: true,
        readOnly: true,
        notify: true
      },

      /**
       * Minimum width in pixels at the card will use the row layout.
       */
      minWidthForRowLayout: {
        type: Number,
        value: 460
      }
    },

    listeners: {
      'iron-resize': '_onIronResize'
    },

    _onIronResize: function() {
      this._setRowLayout(this.offsetWidth >= this.minWidthForRowLayout);
    },

    attached: function() {
      this.async(this.notifyResize, 1);
    }
  });

}());
