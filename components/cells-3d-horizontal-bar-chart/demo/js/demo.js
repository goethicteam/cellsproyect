document.addEventListener('HTMLImportsLoaded', function() {
  (function() {
    'use strict';
    Polymer({
      is: 'cells-3d-horizontal-bar-chart-demo',
      properties: {
        dataLeft: {
          type: Number,
          value: 40
        },
        dataRight: {
          type: Number,
          value: 20
        }
      }
    });
  }());
});