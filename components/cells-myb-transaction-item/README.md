# cells-myb-transaction-item

Transaction item that fills transaction history


```html
    <section class="layout horizontal flex" on-click="_getItemClicked">
        <cells-atom-date
            date="[[ transaction.postedDate ]]"
            format="HH:mm">
        </cells-atom-date>

        <template
            is="dom-if"
            if="{{ !transaction._isRefund }}">
            <cells-atom-icon
                icon="[[ transactionSaleIcon ]]"
                class="sale">
            </cells-atom-icon>
        </template>

        <template
            is="dom-if"
            if="{{ transaction._isRefund }}">
            <cells-atom-icon
                icon="[[ transactionRefundIcon ]]"
                class="flex refund">
            </cells-atom-icon>
        </template>
            
        <template
            is="dom-if"
            if="{{ transaction.operationCancelled }}">
            <cells-atom-icon
                icon="[[ transactionCancelIcon ]]"
                class="flex cancelled">               
            </cells-atom-icon>                                     
        </template>

        <template is="dom-if"
          if="{{ _hasCard }}">
            <cells-molecule-mask
                masked
                value="[[ transaction.card.pan ]]"
                mask-chars="•"                                     
                visible-chars="4">
            </cells-molecule-mask>
        </template>

        <template is="dom-if" if="{{ !_hasCard }}">
            <span class="flex item-card">                 
                [[ t('cells-myb-transaction-item-cash') ]]
            </span>
        </template>

        <article class="flex layout vertical end item-amount">
            <cells-atom-amount
                class$="item-amount__total {{ getClasses(transaction) }}"
                amount="[[ transaction.localAmount.amount ]]"
                currency-code="[[ transaction.localAmount.currency ]]">
            </cells-atom-amount>
        </article>
    </section>
```

## Styling

The following custom properties and mixins are available for styling:

| Transaction-item property | Description     | Default        |
|:----------------|:----------------|:--------------:|
| --cells-myb-transaction-item  | empty mixin     | {}             |

| Transaction-item-card property | Description     | Default        |
|:----------------|:----------------|:--------------:|
| --cells-myb-transaction-item-card  | empty mixin     | {}             |

| Transaction-item-operation property | Description     | Default        |
|:----------------|:----------------|:--------------:|
| --cells-myb-transaction-item-operation-sale  | empty mixin     | {}            |
| --cells-myb-transaction-item-operation-refund  | empty mixin     | {}            |
| --cells-myb-transaction-item-operation-cancelled  | empty mixin     | {}            |

| Transaction-item-amount property | Description     | Default        |
|:----------------|:----------------|:--------------:|
| --cells-myb-transaction-item-amount  | empty mixin     | {}            |
| --cells-myb-transaction-item-amount-total  | empty mixin     | {}            |
| --cells-myb-transaction-item-amount-comision  | empty mixin     | {}            |