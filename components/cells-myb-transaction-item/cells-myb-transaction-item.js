(function() {
  'use strict';

  Polymer({

    is: 'cells-myb-transaction-item',

    behaviors: [
      CellsBehaviors.i18nBehavior
    ],

    ready: function() {
      Polymer.RenderStatus.afterNextRender(this, function() {
        this._transactionRange = {
          start: this.transaction.beginDate || this.transaction.postedDate,
          end: this.transaction.endDate
        };

        this._hasCard = !!(this.transaction.paymentChannel &&
                           this.transaction.paymentChannel.id !== 'MANUAL');
      });
    },

    properties: {

    /**
     * type of view to display
     * @type {String}
     */
      view: {
        type: String
      },

    /**
     * Transaction property
     * @type {Object}
     */
      transaction: {
        type: Object
      },

      /**
       * Transaction refund Icon
       * @type {String}
       */
      transactionRefundIcon: {
        type: String,
        value: 'coronita:return-12'
      },

      /**
       * Transaction sale Icon
       * @type {String}
       */
      transactionSaleIcon: {
        type: String,
        value: 'coronita:return-12'
      },

      /**
       * Transaction cancel icon
       * @type {String}
       */
      transactionCancelIcon: {
        type: String,
        value: 'coronita:close'
      },

      /**
       * hide/show range
       * @type {Boolean}
       */
      hiddenRange: {
        type: Boolean,
        value: false
      },

      /**
       * Transaction payment method
       * @type {Boolean}
       */
      _hasCard: {
        type: Boolean,
        value: false
      },

      /**
       * group of views to show/hide
       * @type {Object}
       */
      _selectedView: {
        type: Object,
        computed: '_selectedViewComputed(view)'
      },

      /**
       * Range to show (begin date/end date)
       * @type {Object}
       */
      _transactionRange: {
        type: Object
      },

      /**
       * range format
       * @type {Object}
       */
      _transactionFormat: {
        type: Object
      }
    },

    _selectedViewComputed: function(view) {
      var _data = {};
      _data[view] = true;

      this._transactionFormat = this._getFormatByView();

      return _data;
    },

    _getFormatByView: function() {
      var _format = {};
      switch (this.view) {
        case 'detail':
          _format = { item: 'HH:mm' };
          break;
        case 'daily':
          _format = { item: '', range: { start: 'DD', end: 'dddd' }};
          break;
        case 'monthly':
          _format = { item: 'MMMM',  range: { start: 'DD', end: 'DD MMMM' }};
          break;
        case 'yearly':
          _format = { item: 'YYYY', range: { start: 'DD MMM', end: 'DD MMM' }};
          break;
        case 'range':
          _format = { item: '', range: { start: 'DD', end: 'dddd' }};
          break;
        default:
          _format = { item: '', range: {start: 'DD', end: 'DD' }};
          break;
      }

      return _format;
    },

    /**
     * Fire launched when tarnsaction item is clicked
     * @event cells-myb-transaction-item-clicked
     */
    _getItemClicked: function() {
      this.fire('cells-myb-transaction-item-clicked', this.transaction);
    },

    /**
     * add class to show if refund
     * @param {Object} transaction selected
     * @return {String} class to show
    */
    _getClasses: function(transaction) {
      var refund = 'refund';
      if (transaction._isRefund) {
        return refund;
      }
      return;
    },

    _translatePaymentMethod: function(id) {
      return this.t('cells-myb-transaction-item-payment-method-' +  id);
    }

  });

}());
