var templateBind = document.getElementById('tbind');

document.addEventListener('WebComponentsReady', function() {

  var operationStats = 
  {
    "statPercentValue": "+7.02",
    "fromDateText": "año pasado",
    "stats": [{
      "total": "3.815",
      "amount": 972759.82,
      "currency": "EUR",
      "literal": "cells-myb-subheader-statics-total-sales",
      "class": "subheader-sales"
    }, {
      "total": "1.219",
      "amount": -308655.06,
      "currency": "EUR",
      "literal": "cells-myb-subheader-statics-total-refunds",
      "class": "subheader-refund"
    }, {
      "amount": -18185.27,
      "currency": "EUR",
      "literal": "cells-myb-subheader-statics-total-fees",
      "class": "subheader-fees"
    }, {
      "amount": 645919.49,
      "currency": "EUR",
      "literal": "cells-myb-subheader-statics-total-amount",
      "class": "subheader-total"
    }]
  };

  var mainAmount = {
      'totalElements': 3575,
      'amount': 14365.26,
      'currency': 'EUR'
    };

  var dateRange = {
    'date': {
      'start': '2016-01-01T00:00:00Z',
      'end': '2016-12-31T23:59:59Z'
    },
    'format': {
      'start': 'MMM',
      'end': 'MMM YYYY'
    },
    'separator': '-'
  };

  document.querySelector('cells-myb-subheader').operationStats = operationStats;
  document.querySelector('cells-myb-subheader').mainAmount = mainAmount;
  document.querySelector('cells-myb-subheader').dateRange = dateRange;

});