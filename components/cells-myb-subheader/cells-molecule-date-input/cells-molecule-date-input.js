(function() {

  Polymer({

    is: 'cells-molecule-date-input',

    properties: {
      /**
       * Format date selected.
       * @type {String}
       */
      format: {
        type: String,
        value: 'DD/MM/YYYY'
      },
      /**
       * Date selected by default
       * @type {String}
       */
      defaultDate: {
        type: String,
        value: '',
        observer: 'setDate'
      },
      /**
       * Actual input date with locale format.
       * @type {String}
       */
      showDate: {
        type: String
      },
      /**
       * Minimum date allowed (DD-MM-YYYY or DD/MM/YYYY).
       * @type {String}
       */
      minDate: {
        type: String
      },
      /**
       * Maximum date allowed (DD-MM-YYYY or DD/MM/YYYY).
       * @type {String}
       */
      maxDate: {
        type: String
      },
      /**
       * Boolean attribute indicating whether the selection of dates in the past is allowed.
       * @type {Boolean}
       */
      allowPastDate: {
        type: Boolean,
        value: false
      },
      /**
       * Default icon.
       * @type {String}
       */
      icon: {
        type: String,
        value: 'banking:G27'
      },
      /**
       * Default icon size.
       * @type {Number|String}
       */
      iconSize: {
        type: Number,
        value: 24
      },
      /**
       * Boolean property setting weekend days availability
       * @type {Boolean}
       */
      availableWeekends: {
        type: Boolean,
        value: false
      },
      /**
       * Boolean disabled select date
       * @type {Boolean}
       */
      disabled: {
        type: Boolean
      },
      /**
       * Set placeholder when date is not selected if showPlaceholder is true
       * @type {String}
       */
      placeholder: {
        type: String,
        value: 'Select date'
      },
      inputFocus: {
        observer: '_onFocus'
      },
      /**
       * Flags if current userAgent belongs to iOS platform (iPhone or iPad)
       * @type {Boolean}
       */
      _isIOS: {
        type: Boolean,
        value: false
      },
      /**
       * Last date selected
       * @type {String}
       */
      _lastDate: {
        type: String
      },
      /**
       * Used to trigger an observer when changing values
       * @type {String}
       */
      _fieldValue: {
        type: String,
        value: ''
      },
      lang: {
        type: String,
        value: 'en'
      }
    },

    listeners: {
      'field.focus': '_onFocus',
      'field.blur': '_onBlur'
    },

    ready: function() {
      //Eval value of <em>_isIOS</em>
      [
        /iphone/i,
        /ipad/i
      ].some(function(pattern) {
        this.set('_isIOS', pattern.test(navigator.userAgent));
        return this._isIOS;
      }, this);

      //Attach input callback based on <em>_isIOS</em>
      this.$.field[(this._isIOS ? 'onblur' : 'onchange')] = this._setValue.bind(this);
    },
    attached: function() {
      this.$.lib.time.locale(this.lang);
    },
    /**
     * Reset state
     */
    reset: function() {
      this.setDate(this.defaultDate);
      this.classList.remove('error');
    },
    _onFocus: function(e) {
      this.toggleClass('focus', true);
    },
    _onBlur: function(e) {
      this.toggleClass('focus', false);
    },
    /**
     * Set the input date value.
     * @param {Date} date The date to set.
     */
    setDate: function(date) {
      var parseDate = this._parseDate(date);
      this.set('_fieldValue', parseDate);
      this.set('_lastDate', parseDate);
      this.set('showDate', date);
    },
    /**
    * Fired after clicking the date input
    * @event input-date-clicked
    */
    _onClick: function(e) {
      if (this.disabled) {
        e.preventDefault();
      } else {
        this.toggleClass('focus', true);
        this.fire('input-date-clicked');
      }
    },
    /**
     * Set weekends availability
     * @param available {Boolean} Weekends are available.
     */
    setWeekendsAvailability: function(available) {
      this.set('availableWeekends', Boolean(available));
    },
    /**
     * Set class disabled.
     * @param {Boolean} disabled property
     */
    _checkedDisabled: function(disabled) {
      return (disabled) ? 'disabled' : '';
    },
    /*
    * Parse date to HTML5 standart
    * @param {String} Date to parse
    */
    _parseDate: function(date) {
      return this.$.lib.time(date).format('YYYY-MM-DD');
    },
    /**
     * Choose format in function lang.
     * @param {String} date to apply.
     * @return {String} The date value formated.
     */
    _parseDateToFormat: function(date) {
      if (date) {
        return this.$.lib.time(date).format(this.format);
      } else {
        return false;
      }
    },
    /**
     * Set the input date value with format.
     */
    _setValue: function() {
      var date = (this._fieldValue === '') ? this._lastDate : this._fieldValue;
      var today = this.$.lib.time().format('YYYY-MM-DD');
      var isToday = this.$.lib.time(today).isSame(date);
      var isPastDate = this.$.lib.time(today).isAfter(date);
      var selectedUTCDay = new Date(date).getUTCDay();
      this.toggleClass('focus', false);

      this.set('showDate', date);

      if (!isToday && isPastDate && !this.allowPastDate) {
        this.classList.add('error');
        /**
        * Fired when we select a past date and it isn't allowed
        * @event input-date-error
        */
        this.fire('input-date-error', {
          message: 'You have to select a date bigger than today.',
          error: 'past-date-not-allowed'
        });
      } else if (!this.availableWeekends && (selectedUTCDay === 0 || selectedUTCDay === 6)) {
        this.classList.add('error');
        this.fire('input-date-error', {
          message: 'Weekends not allowed',
          error: 'weekends-not-allowed'
        });
      } else {
        this.classList.remove('error');
        this.set('_lastDate', this._parseDate(date));
        /**
        * Fired when a valid date is selected.
        * @event input-date-changed
        * @param {{date: (Date)}} set to date selected
        */
        this.fire('input-date-changed', {
          date: date,
          formatDate: this._parseDateToFormat(this.showDate)
        });
      }
    }
  });
}());
