/* global moment */
var myEl;
var selectLang = document.querySelector('select');
var actualKey = null;
var mockNormal = {};
var mockBlockOld = {
  allowPastDate: true
};
var mockDefinedDate = {
  date: '2034-05-15'
};
var mocks = {
  'mockNormal': mockNormal,
  'mockBlockOld': mockBlockOld,
  'mockDefinedDate': mockDefinedDate
};

document.addEventListener('WebComponentsReady', function() {
  myEl = document.querySelector('cells-molecule-date-input');
});

document.addEventListener('input-date-error', function(e) {
  alert(e.detail.message);
});

function setMock(key) {

  actualKey = key || 'mockNormal';

  var contentBox = document.querySelector('#' + actualKey);

  var dateinput = document.createElement('cells-molecule-date-input');
  for (var property in mocks[actualKey]) {
    if (mocks[actualKey].hasOwnProperty(property)) {
      if (property === 'date') {
        dateinput.setDate(mocks[actualKey][property]);
      } else {
        dateinput.set(property, mocks[actualKey][property]);
      }
    }
  }
  document.querySelector('#mockBox').innerHTML = '';
  document.querySelector('#mockBox').appendChild(dateinput);

}

function getFormatedDate() {
  var format = document.querySelector('#format-input').value;
  var result = document.querySelector('cells-molecule-date-input').getFormatDate(format);
  alert(result);
}

function setMinimumDate() {
  var minDate = document.querySelector('#min-date-input').value;
  myEl.set('minDate', moment(minDate, 'DD/MM/YYYY').toDate());
}

function setMaximumDate() {
  var maxDate = document.querySelector('#max-date-input').value;
  myEl.set('maxDate', moment(maxDate, 'DD/MM/YYYY').toDate());
}

function setUnavailableWeekends() {
  document.querySelector('cells-molecule-date-input').setWeekendsAvailability(false);
}

function setAvailableWeekends() {
  document.querySelector('cells-molecule-date-input').setWeekendsAvailability(true);
}

function setReset() {
  document.querySelector('cells-molecule-date-input').reset();
}

function toggleDisabled() {
  myEl.set('disabled', !myEl.disabled);
}

setMock();
