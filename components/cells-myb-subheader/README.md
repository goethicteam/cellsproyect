# cells-myb-subheader

Example:
```html
<cells-myb-subheader></cells-myb-subheader>
```

```


The following custom properties and mixins are available for styling:

| Custom property | Description     | Default        |
|:----------------|:----------------|:--------------:|
| --cells-myb-subheader-scope      | scope description | default value  |
| --cells-myb-subheader  | empty mixin     | {}             |
| --cells-myb-subheader-total-operation  | empty mixin     | {}             |
| --cells-myb-subheader-operation-top  | empty mixin     | {}             |
| --cells-myb-subheader-operation-bottom  | empty mixin     | {}             |
| --cells-myb-subheader-from-date  | empty mixin     | {}             |
| --cells-myb-subheader-see-more  | empty mixin     | {}             |
| --cells-myb-subheader-collapsible-content  | empty mixin     | {}             |
| --cells-myb-subheader-full-info  | empty mixin     | {}             |
| --cells-myb-subheader-full-info-detail  | empty mixin     | {}             |
| --cells-myb-subheader-full-info-sales-refund  | empty mixin     | {}             |
| --cells-myb-subheader-full-info-sales  | empty mixin     | {}             |
| --cells-myb-subheader-full-info-refund  | empty mixin     | {}             |
| --cells-myb-subheader-full-info-fees  | empty mixin     | {}             |
