(function() {

  /* global moment */

  'use strict';

  Polymer({

    is: 'cells-myb-subheader',

    behaviors: [
      CellsBehaviors.i18nBehavior
    ],

    properties: {
      /**
       * Resume data with from / to params.
       * @type {Array}
       */
      mainAmount: {
        type: Object
      },

      /**
       * Variable for date time in resume
       * @type {Object}
       */
      dateRange: {
        type: Object
      },

      /**
       * Stats for comparison from previous date
       * @type {Object}
       * {
       *  fromDateText: [STRING],
       *  statPercentValue: [NUMBER],   // 5.3,  -322.56 ...
       *  stats: [
       *    {
       *      total: [NUMBER],    // optional
       *      amount: [AMOUNT],
       *      currency: [STRING]
       *    }, ...
       *  ],
       *
       * }
       */
      operationStats: {
        type: Object,
        observer: 'operationStatsObserver'
      },

      /**
       * Variable for changing 'see more' text
       * @type {String}
       */
      seeDetails: {
        type: String
      },

      /**
       * Variable to show percentage value formatted
       * @type {String}
       */
      _percentageStatFormat: {
        type: String
      },

      /**
       * Variable for icon type
       * @type {String}
       */
      _percentageStatIcon: {
        type: String
      },

      /**
       * Variable to show or not incrementation
       * @type {Boolean}
       */
      _showPercentage: {
        type: Boolean
      },

      /**
       * Is opened see more detail
       * @type {Boolean}
       */
      isOpenedSeeMore: {
        type: Boolean,
        notify: true,
        reflectToAttribute: true,
        observer: '_onOpenedChange'
      },

      loadingState: {
        type: Boolean,
        value: false
      }
    },

    _onOpenedChange: function() {
      this.fire('open-subheader-state-changed', this.isOpenedSeeMore);
      if (this.isOpenedSeeMore) {
        this.fire('open-subheader-state-opened');
      } else {
        this.fire('open-subheader-state-closed');
      }
      if (!this.isOpenedSeeMore) {
        this.set('seeDetails', this.getTranslation('cells-myb-subheader-see-more'));
      }
    },

    reset: function() {
      this.set('isOpenedSeeMore', false);
      this.set('seeDetails', this.getTranslation('cells-myb-subheader-see-more'));
    },

    showLoading: function() {
      this.set('loadingState', true);
    },

    hideLoading: function() {
      this.set('loadingState', false);
    },

    operationStatsObserver: function(operationStats) {
      var increment;
      var absoluteValue;
      var tmpPercentageStatIcon;
      if (operationStats && operationStats.statPercentValue) {
        increment = operationStats.statPercentValue > 0 ? true : false;
        if (increment) {
          tmpPercentageStatIcon = {
            'icon': 'coronita:upload',
            'class': 'increase'
          };
        } else {
          tmpPercentageStatIcon = {
            'icon': 'coronita:download',
            'class': 'decrease'
          };
        }
        absoluteValue = Math.abs(operationStats.statPercentValue);
        this.set('_percentageStatFormat', absoluteValue.toLocaleString(document.documentElement.lang) + '%');
        this.set('_percentageStatIcon', tmpPercentageStatIcon);
        this.set('_showPercentage', true);
      } else {
        this.set('_showPercentage', false);
      }
      this.hideLoading();
    },

    /**
     * Function to toogle see more (changes literals and opens block)
     * @Func
     */
    toggleSeeMore: function() {
      var strTranslation = this.$.collapse.opened ? 'cells-myb-subheader-see-more' : 'cells-myb-subheader-see-less';
      this.seeDetails = this.getTranslation(strTranslation);
      this.$.collapse.toggle();
    },

    closeSeeMore: function() {
      if (this.$.collapse.opened) {
        this.$.collapse.toggle();
      }
    }
  });
}());
