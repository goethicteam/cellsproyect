var mocks = {
  mock1: 'mock1'
};

var cards = {
  'CREDIT_CARDS': [
    { "cardId": "2103", "number": "*2103", "numberType": { "id": "PAN", "name": "Permanent Account Number" }, "cardType": { "id": "CREDIT_CARD", "name": "Tarjeta de crédito" }, "title": { "id": "CREDIT_CL", "name": "Tarjeta de crédito chile" }, "alias": "Crédito multidivisa", "brandAssociation": { "id": "VISA", "name": "VISA" }, "physicalSupport": { "id": "NORMAL_PLASTIC", "name": "Plástico normal" }, "expirationDate": "**/**", "holderName": "Iván Zamorano", "currencies": [{ "currency": "CLP", "isMajor": true }, { "currency": "USD", "isMajor": false }], "grantedCredits": [{ "amount": 2000000, "currency": "CLP" }, { "amount": 5000, "currency": "USD" }], "availableBalance": { "currentBalances": [{ "amount": 1800000, "currency": "CLP" }, { "amount": 4500, "currency": "USD" }], "postedBalances": [{ "amount": 2000000, "currency": "CLP" }, { "amount": 4800, "currency": "USD" }], "pendingBalances": [{ "amount": 200000, "currency": "CLP" }, { "amount": 300, "currency": "USD" }] }, "disposedBalance": { "currentBalances": [{ "amount": 200000, "currency": "CLP" }, { "amount": 500, "currency": "USD" }], "postedBalances": [{ "amount": 200000, "currency": "CLP" }, { "amount": 200, "currency": "USD" }], "pendingBalances": [{ "amount": 0, "currency": "CLP" }, { "amount": -300, "currency": "USD" }] }, "status": { "id": "operative", "name": "Operative card" }, "images": [{ "id": "FRONT_CARD", "name": "Front face of the card", "url": "https://openapi.bbva.com/ccds/covers?pg=W1&BIN=545646&default_image=true&v=…eight=226&country=es&app_id=com.bbva.wallet&issue_date=20141001&country=cl" }, { "id": "BACK_CARD", "name": "Back face of the card", "url": "/imageBack.jpg" }], "activations": [{ "activationId": "ON_OFF", "name": "Activation for global card usage", "isActive": true }], "participants": [{ "participantId": "CHILE", "firstName": "Claudio", "lastName": "Bravo", "participantType": { "id": "AUTHORIZED", "name": "Adicional" } }], "relatedContracts": [{ "relatedContractId": "8002", "contractId": "2001", "number": "050400010100001603", "numberType": { "id": "LIC", "name": "Local Identification Code" }, "product": { "id": "ACCOUNTS", "name": "Cuenta corriente" }, "relationType": { "id": "LINKED_WITH", "name": "Linked with" } }], "isActive": true },
    { "cardId": "2101", "number": "*3283", "numberType": { "id": "PAN", "name": "Permanent Account Number" }, "cardType": { "id": "CREDIT_CARD", "name": "Tarjeta de crédito" }, "title": { "id": "CREDIT_CL", "name": "Tarjeta de crédito" }, "alias": "Tarjeta crédito", "brandAssociation": { "id": "VISA", "name": "VISA" }, "physicalSupport": { "id": "NORMAL_PLASTIC", "name": "Plástico normal" }, "expirationDate": "**/**", "holderName": "Iván Zamorano", "currencies": [{ "currency": "CLP", "isMajor": true }, { "currency": "USD", "isMajor": false }], "grantedCredits": [{ "amount": 2000000, "currency": "CLP" }, { "amount": 5000, "currency": "USD" }], "availableBalance": { "currentBalances": [{ "amount": 1800000, "currency": "CLP" }, { "amount": 4500, "currency": "USD" }], "postedBalances": [{ "amount": 2000000, "currency": "CLP" }, { "amount": 4800, "currency": "USD" }], "pendingBalances": [{ "amount": 200000, "currency": "CLP" }, { "amount": 300, "currency": "USD" }] }, "disposedBalance": { "currentBalances": [{ "amount": 200000, "currency": "CLP" }, { "amount": 500, "currency": "USD" }], "postedBalances": [{ "amount": 200000, "currency": "CLP" }, { "amount": 200, "currency": "USD" }], "pendingBalances": [{ "amount": 0, "currency": "CLP" }, { "amount": -300, "currency": "USD" }] }, "status": { "id": "operative", "name": "Operative card" }, "images": [{ "id": "FRONT_CARD", "name": "Front face of the card", "url": "https://openapi.bbva.com/ccds/covers?pg=Z1&BIN=553656&default_image=true&v=…eight=226&country=es&app_id=com.bbva.wallet&issue_date=20141001&country=cl" }, { "id": "BACK_CARD", "name": "Back face of the card", "url": "/imageBack.jpg" }], "activations": [{ "activationId": "ON_OFF", "name": "Activation for global card usage", "isActive": true }], "participants": [{ "participantId": "1234123276", "firstName": "Iván", "lastName": "Zamorano", "participantType": { "id": "HOLDER", "name": "Titular" } }], "relatedContracts": [{ "relatedContractId": "8002", "contractId": "2001", "number": "050400010100001603", "numberType": { "id": "LIC", "name": "Local Identification Code" }, "product": { "id": "ACCOUNTS", "name": "Cuenta corriente" }, "relationType": { "id": "LINKED_WITH", "name": "Linked with" } }, { "relatedContractId": "8005", "contractId": "2103", "number": "4318573837652103", "numberType": { "id": "PAN", "name": "Permanent Account Number" }, "product": { "id": "CARDS", "name": "Tarjetas" }, "relationType": { "id": "LINKED_WITH", "name": "Linked with" } }, { "relatedContractId": "8006", "contractId": "2104", "number": "4172752399835477", "numberType": { "id": "PAN", "name": "Permanent Account Number" }, "product": { "id": "CARDS", "name": "Tarjetas" }, "relationType": { "id": "LINKED_WITH", "name": "Linked with" } }], "isActive": true },
    { "cardId": "2104", "number": "*5477", "numberType": { "id": "PAN", "name": "Permanent Account Number" }, "cardType": { "id": "CREDIT_CARD", "name": "Tarjeta de crédito" }, "title": { "id": "CREDIT_CL", "name": "Tarjeta de crédito" }, "alias": "Sticker", "brandAssociation": { "id": "VISA", "name": "VISA" }, "physicalSupport": { "id": "STICKER", "name": "Sticker" }, "expirationDate": "**/**", "holderName": "Claudio Bravo", "currencies": [{ "currency": "CLP", "isMajor": true }, { "currency": "USD", "isMajor": false }], "grantedCredits": [{ "amount": 2000000, "currency": "CLP" }, { "amount": 5000, "currency": "USD" }], "availableBalance": { "currentBalances": [{ "amount": 1800000, "currency": "CLP" }, { "amount": 4500, "currency": "USD" }], "postedBalances": [{ "amount": 2000000, "currency": "CLP" }, { "amount": 4800, "currency": "USD" }], "pendingBalances": [{ "amount": 200000, "currency": "CLP" }, { "amount": 300, "currency": "USD" }] }, "disposedBalance": { "currentBalances": [{ "amount": 200000, "currency": "CLP" }, { "amount": 500, "currency": "USD" }], "postedBalances": [{ "amount": 200000, "currency": "CLP" }, { "amount": 200, "currency": "USD" }], "pendingBalances": [{ "amount": 0, "currency": "CLP" }, { "amount": -300, "currency": "USD" }] }, "status": { "id": "off", "name": "Operative card" }, "images": [{ "id": "FRONT_CARD", "name": "Front face of the card", "url": "https://openapi.bbva.com/ccds/covers?pg=Y1&BIN=490225&default_image=true&v=…eight=226&country=es&app_id=com.bbva.wallet&issue_date=20141001&country=cl" }, { "id": "BACK_CARD", "name": "Back face of the card", "url": "/imageBack.jpg" }], "activations": [{ "activationId": "ON_OFF", "name": "Activation for global card usage", "isActive": false }], "participants": [{ "participantId": "CHILE", "firstName": "Claudio", "lastName": "Bravo", "participantType": { "id": "AUTHORIZED", "name": "Adicional" } }], "relatedContracts": [{ "relatedContractId": "8004", "contractId": "2004", "number": "050400010100001603", "numberType": { "id": "LIC", "name": "Local Identification Code" }, "product": { "id": "ACCOUNTS", "name": "Cuenta corriente" }, "relationType": { "id": "LINKED_WITH", "name": "Linked with" } }], "isActive": false }]
};

var cardsSorted = {
  'CREDIT_CARDS': [
    { "cardId": "2101", "number": "*3283", "numberType": { "id": "PAN", "name": "Permanent Account Number" }, "cardType": { "id": "CREDIT_CARD", "name": "Tarjeta de crédito" }, "title": { "id": "CREDIT_CL", "name": "Tarjeta de crédito" }, "alias": "Tarjeta crédito", "brandAssociation": { "id": "VISA", "name": "VISA" }, "physicalSupport": { "id": "NORMAL_PLASTIC", "name": "Plástico normal" }, "expirationDate": "**/**", "holderName": "Iván Zamorano", "currencies": [{ "currency": "CLP", "isMajor": true }, { "currency": "USD", "isMajor": false }], "grantedCredits": [{ "amount": 2000000, "currency": "CLP" }, { "amount": 5000, "currency": "USD" }], "availableBalance": { "currentBalances": [{ "amount": 1800000, "currency": "CLP" }, { "amount": 4500, "currency": "USD" }], "postedBalances": [{ "amount": 2000000, "currency": "CLP" }, { "amount": 4800, "currency": "USD" }], "pendingBalances": [{ "amount": 200000, "currency": "CLP" }, { "amount": 300, "currency": "USD" }] }, "disposedBalance": { "currentBalances": [{ "amount": 200000, "currency": "CLP" }, { "amount": 500, "currency": "USD" }], "postedBalances": [{ "amount": 200000, "currency": "CLP" }, { "amount": 200, "currency": "USD" }], "pendingBalances": [{ "amount": 0, "currency": "CLP" }, { "amount": -300, "currency": "USD" }] }, "status": { "id": "operative", "name": "Operative card" }, "images": [{ "id": "FRONT_CARD", "name": "Front face of the card", "url": "https://openapi.bbva.com/ccds/covers?pg=Z1&BIN=553656&default_image=true&v=…eight=226&country=es&app_id=com.bbva.wallet&issue_date=20141001&country=cl" }, { "id": "BACK_CARD", "name": "Back face of the card", "url": "/imageBack.jpg" }], "activations": [{ "activationId": "ON_OFF", "name": "Activation for global card usage", "isActive": true }], "participants": [{ "participantId": "1234123276", "firstName": "Iván", "lastName": "Zamorano", "participantType": { "id": "HOLDER", "name": "Titular" } }], "relatedContracts": [{ "relatedContractId": "8002", "contractId": "2001", "number": "050400010100001603", "numberType": { "id": "LIC", "name": "Local Identification Code" }, "product": { "id": "ACCOUNTS", "name": "Cuenta corriente" }, "relationType": { "id": "LINKED_WITH", "name": "Linked with" } }, { "relatedContractId": "8005", "contractId": "2103", "number": "4318573837652103", "numberType": { "id": "PAN", "name": "Permanent Account Number" }, "product": { "id": "CARDS", "name": "Tarjetas" }, "relationType": { "id": "LINKED_WITH", "name": "Linked with" } }, { "relatedContractId": "8006", "contractId": "2104", "number": "4172752399835477", "numberType": { "id": "PAN", "name": "Permanent Account Number" }, "product": { "id": "CARDS", "name": "Tarjetas" }, "relationType": { "id": "LINKED_WITH", "name": "Linked with" } }], "isActive": true },
    { "cardId": "2104", "number": "*5477", "numberType": { "id": "PAN", "name": "Permanent Account Number" }, "cardType": { "id": "CREDIT_CARD", "name": "Tarjeta de crédito" }, "title": { "id": "CREDIT_CL", "name": "Tarjeta de crédito" }, "alias": "Sticker", "brandAssociation": { "id": "VISA", "name": "VISA" }, "physicalSupport": { "id": "STICKER", "name": "Sticker" }, "expirationDate": "**/**", "holderName": "Claudio Bravo", "currencies": [{ "currency": "CLP", "isMajor": true }, { "currency": "USD", "isMajor": false }], "grantedCredits": [{ "amount": 2000000, "currency": "CLP" }, { "amount": 5000, "currency": "USD" }], "availableBalance": { "currentBalances": [{ "amount": 1800000, "currency": "CLP" }, { "amount": 4500, "currency": "USD" }], "postedBalances": [{ "amount": 2000000, "currency": "CLP" }, { "amount": 4800, "currency": "USD" }], "pendingBalances": [{ "amount": 200000, "currency": "CLP" }, { "amount": 300, "currency": "USD" }] }, "disposedBalance": { "currentBalances": [{ "amount": 200000, "currency": "CLP" }, { "amount": 500, "currency": "USD" }], "postedBalances": [{ "amount": 200000, "currency": "CLP" }, { "amount": 200, "currency": "USD" }], "pendingBalances": [{ "amount": 0, "currency": "CLP" }, { "amount": -300, "currency": "USD" }] }, "status": { "id": "off", "name": "Operative card" }, "images": [{ "id": "FRONT_CARD", "name": "Front face of the card", "url": "https://openapi.bbva.com/ccds/covers?pg=Y1&BIN=490225&default_image=true&v=…eight=226&country=es&app_id=com.bbva.wallet&issue_date=20141001&country=cl" }, { "id": "BACK_CARD", "name": "Back face of the card", "url": "/imageBack.jpg" }], "activations": [{ "activationId": "ON_OFF", "name": "Activation for global card usage", "isActive": false }], "participants": [{ "participantId": "CHILE", "firstName": "Claudio", "lastName": "Bravo", "participantType": { "id": "AUTHORIZED", "name": "Adicional" } }], "relatedContracts": [{ "relatedContractId": "8004", "contractId": "2004", "number": "050400010100001603", "numberType": { "id": "LIC", "name": "Local Identification Code" }, "product": { "id": "ACCOUNTS", "name": "Cuenta corriente" }, "relationType": { "id": "LINKED_WITH", "name": "Linked with" } }], "isActive": false },
    { "cardId": "2103", "number": "*2103", "numberType": { "id": "PAN", "name": "Permanent Account Number" }, "cardType": { "id": "CREDIT_CARD", "name": "Tarjeta de crédito" }, "title": { "id": "CREDIT_CL", "name": "Tarjeta de crédito chile" }, "alias": "Crédito multidivisa", "brandAssociation": { "id": "VISA", "name": "VISA" }, "physicalSupport": { "id": "NORMAL_PLASTIC", "name": "Plástico normal" }, "expirationDate": "**/**", "holderName": "Iván Zamorano", "currencies": [{ "currency": "CLP", "isMajor": true }, { "currency": "USD", "isMajor": false }], "grantedCredits": [{ "amount": 2000000, "currency": "CLP" }, { "amount": 5000, "currency": "USD" }], "availableBalance": { "currentBalances": [{ "amount": 1800000, "currency": "CLP" }, { "amount": 4500, "currency": "USD" }], "postedBalances": [{ "amount": 2000000, "currency": "CLP" }, { "amount": 4800, "currency": "USD" }], "pendingBalances": [{ "amount": 200000, "currency": "CLP" }, { "amount": 300, "currency": "USD" }] }, "disposedBalance": { "currentBalances": [{ "amount": 200000, "currency": "CLP" }, { "amount": 500, "currency": "USD" }], "postedBalances": [{ "amount": 200000, "currency": "CLP" }, { "amount": 200, "currency": "USD" }], "pendingBalances": [{ "amount": 0, "currency": "CLP" }, { "amount": -300, "currency": "USD" }] }, "status": { "id": "operative", "name": "Operative card" }, "images": [{ "id": "FRONT_CARD", "name": "Front face of the card", "url": "https://openapi.bbva.com/ccds/covers?pg=W1&BIN=545646&default_image=true&v=…eight=226&country=es&app_id=com.bbva.wallet&issue_date=20141001&country=cl" }, { "id": "BACK_CARD", "name": "Back face of the card", "url": "/imageBack.jpg" }], "activations": [{ "activationId": "ON_OFF", "name": "Activation for global card usage", "isActive": true }], "participants": [{ "participantId": "CHILE", "firstName": "Claudio", "lastName": "Bravo", "participantType": { "id": "AUTHORIZED", "name": "Adicional" } }], "relatedContracts": [{ "relatedContractId": "8002", "contractId": "2001", "number": "050400010100001603", "numberType": { "id": "LIC", "name": "Local Identification Code" }, "product": { "id": "ACCOUNTS", "name": "Cuenta corriente" }, "relationType": { "id": "LINKED_WITH", "name": "Linked with" } }], "isActive": true }]
};

var rawCards = [{ "cardId": "2101", "number": "*3283", "numberType": { "id": "PAN", "name": "Permanent Account Number" }, "cardType": { "id": "CREDIT_CARD", "name": "Tarjeta de crédito" }, "title": { "id": "CREDIT_CL", "name": "Tarjeta de crédito" }, "alias": "Tarjeta crédito", "brandAssociation": { "id": "VISA", "name": "VISA" }, "physicalSupport": { "id": "NORMAL_PLASTIC", "name": "Plástico normal" }, "expirationDate": "**/**", "holderName": "Iván Zamorano", "currencies": [{ "currency": "CLP", "isMajor": true }, { "currency": "USD", "isMajor": false }], "grantedCredits": [{ "amount": 2000000, "currency": "CLP" }, { "amount": 5000, "currency": "USD" }], "availableBalance": { "currentBalances": [{ "amount": 1800000, "currency": "CLP" }, { "amount": 4500, "currency": "USD" }], "postedBalances": [{ "amount": 2000000, "currency": "CLP" }, { "amount": 4800, "currency": "USD" }], "pendingBalances": [{ "amount": 200000, "currency": "CLP" }, { "amount": 300, "currency": "USD" }] }, "disposedBalance": { "currentBalances": [{ "amount": 200000, "currency": "CLP" }, { "amount": 500, "currency": "USD" }], "postedBalances": [{ "amount": 200000, "currency": "CLP" }, { "amount": 200, "currency": "USD" }], "pendingBalances": [{ "amount": 0, "currency": "CLP" }, { "amount": -300, "currency": "USD" }] }, "status": { "id": "OPERATIVE", "name": "Operative card" }, "images": [{ "id": "FRONT_CARD", "name": "Front face of the card", "url": "https://openapi.bbva.com/ccds/covers?pg=Z1&BIN=553656&default_image=true&v=…eight=226&country=es&app_id=com.bbva.wallet&issue_date=20141001&country=cl" }, { "id": "BACK_CARD", "name": "Back face of the card", "url": "/imageBack.jpg" }], "activations": [{ "activationId": "ON_OFF", "name": "Activation for global card usage", "isActive": true }], "participants": [{ "participantId": "1234123276", "firstName": "Iván", "lastName": "Zamorano", "participantType": { "id": "HOLDER", "name": "Titular" } }], "relatedContracts": [{ "relatedContractId": "8002", "contractId": "2001", "number": "050400010100001603", "numberType": { "id": "LIC", "name": "Local Identification Code" }, "product": { "id": "ACCOUNTS", "name": "Cuenta corriente" }, "relationType": { "id": "LINKED_WITH", "name": "Linked with" } }, { "relatedContractId": "8005", "contractId": "2103", "number": "4318573837652103", "numberType": { "id": "PAN", "name": "Permanent Account Number" }, "product": { "id": "CARDS", "name": "Tarjetas" }, "relationType": { "id": "LINKED_WITH", "name": "Linked with" } }, { "relatedContractId": "8006", "contractId": "2104", "number": "4172752399835477", "numberType": { "id": "PAN", "name": "Permanent Account Number" }, "product": { "id": "CARDS", "name": "Tarjetas" }, "relationType": { "id": "LINKED_WITH", "name": "Linked with" } }] }, { "cardId": "2107", "number": "*8259", "numberType": { "id": "PAN", "name": "Permanent Account Number" }, "cardType": { "id": "CREDIT_CARD", "name": "Tarjeta de crédito" }, "title": { "id": "CREDIT_CL", "name": "Tarjeta de crédito" }, "alias": "Tarjeta crédito", "brandAssociation": { "id": "VISA", "name": "VISA" }, "physicalSupport": { "id": "NORMAL_PLASTIC", "name": "Plástico normal" }, "expirationDate": "**/**", "holderName": "Claudio Bravo", "currencies": [{ "currency": "CLP", "isMajor": true }, { "currency": "USD", "isMajor": false }], "grantedCredits": [{ "amount": 3000000, "currency": "CLP" }, { "amount": 7500, "currency": "USD" }], "availableBalance": { "currentBalances": [{ "amount": 2000000, "currency": "CLP" }, { "amount": 5000, "currency": "USD" }], "postedBalances": [{ "amount": 2000000, "currency": "CLP" }, { "amount": 5000, "currency": "USD" }], "pendingBalances": [{ "amount": 0, "currency": "CLP" }, { "amount": 0, "currency": "USD" }] }, "disposedBalance": { "currentBalances": [{ "amount": 1000000, "currency": "CLP" }, { "amount": 2500, "currency": "USD" }], "postedBalances": [{ "amount": 1000000, "currency": "CLP" }, { "amount": 2500, "currency": "USD" }], "pendingBalances": [{ "amount": 0, "currency": "CLP" }, { "amount": 0, "currency": "USD" }] }, "status": { "id": "OPERATIVE", "name": "Operative card" }, "images": [{ "id": "FRONT_CARD", "name": "Front face of the card", "url": "https://openapi.bbva.com/ccds/covers?pg=Z1&BIN=553656&default_image=true&v=…eight=226&country=es&app_id=com.bbva.wallet&issue_date=20141001&country=cl" }, { "id": "BACK_CARD", "name": "Back face of the card", "url": "/imageBack.jpg" }], "activations": [{ "activationId": "ON_OFF", "name": "Activation for global card usage", "isActive": true }], "participants": [{ "participantId": "CHILE", "firstName": "Claudio", "lastName": "Bravo", "participantType": { "id": "HOLDER", "name": "Titular" } }], "relatedContracts": [] }, { "cardId": "2108", "number": "*6501", "numberType": { "id": "PAN", "name": "Permanent Account Number" }, "cardType": { "id": "CREDIT_CARD", "name": "Tarjeta de crédito" }, "title": { "id": "CREDIT_CL", "name": "Tarjeta de crédito" }, "alias": "Tarjeta crédito", "brandAssociation": { "id": "VISA", "name": "VISA" }, "physicalSupport": { "id": "NORMAL_PLASTIC", "name": "Plástico normal" }, "expirationDate": "**/**", "holderName": "Claudio Bravo", "currencies": [{ "currency": "CLP", "isMajor": true }, { "currency": "USD", "isMajor": false }], "grantedCredits": [{ "amount": 4000000, "currency": "CLP" }, { "amount": 10000, "currency": "USD" }], "availableBalance": { "currentBalances": [{ "amount": 3000000, "currency": "CLP" }, { "amount": 7500, "currency": "USD" }], "postedBalances": [{ "amount": 3000000, "currency": "CLP" }, { "amount": 7500, "currency": "USD" }], "pendingBalances": [{ "amount": 0, "currency": "CLP" }, { "amount": 0, "currency": "USD" }] }, "disposedBalance": { "currentBalances": [{ "amount": 1000000, "currency": "CLP" }, { "amount": 2500, "currency": "USD" }], "postedBalances": [{ "amount": 1000000, "currency": "CLP" }, { "amount": 2500, "currency": "USD" }], "pendingBalances": [{ "amount": 0, "currency": "CLP" }, { "amount": 0, "currency": "USD" }] }, "status": { "id": "OPERATIVE", "name": "Operative card" }, "images": [{ "id": "FRONT_CARD", "name": "Front face of the card", "url": "https://openapi.bbva.com/ccds/covers?pg=Z1&BIN=553656&default_image=true&v=…eight=226&country=es&app_id=com.bbva.wallet&issue_date=20141001&country=cl" }, { "id": "BACK_CARD", "name": "Back face of the card", "url": "/imageBack.jpg" }], "activations": [{ "activationId": "ON_OFF", "name": "Activation for global card usage", "isActive": true }], "participants": [{ "participantId": "CHILE", "firstName": "Claudio", "lastName": "Bravo", "participantType": { "id": "HOLDER", "name": "Titular" } }], "relatedContracts": [] }, { "cardId": "2104", "number": "*5477", "numberType": { "id": "PAN", "name": "Permanent Account Number" }, "cardType": { "id": "CREDIT_CARD", "name": "Tarjeta de crédito" }, "title": { "id": "CREDIT_CL", "name": "Tarjeta de crédito" }, "alias": "Sticker", "brandAssociation": { "id": "VISA", "name": "VISA" }, "physicalSupport": { "id": "STICKER", "name": "Sticker" }, "expirationDate": "**/**", "holderName": "Claudio Bravo", "currencies": [{ "currency": "CLP", "isMajor": true }, { "currency": "USD", "isMajor": false }], "grantedCredits": [{ "amount": 2000000, "currency": "CLP" }, { "amount": 5000, "currency": "USD" }], "availableBalance": { "currentBalances": [{ "amount": 1800000, "currency": "CLP" }, { "amount": 4500, "currency": "USD" }], "postedBalances": [{ "amount": 2000000, "currency": "CLP" }, { "amount": 4800, "currency": "USD" }], "pendingBalances": [{ "amount": 200000, "currency": "CLP" }, { "amount": 300, "currency": "USD" }] }, "disposedBalance": { "currentBalances": [{ "amount": 200000, "currency": "CLP" }, { "amount": 500, "currency": "USD" }], "postedBalances": [{ "amount": 200000, "currency": "CLP" }, { "amount": 200, "currency": "USD" }], "pendingBalances": [{ "amount": 0, "currency": "CLP" }, { "amount": -300, "currency": "USD" }] }, "status": { "id": "OPERATIVE", "name": "Operative card" }, "images": [{ "id": "FRONT_CARD", "name": "Front face of the card", "url": "https://openapi.bbva.com/ccds/covers?pg=Y1&BIN=490225&default_image=true&v=…eight=226&country=es&app_id=com.bbva.wallet&issue_date=20141001&country=cl" }, { "id": "BACK_CARD", "name": "Back face of the card", "url": "/imageBack.jpg" }], "activations": [{ "activationId": "ON_OFF", "name": "Activation for global card usage", "isActive": false }], "participants": [{ "participantId": "CHILE", "firstName": "Claudio", "lastName": "Bravo", "participantType": { "id": "HOLDER", "name": "Titular" } }], "relatedContracts": [{ "relatedContractId": "8004", "contractId": "2004", "number": "050400010100001603", "numberType": { "id": "LIC", "name": "Local Identification Code" }, "product": { "id": "ACCOUNTS", "name": "Cuenta corriente" }, "relationType": { "id": "LINKED_WITH", "name": "Linked with" } }] }, { "cardId": "2103", "number": "*2103", "numberType": { "id": "PAN", "name": "Permanent Account Number" }, "cardType": { "id": "CREDIT_CARD", "name": "Tarjeta de crédito" }, "title": { "id": "CREDIT_CL", "name": "Tarjeta de crédito chile" }, "alias": "Crédito multidivisa", "brandAssociation": { "id": "VISA", "name": "VISA" }, "physicalSupport": { "id": "NORMAL_PLASTIC", "name": "Plástico normal" }, "expirationDate": "**/**", "holderName": "Iván Zamorano", "currencies": [{ "currency": "CLP", "isMajor": true }, { "currency": "USD", "isMajor": false }], "grantedCredits": [{ "amount": 2000000, "currency": "CLP" }, { "amount": 5000, "currency": "USD" }], "availableBalance": { "currentBalances": [{ "amount": 1800000, "currency": "CLP" }, { "amount": 4500, "currency": "USD" }], "postedBalances": [{ "amount": 2000000, "currency": "CLP" }, { "amount": 4800, "currency": "USD" }], "pendingBalances": [{ "amount": 200000, "currency": "CLP" }, { "amount": 300, "currency": "USD" }] }, "disposedBalance": { "currentBalances": [{ "amount": 200000, "currency": "CLP" }, { "amount": 500, "currency": "USD" }], "postedBalances": [{ "amount": 200000, "currency": "CLP" }, { "amount": 200, "currency": "USD" }], "pendingBalances": [{ "amount": 0, "currency": "CLP" }, { "amount": -300, "currency": "USD" }] }, "status": { "id": "OPERATIVE", "name": "Operative card" }, "images": [{ "id": "FRONT_CARD", "name": "Front face of the card", "url": "https://openapi.bbva.com/ccds/covers?pg=W1&BIN=545646&default_image=true&v=…eight=226&country=es&app_id=com.bbva.wallet&issue_date=20141001&country=cl" }, { "id": "BACK_CARD", "name": "Back face of the card", "url": "/imageBack.jpg" }], "activations": [{ "activationId": "ON_OFF", "name": "Activation for global card usage", "isActive": true }], "participants": [{ "participantId": "CHILE", "firstName": "Claudio", "lastName": "Bravo", "participantType": { "id": "HOLDER", "name": "Titular" } }], "relatedContracts": [{ "relatedContractId": "8002", "contractId": "2001", "number": "050400010100001603", "numberType": { "id": "LIC", "name": "Local Identification Code" }, "product": { "id": "ACCOUNTS", "name": "Cuenta corriente" }, "relationType": { "id": "LINKED_WITH", "name": "Linked with" } }] }];

var formattedCards = { "CREDIT_CARD": [{ "cardId": "2101", "number": "*3283", "numberType": { "id": "PAN", "name": "Permanent Account Number" }, "cardType": { "id": "CREDIT_CARD", "name": "Tarjeta de crédito" }, "title": { "id": "CREDIT_CL", "name": "Tarjeta de crédito" }, "alias": "Tarjeta crédito", "brandAssociation": { "id": "VISA", "name": "VISA" }, "physicalSupport": { "id": "NORMAL_PLASTIC", "name": "Plástico normal" }, "expirationDate": "**/**", "holderName": "Iván Zamorano", "currencies": [{ "currency": "CLP", "isMajor": true }, { "currency": "USD", "isMajor": false }], "grantedCredits": [{ "amount": 2000000, "currency": "CLP" }, { "amount": 5000, "currency": "USD" }], "availableBalance": { "currentBalances": [{ "amount": 1800000, "currency": "CLP" }, { "amount": 4500, "currency": "USD" }], "postedBalances": [{ "amount": 2000000, "currency": "CLP" }, { "amount": 4800, "currency": "USD" }], "pendingBalances": [{ "amount": 200000, "currency": "CLP" }, { "amount": 300, "currency": "USD" }] }, "disposedBalance": { "currentBalances": [{ "amount": 200000, "currency": "CLP" }, { "amount": 500, "currency": "USD" }], "postedBalances": [{ "amount": 200000, "currency": "CLP" }, { "amount": 200, "currency": "USD" }], "pendingBalances": [{ "amount": 0, "currency": "CLP" }, { "amount": -300, "currency": "USD" }] }, "status": { "id": "operative", "name": "Operative card" }, "images": [{ "id": "FRONT_CARD", "name": "Front face of the card", "url": "https://openapi.bbva.com/ccds/covers?pg=Z1&BIN=553656&default_image=true&v=4&width=360&height=226&country=es&app_id=com.bbva.wallet&issue_date=20141001&country=cl" }, { "id": "BACK_CARD", "name": "Back face of the card", "url": "/imageBack.jpg" }], "activations": [{ "activationId": "ON_OFF", "name": "Activation for global card usage", "isActive": true }], "participants": [{ "participantId": "1234123276", "firstName": "Iván", "lastName": "Zamorano", "participantType": { "id": "HOLDER", "name": "Titular" } }], "relatedContracts": [{ "relatedContractId": "8002", "contractId": "2001", "number": "050400010100001603", "numberType": { "id": "LIC", "name": "Local Identification Code" }, "product": { "id": "ACCOUNTS", "name": "Cuenta corriente" }, "relationType": { "id": "LINKED_WITH", "name": "Linked with" } }, { "relatedContractId": "8005", "contractId": "2103", "number": "4318573837652103", "numberType": { "id": "PAN", "name": "Permanent Account Number" }, "product": { "id": "CARDS", "name": "Tarjetas" }, "relationType": { "id": "LINKED_WITH", "name": "Linked with" } }, { "relatedContractId": "8006", "contractId": "2104", "number": "4172752399835477", "numberType": { "id": "PAN", "name": "Permanent Account Number" }, "product": { "id": "CARDS", "name": "Tarjetas" }, "relationType": { "id": "LINKED_WITH", "name": "Linked with" } }], "isActive": true }, { "cardId": "2107", "number": "*8259", "numberType": { "id": "PAN", "name": "Permanent Account Number" }, "cardType": { "id": "CREDIT_CARD", "name": "Tarjeta de crédito" }, "title": { "id": "CREDIT_CL", "name": "Tarjeta de crédito" }, "alias": "Tarjeta crédito", "brandAssociation": { "id": "VISA", "name": "VISA" }, "physicalSupport": { "id": "NORMAL_PLASTIC", "name": "Plástico normal" }, "expirationDate": "**/**", "holderName": "Claudio Bravo", "currencies": [{ "currency": "CLP", "isMajor": true }, { "currency": "USD", "isMajor": false }], "grantedCredits": [{ "amount": 3000000, "currency": "CLP" }, { "amount": 7500, "currency": "USD" }], "availableBalance": { "currentBalances": [{ "amount": 2000000, "currency": "CLP" }, { "amount": 5000, "currency": "USD" }], "postedBalances": [{ "amount": 2000000, "currency": "CLP" }, { "amount": 5000, "currency": "USD" }], "pendingBalances": [{ "amount": 0, "currency": "CLP" }, { "amount": 0, "currency": "USD" }] }, "disposedBalance": { "currentBalances": [{ "amount": 1000000, "currency": "CLP" }, { "amount": 2500, "currency": "USD" }], "postedBalances": [{ "amount": 1000000, "currency": "CLP" }, { "amount": 2500, "currency": "USD" }], "pendingBalances": [{ "amount": 0, "currency": "CLP" }, { "amount": 0, "currency": "USD" }] }, "status": { "id": "operative", "name": "Operative card" }, "images": [{ "id": "FRONT_CARD", "name": "Front face of the card", "url": "https://openapi.bbva.com/ccds/covers?pg=Z1&BIN=553656&default_image=true&v=4&width=360&height=226&country=es&app_id=com.bbva.wallet&issue_date=20141001&country=cl" }, { "id": "BACK_CARD", "name": "Back face of the card", "url": "/imageBack.jpg" }], "activations": [{ "activationId": "ON_OFF", "name": "Activation for global card usage", "isActive": true }], "participants": [{ "participantId": "CHILE", "firstName": "Claudio", "lastName": "Bravo", "participantType": { "id": "HOLDER", "name": "Titular" } }], "relatedContracts": [], "isActive": true }, { "cardId": "2108", "number": "*6501", "numberType": { "id": "PAN", "name": "Permanent Account Number" }, "cardType": { "id": "CREDIT_CARD", "name": "Tarjeta de crédito" }, "title": { "id": "CREDIT_CL", "name": "Tarjeta de crédito" }, "alias": "Tarjeta crédito", "brandAssociation": { "id": "VISA", "name": "VISA" }, "physicalSupport": { "id": "NORMAL_PLASTIC", "name": "Plástico normal" }, "expirationDate": "**/**", "holderName": "Claudio Bravo", "currencies": [{ "currency": "CLP", "isMajor": true }, { "currency": "USD", "isMajor": false }], "grantedCredits": [{ "amount": 4000000, "currency": "CLP" }, { "amount": 10000, "currency": "USD" }], "availableBalance": { "currentBalances": [{ "amount": 3000000, "currency": "CLP" }, { "amount": 7500, "currency": "USD" }], "postedBalances": [{ "amount": 3000000, "currency": "CLP" }, { "amount": 7500, "currency": "USD" }], "pendingBalances": [{ "amount": 0, "currency": "CLP" }, { "amount": 0, "currency": "USD" }] }, "disposedBalance": { "currentBalances": [{ "amount": 1000000, "currency": "CLP" }, { "amount": 2500, "currency": "USD" }], "postedBalances": [{ "amount": 1000000, "currency": "CLP" }, { "amount": 2500, "currency": "USD" }], "pendingBalances": [{ "amount": 0, "currency": "CLP" }, { "amount": 0, "currency": "USD" }] }, "status": { "id": "operative", "name": "Operative card" }, "images": [{ "id": "FRONT_CARD", "name": "Front face of the card", "url": "https://openapi.bbva.com/ccds/covers?pg=Z1&BIN=553656&default_image=true&v=4&width=360&height=226&country=es&app_id=com.bbva.wallet&issue_date=20141001&country=cl" }, { "id": "BACK_CARD", "name": "Back face of the card", "url": "/imageBack.jpg" }], "activations": [{ "activationId": "ON_OFF", "name": "Activation for global card usage", "isActive": true }], "participants": [{ "participantId": "CHILE", "firstName": "Claudio", "lastName": "Bravo", "participantType": { "id": "HOLDER", "name": "Titular" } }], "relatedContracts": [], "isActive": true }, { "cardId": "2104", "number": "*5477", "numberType": { "id": "PAN", "name": "Permanent Account Number" }, "cardType": { "id": "CREDIT_CARD", "name": "Tarjeta de crédito" }, "title": { "id": "CREDIT_CL", "name": "Tarjeta de crédito" }, "alias": "Sticker", "brandAssociation": { "id": "VISA", "name": "VISA" }, "physicalSupport": { "id": "STICKER", "name": "Sticker" }, "expirationDate": "**/**", "holderName": "Claudio Bravo", "currencies": [{ "currency": "CLP", "isMajor": true }, { "currency": "USD", "isMajor": false }], "grantedCredits": [{ "amount": 2000000, "currency": "CLP" }, { "amount": 5000, "currency": "USD" }], "availableBalance": { "currentBalances": [{ "amount": 1800000, "currency": "CLP" }, { "amount": 4500, "currency": "USD" }], "postedBalances": [{ "amount": 2000000, "currency": "CLP" }, { "amount": 4800, "currency": "USD" }], "pendingBalances": [{ "amount": 200000, "currency": "CLP" }, { "amount": 300, "currency": "USD" }] }, "disposedBalance": { "currentBalances": [{ "amount": 200000, "currency": "CLP" }, { "amount": 500, "currency": "USD" }], "postedBalances": [{ "amount": 200000, "currency": "CLP" }, { "amount": 200, "currency": "USD" }], "pendingBalances": [{ "amount": 0, "currency": "CLP" }, { "amount": -300, "currency": "USD" }] }, "status": { "id": "off", "name": "Operative card" }, "images": [{ "id": "FRONT_CARD", "name": "Front face of the card", "url": "https://openapi.bbva.com/ccds/covers?pg=Y1&BIN=490225&default_image=true&v=4&width=360&height=226&country=es&app_id=com.bbva.wallet&issue_date=20141001&country=cl" }, { "id": "BACK_CARD", "name": "Back face of the card", "url": "/imageBack.jpg" }], "activations": [{ "activationId": "ON_OFF", "name": "Activation for global card usage", "isActive": false }], "participants": [{ "participantId": "CHILE", "firstName": "Claudio", "lastName": "Bravo", "participantType": { "id": "HOLDER", "name": "Titular" } }], "relatedContracts": [{ "relatedContractId": "8004", "contractId": "2004", "number": "050400010100001603", "numberType": { "id": "LIC", "name": "Local Identification Code" }, "product": { "id": "ACCOUNTS", "name": "Cuenta corriente" }, "relationType": { "id": "LINKED_WITH", "name": "Linked with" } }], "isActive": false }, { "cardId": "2103", "number": "*2103", "numberType": { "id": "PAN", "name": "Permanent Account Number" }, "cardType": { "id": "CREDIT_CARD", "name": "Tarjeta de crédito" }, "title": { "id": "CREDIT_CL", "name": "Tarjeta de crédito chile" }, "alias": "Crédito multidivisa", "brandAssociation": { "id": "VISA", "name": "VISA" }, "physicalSupport": { "id": "NORMAL_PLASTIC", "name": "Plástico normal" }, "expirationDate": "**/**", "holderName": "Iván Zamorano", "currencies": [{ "currency": "CLP", "isMajor": true }, { "currency": "USD", "isMajor": false }], "grantedCredits": [{ "amount": 2000000, "currency": "CLP" }, { "amount": 5000, "currency": "USD" }], "availableBalance": { "currentBalances": [{ "amount": 1800000, "currency": "CLP" }, { "amount": 4500, "currency": "USD" }], "postedBalances": [{ "amount": 2000000, "currency": "CLP" }, { "amount": 4800, "currency": "USD" }], "pendingBalances": [{ "amount": 200000, "currency": "CLP" }, { "amount": 300, "currency": "USD" }] }, "disposedBalance": { "currentBalances": [{ "amount": 200000, "currency": "CLP" }, { "amount": 500, "currency": "USD" }], "postedBalances": [{ "amount": 200000, "currency": "CLP" }, { "amount": 200, "currency": "USD" }], "pendingBalances": [{ "amount": 0, "currency": "CLP" }, { "amount": -300, "currency": "USD" }] }, "status": { "id": "operative", "name": "Operative card" }, "images": [{ "id": "FRONT_CARD", "name": "Front face of the card", "url": "https://openapi.bbva.com/ccds/covers?pg=W1&BIN=545646&default_image=true&v=4&width=360&height=226&country=es&app_id=com.bbva.wallet&issue_date=20141001&country=cl" }, { "id": "BACK_CARD", "name": "Back face of the card", "url": "/imageBack.jpg" }], "activations": [{ "activationId": "ON_OFF", "name": "Activation for global card usage", "isActive": true }], "participants": [{ "participantId": "CHILE", "firstName": "Claudio", "lastName": "Bravo", "participantType": { "id": "HOLDER", "name": "Titular" } }], "relatedContracts": [{ "relatedContractId": "8002", "contractId": "2001", "number": "050400010100001603", "numberType": { "id": "LIC", "name": "Local Identification Code" }, "product": { "id": "ACCOUNTS", "name": "Cuenta corriente" }, "relationType": { "id": "LINKED_WITH", "name": "Linked with" } }], "isActive": true }] };

var sorteredCards = {"CREDIT_CARD":[{"cardId":"2101","number":"*3283","numberType":{"id":"PAN","name":"Permanent Account Number"},"cardType":{"id":"CREDIT_CARD","name":"Tarjeta de crédito"},"title":{"id":"CREDIT_CL","name":"Tarjeta de crédito"},"alias":"Tarjeta crédito","brandAssociation":{"id":"VISA","name":"VISA"},"physicalSupport":{"id":"NORMAL_PLASTIC","name":"Plástico normal"},"expirationDate":"**/**","holderName":"Iván Zamorano","currencies":[{"currency":"CLP","isMajor":true},{"currency":"USD","isMajor":false}],"grantedCredits":[{"amount":2000000,"currency":"CLP"},{"amount":5000,"currency":"USD"}],"availableBalance":{"currentBalances":[{"amount":1800000,"currency":"CLP"},{"amount":4500,"currency":"USD"}],"postedBalances":[{"amount":2000000,"currency":"CLP"},{"amount":4800,"currency":"USD"}],"pendingBalances":[{"amount":200000,"currency":"CLP"},{"amount":300,"currency":"USD"}]},"disposedBalance":{"currentBalances":[{"amount":200000,"currency":"CLP"},{"amount":500,"currency":"USD"}],"postedBalances":[{"amount":200000,"currency":"CLP"},{"amount":200,"currency":"USD"}],"pendingBalances":[{"amount":0,"currency":"CLP"},{"amount":-300,"currency":"USD"}]},"status":{"id":"operative","name":"Operative card"},"images":[{"id":"FRONT_CARD","name":"Front face of the card","url":"https://openapi.bbva.com/ccds/covers?pg=Z1&BIN=553656&default_image=true&v=…eight=226&country=es&app_id=com.bbva.wallet&issue_date=20141001&country=cl"},{"id":"BACK_CARD","name":"Back face of the card","url":"/imageBack.jpg"}],"activations":[{"activationId":"ON_OFF","name":"Activation for global card usage","isActive":true}],"participants":[{"participantId":"1234123276","firstName":"Iván","lastName":"Zamorano","participantType":{"id":"HOLDER","name":"Titular"}}],"relatedContracts":[{"relatedContractId":"8002","contractId":"2001","number":"050400010100001603","numberType":{"id":"LIC","name":"Local Identification Code"},"product":{"id":"ACCOUNTS","name":"Cuenta corriente"},"relationType":{"id":"LINKED_WITH","name":"Linked with"}},{"relatedContractId":"8005","contractId":"2103","number":"4318573837652103","numberType":{"id":"PAN","name":"Permanent Account Number"},"product":{"id":"CARDS","name":"Tarjetas"},"relationType":{"id":"LINKED_WITH","name":"Linked with"}},{"relatedContractId":"8006","contractId":"2104","number":"4172752399835477","numberType":{"id":"PAN","name":"Permanent Account Number"},"product":{"id":"CARDS","name":"Tarjetas"},"relationType":{"id":"LINKED_WITH","name":"Linked with"}}],"isActive":true},{"cardId":"2104","number":"*5477","numberType":{"id":"PAN","name":"Permanent Account Number"},"cardType":{"id":"CREDIT_CARD","name":"Tarjeta de crédito"},"title":{"id":"CREDIT_CL","name":"Tarjeta de crédito"},"alias":"Sticker","brandAssociation":{"id":"VISA","name":"VISA"},"physicalSupport":{"id":"STICKER","name":"Sticker"},"expirationDate":"**/**","holderName":"Claudio Bravo","currencies":[{"currency":"CLP","isMajor":true},{"currency":"USD","isMajor":false}],"grantedCredits":[{"amount":2000000,"currency":"CLP"},{"amount":5000,"currency":"USD"}],"availableBalance":{"currentBalances":[{"amount":1800000,"currency":"CLP"},{"amount":4500,"currency":"USD"}],"postedBalances":[{"amount":2000000,"currency":"CLP"},{"amount":4800,"currency":"USD"}],"pendingBalances":[{"amount":200000,"currency":"CLP"},{"amount":300,"currency":"USD"}]},"disposedBalance":{"currentBalances":[{"amount":200000,"currency":"CLP"},{"amount":500,"currency":"USD"}],"postedBalances":[{"amount":200000,"currency":"CLP"},{"amount":200,"currency":"USD"}],"pendingBalances":[{"amount":0,"currency":"CLP"},{"amount":-300,"currency":"USD"}]},"status":{"id":"off","name":"Operative card"},"images":[{"id":"FRONT_CARD","name":"Front face of the card","url":"https://openapi.bbva.com/ccds/covers?pg=Y1&BIN=490225&default_image=true&v=…eight=226&country=es&app_id=com.bbva.wallet&issue_date=20141001&country=cl"},{"id":"BACK_CARD","name":"Back face of the card","url":"/imageBack.jpg"}],"activations":[{"activationId":"ON_OFF","name":"Activation for global card usage","isActive":false}],"participants":[{"participantId":"CHILE","firstName":"Claudio","lastName":"Bravo","participantType":{"id":"HOLDER","name":"Titular"}}],"relatedContracts":[{"relatedContractId":"8004","contractId":"2004","number":"050400010100001603","numberType":{"id":"LIC","name":"Local Identification Code"},"product":{"id":"ACCOUNTS","name":"Cuenta corriente"},"relationType":{"id":"LINKED_WITH","name":"Linked with"}}],"isActive":false,"isFirstChild":true,"isChild":true},{"cardId":"2103","number":"*2103","numberType":{"id":"PAN","name":"Permanent Account Number"},"cardType":{"id":"CREDIT_CARD","name":"Tarjeta de crédito"},"title":{"id":"CREDIT_CL","name":"Tarjeta de crédito chile"},"alias":"Crédito multidivisa","brandAssociation":{"id":"VISA","name":"VISA"},"physicalSupport":{"id":"NORMAL_PLASTIC","name":"Plástico normal"},"expirationDate":"**/**","holderName":"Iván Zamorano","currencies":[{"currency":"CLP","isMajor":true},{"currency":"USD","isMajor":false}],"grantedCredits":[{"amount":2000000,"currency":"CLP"},{"amount":5000,"currency":"USD"}],"availableBalance":{"currentBalances":[{"amount":1800000,"currency":"CLP"},{"amount":4500,"currency":"USD"}],"postedBalances":[{"amount":2000000,"currency":"CLP"},{"amount":4800,"currency":"USD"}],"pendingBalances":[{"amount":200000,"currency":"CLP"},{"amount":300,"currency":"USD"}]},"disposedBalance":{"currentBalances":[{"amount":200000,"currency":"CLP"},{"amount":500,"currency":"USD"}],"postedBalances":[{"amount":200000,"currency":"CLP"},{"amount":200,"currency":"USD"}],"pendingBalances":[{"amount":0,"currency":"CLP"},{"amount":-300,"currency":"USD"}]},"status":{"id":"operative","name":"Operative card"},"images":[{"id":"FRONT_CARD","name":"Front face of the card","url":"https://openapi.bbva.com/ccds/covers?pg=W1&BIN=545646&default_image=true&v=…eight=226&country=es&app_id=com.bbva.wallet&issue_date=20141001&country=cl"},{"id":"BACK_CARD","name":"Back face of the card","url":"/imageBack.jpg"}],"activations":[{"activationId":"ON_OFF","name":"Activation for global card usage","isActive":true}],"participants":[{"participantId":"CHILE","firstName":"Claudio","lastName":"Bravo","participantType":{"id":"HOLDER","name":"Titular"}}],"relatedContracts":[{"relatedContractId":"8002","contractId":"2001","number":"050400010100001603","numberType":{"id":"LIC","name":"Local Identification Code"},"product":{"id":"ACCOUNTS","name":"Cuenta corriente"},"relationType":{"id":"LINKED_WITH","name":"Linked with"}}],"isActive":true,"isChild":true},{"cardId":"2107","number":"*8259","numberType":{"id":"PAN","name":"Permanent Account Number"},"cardType":{"id":"CREDIT_CARD","name":"Tarjeta de crédito"},"title":{"id":"CREDIT_CL","name":"Tarjeta de crédito"},"alias":"Tarjeta crédito","brandAssociation":{"id":"VISA","name":"VISA"},"physicalSupport":{"id":"NORMAL_PLASTIC","name":"Plástico normal"},"expirationDate":"**/**","holderName":"Claudio Bravo","currencies":[{"currency":"CLP","isMajor":true},{"currency":"USD","isMajor":false}],"grantedCredits":[{"amount":3000000,"currency":"CLP"},{"amount":7500,"currency":"USD"}],"availableBalance":{"currentBalances":[{"amount":2000000,"currency":"CLP"},{"amount":5000,"currency":"USD"}],"postedBalances":[{"amount":2000000,"currency":"CLP"},{"amount":5000,"currency":"USD"}],"pendingBalances":[{"amount":0,"currency":"CLP"},{"amount":0,"currency":"USD"}]},"disposedBalance":{"currentBalances":[{"amount":1000000,"currency":"CLP"},{"amount":2500,"currency":"USD"}],"postedBalances":[{"amount":1000000,"currency":"CLP"},{"amount":2500,"currency":"USD"}],"pendingBalances":[{"amount":0,"currency":"CLP"},{"amount":0,"currency":"USD"}]},"status":{"id":"operative","name":"Operative card"},"images":[{"id":"FRONT_CARD","name":"Front face of the card","url":"https://openapi.bbva.com/ccds/covers?pg=Z1&BIN=553656&default_image=true&v=…eight=226&country=es&app_id=com.bbva.wallet&issue_date=20141001&country=cl"},{"id":"BACK_CARD","name":"Back face of the card","url":"/imageBack.jpg"}],"activations":[{"activationId":"ON_OFF","name":"Activation for global card usage","isActive":true}],"participants":[{"participantId":"CHILE","firstName":"Claudio","lastName":"Bravo","participantType":{"id":"HOLDER","name":"Titular"}}],"relatedContracts":[],"isActive":true},{"cardId":"2108","number":"*6501","numberType":{"id":"PAN","name":"Permanent Account Number"},"cardType":{"id":"CREDIT_CARD","name":"Tarjeta de crédito"},"title":{"id":"CREDIT_CL","name":"Tarjeta de crédito"},"alias":"Tarjeta crédito","brandAssociation":{"id":"VISA","name":"VISA"},"physicalSupport":{"id":"NORMAL_PLASTIC","name":"Plástico normal"},"expirationDate":"**/**","holderName":"Claudio Bravo","currencies":[{"currency":"CLP","isMajor":true},{"currency":"USD","isMajor":false}],"grantedCredits":[{"amount":4000000,"currency":"CLP"},{"amount":10000,"currency":"USD"}],"availableBalance":{"currentBalances":[{"amount":3000000,"currency":"CLP"},{"amount":7500,"currency":"USD"}],"postedBalances":[{"amount":3000000,"currency":"CLP"},{"amount":7500,"currency":"USD"}],"pendingBalances":[{"amount":0,"currency":"CLP"},{"amount":0,"currency":"USD"}]},"disposedBalance":{"currentBalances":[{"amount":1000000,"currency":"CLP"},{"amount":2500,"currency":"USD"}],"postedBalances":[{"amount":1000000,"currency":"CLP"},{"amount":2500,"currency":"USD"}],"pendingBalances":[{"amount":0,"currency":"CLP"},{"amount":0,"currency":"USD"}]},"status":{"id":"operative","name":"Operative card"},"images":[{"id":"FRONT_CARD","name":"Front face of the card","url":"https://openapi.bbva.com/ccds/covers?pg=Z1&BIN=553656&default_image=true&v=…eight=226&country=es&app_id=com.bbva.wallet&issue_date=20141001&country=cl"},{"id":"BACK_CARD","name":"Back face of the card","url":"/imageBack.jpg"}],"activations":[{"activationId":"ON_OFF","name":"Activation for global card usage","isActive":true}],"participants":[{"participantId":"CHILE","firstName":"Claudio","lastName":"Bravo","participantType":{"id":"HOLDER","name":"Titular"}}],"relatedContracts":[],"isActive":true},{"cardId":"2104","number":"*5477","numberType":{"id":"PAN","name":"Permanent Account Number"},"cardType":{"id":"CREDIT_CARD","name":"Tarjeta de crédito"},"title":{"id":"CREDIT_CL","name":"Tarjeta de crédito"},"alias":"Sticker","brandAssociation":{"id":"VISA","name":"VISA"},"physicalSupport":{"id":"STICKER","name":"Sticker"},"expirationDate":"**/**","holderName":"Claudio Bravo","currencies":[{"currency":"CLP","isMajor":true},{"currency":"USD","isMajor":false}],"grantedCredits":[{"amount":2000000,"currency":"CLP"},{"amount":5000,"currency":"USD"}],"availableBalance":{"currentBalances":[{"amount":1800000,"currency":"CLP"},{"amount":4500,"currency":"USD"}],"postedBalances":[{"amount":2000000,"currency":"CLP"},{"amount":4800,"currency":"USD"}],"pendingBalances":[{"amount":200000,"currency":"CLP"},{"amount":300,"currency":"USD"}]},"disposedBalance":{"currentBalances":[{"amount":200000,"currency":"CLP"},{"amount":500,"currency":"USD"}],"postedBalances":[{"amount":200000,"currency":"CLP"},{"amount":200,"currency":"USD"}],"pendingBalances":[{"amount":0,"currency":"CLP"},{"amount":-300,"currency":"USD"}]},"status":{"id":"off","name":"Operative card"},"images":[{"id":"FRONT_CARD","name":"Front face of the card","url":"https://openapi.bbva.com/ccds/covers?pg=Y1&BIN=490225&default_image=true&v=…eight=226&country=es&app_id=com.bbva.wallet&issue_date=20141001&country=cl"},{"id":"BACK_CARD","name":"Back face of the card","url":"/imageBack.jpg"}],"activations":[{"activationId":"ON_OFF","name":"Activation for global card usage","isActive":false}],"participants":[{"participantId":"CHILE","firstName":"Claudio","lastName":"Bravo","participantType":{"id":"HOLDER","name":"Titular"}}],"relatedContracts":[{"relatedContractId":"8004","contractId":"2004","number":"050400010100001603","numberType":{"id":"LIC","name":"Local Identification Code"},"product":{"id":"ACCOUNTS","name":"Cuenta corriente"},"relationType":{"id":"LINKED_WITH","name":"Linked with"}}],"isActive":false,"isFirstChild":true,"isChild":true},{"cardId":"2103","number":"*2103","numberType":{"id":"PAN","name":"Permanent Account Number"},"cardType":{"id":"CREDIT_CARD","name":"Tarjeta de crédito"},"title":{"id":"CREDIT_CL","name":"Tarjeta de crédito chile"},"alias":"Crédito multidivisa","brandAssociation":{"id":"VISA","name":"VISA"},"physicalSupport":{"id":"NORMAL_PLASTIC","name":"Plástico normal"},"expirationDate":"**/**","holderName":"Iván Zamorano","currencies":[{"currency":"CLP","isMajor":true},{"currency":"USD","isMajor":false}],"grantedCredits":[{"amount":2000000,"currency":"CLP"},{"amount":5000,"currency":"USD"}],"availableBalance":{"currentBalances":[{"amount":1800000,"currency":"CLP"},{"amount":4500,"currency":"USD"}],"postedBalances":[{"amount":2000000,"currency":"CLP"},{"amount":4800,"currency":"USD"}],"pendingBalances":[{"amount":200000,"currency":"CLP"},{"amount":300,"currency":"USD"}]},"disposedBalance":{"currentBalances":[{"amount":200000,"currency":"CLP"},{"amount":500,"currency":"USD"}],"postedBalances":[{"amount":200000,"currency":"CLP"},{"amount":200,"currency":"USD"}],"pendingBalances":[{"amount":0,"currency":"CLP"},{"amount":-300,"currency":"USD"}]},"status":{"id":"operative","name":"Operative card"},"images":[{"id":"FRONT_CARD","name":"Front face of the card","url":"https://openapi.bbva.com/ccds/covers?pg=W1&BIN=545646&default_image=true&v=…eight=226&country=es&app_id=com.bbva.wallet&issue_date=20141001&country=cl"},{"id":"BACK_CARD","name":"Back face of the card","url":"/imageBack.jpg"}],"activations":[{"activationId":"ON_OFF","name":"Activation for global card usage","isActive":true}],"participants":[{"participantId":"CHILE","firstName":"Claudio","lastName":"Bravo","participantType":{"id":"HOLDER","name":"Titular"}}],"relatedContracts":[{"relatedContractId":"8002","contractId":"2001","number":"050400010100001603","numberType":{"id":"LIC","name":"Local Identification Code"},"product":{"id":"ACCOUNTS","name":"Cuenta corriente"},"relationType":{"id":"LINKED_WITH","name":"Linked with"}}],"isActive":true,"isChild":true}]};

var availableBalanceCards = [
  {
    "currencies": [
      {
        "currency": "CLP",
        "isMajor": true
      },
      {
        "currency": "USD",
        "isMajor": false
      }
    ],
    "description": {
      "value": "*3283",
      "masked": true
    },
    "id": "2101",
    "name": "Tarjeta crédito",
    "number": "*3283",
    "primaryAmount": {
      "amount": 1800000,
      "currency": "CLP",
      "label": "cells-global-dashboard-cards-available"
    },
    "secondaryAmount": {
      "amount": 1800000,
      "currency": "CLP",
      "label": "cells-global-dashboard-cards-available"
    },
    "imgSrc": "https://openapi.bbva.com/ccds/covers?pg=Z1&BIN=553656&default_image=true&v=…eight=226&country=es&app_id=com.bbva.wallet&issue_date=20141001&country=cl"
  },
  {
    "currencies": [
      {
        "currency": "CLP",
        "isMajor": true
      },
      {
        "currency": "USD",
        "isMajor": false
      }
    ],
    "description": {
      "value": "*5477",
      "masked": true
    },
    "id": "2104",
    "name": "Sticker",
    "number": "*5477",
    "primaryAmount": {
      "amount": 1800000,
      "currency": "CLP",
      "label": "cells-global-dashboard-cards-available"
    },
    "secondaryAmount": {
      "amount": 1800000,
      "currency": "CLP",
      "label": "cells-global-dashboard-cards-available"
    },
    "isChildClass": "card-child",
    "isChild": true,
    "isFirstChildClass": "card-first-child",
    "isFirstChild": true,
    "imgSrc": "https://openapi.bbva.com/ccds/covers?pg=Y1&BIN=490225&default_image=true&v=…eight=226&country=es&app_id=com.bbva.wallet&issue_date=20141001&country=cl"
  },
  {
    "currencies": [
      {
        "currency": "CLP",
        "isMajor": true
      },
      {
        "currency": "USD",
        "isMajor": false
      }
    ],
    "description": {
      "value": "*2103",
      "masked": true
    },
    "id": "2103",
    "name": "Crédito multidivisa",
    "number": "*2103",
    "primaryAmount": {
      "amount": 1800000,
      "currency": "CLP",
      "label": "cells-global-dashboard-cards-available"
    },
    "secondaryAmount": {
      "amount": 1800000,
      "currency": "CLP",
      "label": "cells-global-dashboard-cards-available"
    },
    "isChildClass": "card-child",
    "isChild": true,
    "imgSrc": "https://openapi.bbva.com/ccds/covers?pg=W1&BIN=545646&default_image=true&v=…eight=226&country=es&app_id=com.bbva.wallet&issue_date=20141001&country=cl"
  },
  {
    "currencies": [
      {
        "currency": "CLP",
        "isMajor": true
      },
      {
        "currency": "USD",
        "isMajor": false
      }
    ],
    "description": {
      "value": "*8259",
      "masked": true
    },
    "id": "2107",
    "name": "Tarjeta crédito",
    "number": "*8259",
    "primaryAmount": {
      "amount": 2000000,
      "currency": "CLP",
      "label": "cells-global-dashboard-cards-available"
    },
    "secondaryAmount": {
      "amount": 2000000,
      "currency": "CLP",
      "label": "cells-global-dashboard-cards-available"
    },
    "imgSrc": "https://openapi.bbva.com/ccds/covers?pg=Z1&BIN=553656&default_image=true&v=…eight=226&country=es&app_id=com.bbva.wallet&issue_date=20141001&country=cl"
  },
  {
    "currencies": [
      {
        "currency": "CLP",
        "isMajor": true
      },
      {
        "currency": "USD",
        "isMajor": false
      }
    ],
    "description": {
      "value": "*6501",
      "masked": true
    },
    "id": "2108",
    "name": "Tarjeta crédito",
    "number": "*6501",
    "primaryAmount": {
      "amount": 3000000,
      "currency": "CLP",
      "label": "cells-global-dashboard-cards-available"
    },
    "secondaryAmount": {
      "amount": 3000000,
      "currency": "CLP",
      "label": "cells-global-dashboard-cards-available"
    },
    "imgSrc": "https://openapi.bbva.com/ccds/covers?pg=Z1&BIN=553656&default_image=true&v=…eight=226&country=es&app_id=com.bbva.wallet&issue_date=20141001&country=cl"
  },
  {
    "currencies": [
      {
        "currency": "CLP",
        "isMajor": true
      },
      {
        "currency": "USD",
        "isMajor": false
      }
    ],
    "description": {
      "value": "*5477",
      "masked": true
    },
    "id": "2104",
    "name": "Sticker",
    "number": "*5477",
    "primaryAmount": {
      "amount": 1800000,
      "currency": "CLP",
      "label": "cells-global-dashboard-cards-available"
    },
    "secondaryAmount": {
      "amount": 1800000,
      "currency": "CLP",
      "label": "cells-global-dashboard-cards-available"
    },
    "isChildClass": "card-child",
    "isChild": true,
    "isFirstChildClass": "card-first-child",
    "isFirstChild": true,
    "imgSrc": "https://openapi.bbva.com/ccds/covers?pg=Y1&BIN=490225&default_image=true&v=…eight=226&country=es&app_id=com.bbva.wallet&issue_date=20141001&country=cl"
  },
  {
    "currencies": [
      {
        "currency": "CLP",
        "isMajor": true
      },
      {
        "currency": "USD",
        "isMajor": false
      }
    ],
    "description": {
      "value": "*2103",
      "masked": true
    },
    "id": "2103",
    "name": "Crédito multidivisa",
    "number": "*2103",
    "primaryAmount": {
      "amount": 1800000,
      "currency": "CLP",
      "label": "cells-global-dashboard-cards-available"
    },
    "secondaryAmount": {
      "amount": 1800000,
      "currency": "CLP",
      "label": "cells-global-dashboard-cards-available"
    },
    "isChildClass": "card-child",
    "isChild": true,
    "imgSrc": "https://openapi.bbva.com/ccds/covers?pg=W1&BIN=545646&default_image=true&v=…eight=226&country=es&app_id=com.bbva.wallet&issue_date=20141001&country=cl"
  }
];

var grantedCreditsCards = [
  {
    "currencies": [
      {
        "currency": "CLP",
        "isMajor": true
      },
      {
        "currency": "USD",
        "isMajor": false
      }
    ],
    "description": {
      "value": "*3283",
      "masked": true
    },
    "id": "2101",
    "name": "Tarjeta crédito",
    "number": "*3283",
    "primaryAmount": {
      "amount": 1800000,
      "currency": "CLP",
      "label": "cells-global-dashboard-cards-available"
    },
    "secondaryAmount": {
      "amount": 2000000,
      "currency": "CLP",
      "label": "cells-global-dashboard-cards-granted"
    },
    "imgSrc": "https://openapi.bbva.com/ccds/covers?pg=Z1&BIN=553656&default_image=true&v=…eight=226&country=es&app_id=com.bbva.wallet&issue_date=20141001&country=cl"
  },
  {
    "currencies": [
      {
        "currency": "CLP",
        "isMajor": true
      },
      {
        "currency": "USD",
        "isMajor": false
      }
    ],
    "description": {
      "value": "*5477",
      "masked": true
    },
    "id": "2104",
    "name": "Sticker",
    "number": "*5477",
    "primaryAmount": {
      "amount": 1800000,
      "currency": "CLP",
      "label": "cells-global-dashboard-cards-available"
    },
    "secondaryAmount": {
      "amount": 2000000,
      "currency": "CLP",
      "label": "cells-global-dashboard-cards-granted"
    },
    "isChildClass": "card-child",
    "isChild": true,
    "isFirstChildClass": "card-first-child",
    "isFirstChild": true,
    "imgSrc": "https://openapi.bbva.com/ccds/covers?pg=Y1&BIN=490225&default_image=true&v=…eight=226&country=es&app_id=com.bbva.wallet&issue_date=20141001&country=cl"
  },
  {
    "currencies": [
      {
        "currency": "CLP",
        "isMajor": true
      },
      {
        "currency": "USD",
        "isMajor": false
      }
    ],
    "description": {
      "value": "*2103",
      "masked": true
    },
    "id": "2103",
    "name": "Crédito multidivisa",
    "number": "*2103",
    "primaryAmount": {
      "amount": 1800000,
      "currency": "CLP",
      "label": "cells-global-dashboard-cards-available"
    },
    "secondaryAmount": {
      "amount": 2000000,
      "currency": "CLP",
      "label": "cells-global-dashboard-cards-granted"
    },
    "isChildClass": "card-child",
    "isChild": true,
    "imgSrc": "https://openapi.bbva.com/ccds/covers?pg=W1&BIN=545646&default_image=true&v=…eight=226&country=es&app_id=com.bbva.wallet&issue_date=20141001&country=cl"
  },
  {
    "currencies": [
      {
        "currency": "CLP",
        "isMajor": true
      },
      {
        "currency": "USD",
        "isMajor": false
      }
    ],
    "description": {
      "value": "*8259",
      "masked": true
    },
    "id": "2107",
    "name": "Tarjeta crédito",
    "number": "*8259",
    "primaryAmount": {
      "amount": 2000000,
      "currency": "CLP",
      "label": "cells-global-dashboard-cards-available"
    },
    "secondaryAmount": {
      "amount": 3000000,
      "currency": "CLP",
      "label": "cells-global-dashboard-cards-granted"
    },
    "imgSrc": "https://openapi.bbva.com/ccds/covers?pg=Z1&BIN=553656&default_image=true&v=…eight=226&country=es&app_id=com.bbva.wallet&issue_date=20141001&country=cl"
  },
  {
    "currencies": [
      {
        "currency": "CLP",
        "isMajor": true
      },
      {
        "currency": "USD",
        "isMajor": false
      }
    ],
    "description": {
      "value": "*6501",
      "masked": true
    },
    "id": "2108",
    "name": "Tarjeta crédito",
    "number": "*6501",
    "primaryAmount": {
      "amount": 3000000,
      "currency": "CLP",
      "label": "cells-global-dashboard-cards-available"
    },
    "secondaryAmount": {
      "amount": 4000000,
      "currency": "CLP",
      "label": "cells-global-dashboard-cards-granted"
    },
    "imgSrc": "https://openapi.bbva.com/ccds/covers?pg=Z1&BIN=553656&default_image=true&v=…eight=226&country=es&app_id=com.bbva.wallet&issue_date=20141001&country=cl"
  },
  {
    "currencies": [
      {
        "currency": "CLP",
        "isMajor": true
      },
      {
        "currency": "USD",
        "isMajor": false
      }
    ],
    "description": {
      "value": "*5477",
      "masked": true
    },
    "id": "2104",
    "name": "Sticker",
    "number": "*5477",
    "primaryAmount": {
      "amount": 1800000,
      "currency": "CLP",
      "label": "cells-global-dashboard-cards-available"
    },
    "secondaryAmount": {
      "amount": 2000000,
      "currency": "CLP",
      "label": "cells-global-dashboard-cards-granted"
    },
    "isChildClass": "card-child",
    "isChild": true,
    "isFirstChildClass": "card-first-child",
    "isFirstChild": true,
    "imgSrc": "https://openapi.bbva.com/ccds/covers?pg=Y1&BIN=490225&default_image=true&v=…eight=226&country=es&app_id=com.bbva.wallet&issue_date=20141001&country=cl"
  },
  {
    "currencies": [
      {
        "currency": "CLP",
        "isMajor": true
      },
      {
        "currency": "USD",
        "isMajor": false
      }
    ],
    "description": {
      "value": "*2103",
      "masked": true
    },
    "id": "2103",
    "name": "Crédito multidivisa",
    "number": "*2103",
    "primaryAmount": {
      "amount": 1800000,
      "currency": "CLP",
      "label": "cells-global-dashboard-cards-available"
    },
    "secondaryAmount": {
      "amount": 2000000,
      "currency": "CLP",
      "label": "cells-global-dashboard-cards-granted"
    },
    "isChildClass": "card-child",
    "isChild": true,
    "imgSrc": "https://openapi.bbva.com/ccds/covers?pg=W1&BIN=545646&default_image=true&v=…eight=226&country=es&app_id=com.bbva.wallet&issue_date=20141001&country=cl"
  }
];