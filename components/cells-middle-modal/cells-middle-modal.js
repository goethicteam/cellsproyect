Polymer({

  is: 'cells-middle-modal',

  behaviors: [
    CellsBehaviors.i18nBehavior
  ],

  properties: {
    /**
     * Allows the modal to hide itself when the overlay is clicked
     */
    closeOnClick: {
      type: Boolean,
      value: false,
      reflectToAttribute: true
    },
    /**
     * Allows adding an icon to close the modal. If no focusTarget is defined, this icon will receive the focus each time the modal is opened.
     */
    closeIcon: {
      type: String
    },
    /**
     * Allows adding a header with an icon to close the modal. If no focusTarget is defined, this icon will receive the focus each time the modal is opened.
     */
    mainHeaderText: {
      type: String
    },
    /**
     * Allows to define an aria-level for the main header text. If 0 is provided, main header text won't be treated as a heading.
     */
    headingLevel: {
      type: Number,
      value: 1
    },
    /**
     * If true, makes modal visible
     */
    open: {
      type: Boolean,
      observer: '_open',
      reflectToAttribute: true,
      notify: true
    },
    /**
     * Set the Id of one of the modal contents here to automatically move the focus to that element (if it's focusable) each time the modal is opened.
     */
    focusTarget: {
      type: String
    }
  },
  hostAttributes: {
    'aria-hidden': 'true',
    'role': 'dialog'
  },
  listeners: {
    click: '_clickHandler',
    keydown: '_manageTabFocusout'
  },

  attached: function() {
    var _this = this;
    document.addEventListener('focus', function(event) {
      _this._focusTrap(event);
    }, true);

    Polymer.RenderStatus.afterNextRender(this, function() {
      // if the content is taller than the modal, reset scrollTop to 0
      if (this.$$('.md-content').offsetHeight > this.offsetHeight) {
        this.scrollTop = 0;
      }
    });
  },

  /**
   * Returns a specific class name for the modal content div if a main header text exists
   * @private
   */
  _mainHeaderClass: function(mainHeaderText) {
    return this.closeIcon ? 'main-header-text with-close-icon' : 'main-header-text';
  },

  /**
   * Manage focus trap
   * @private
   */
  _focusTrap: function(event) {
    if (this.open && !this.contains(event.target)) {
      event.stopPropagation();
      this.$.dialog.focus();
    }
  },

  /**
  * Manages opening and closing the modal
  */
  _open: function(newValue) {
    if (newValue) {
      this._show();
    } else {
      this._hide();
    }
  },

  /**
   * Returns current active element taking Shadow DOM in account
   */
  _deepActiveElement: function() {
    var active = document.activeElement || document.body;
    while (active.root && Polymer.dom(active.root).activeElement) {
      active = Polymer.dom(active.root).activeElement;
    }
    return active;
  },

  /**
   * Show `cells-middle-modal`
   * @public
   */
  _show: function() {
    var dialog = this.$.dialog;
    this.listen(document, 'keydown', '_onKeyDown');
    this.set('_focusOrigin', this._deepActiveElement());
    this.setAttribute('aria-hidden', 'false');
    this.async(function() {
      var target = Polymer.dom(this).querySelector('#' + this.focusTarget) || Polymer.dom(this.root).querySelector('#' + this.focusTarget);
      if (this.focusTarget && !!target) {
        target.focus();
      } else if (this.closeIcon) {
        this.$$('#btn-close').focus();
      } else {
        dialog.focus();
      }
      /**
       * Fired when `cells-middle-modal` is opened.
       * @event cells-middle-modal-opened
       */
      this.fire('cells-middle-modal-opened');
    }, 20);

  },

  /**
   * Close modal and unlisten keyDown event when Esc key is pressed and dialog is open
   */
  _onKeyDown: function(event) {
    if (this.open && event.keyCode === 27) {
      this.open = false;
      this.unlisten(document, 'keydown', '_onKeyDown');
    }
  },

  /**
   * Hide `cells-middle-modal`
   */
  _hide: function() {
    this.setAttribute('aria-hidden', 'true');
    if (this._focusOrigin) {
      this._focusOrigin.focus();
    }
    /**
     * Fired when `cells-middle-modal` is hidden.
     * @event cells-middle-modal-hidden
     */
    this.fire('cells-middle-modal-hidden');
  },

  /**
   * Hide `cells-middle-modal` if attribute `closeOnClick` is set to true
   */
  _onClick: function() {
    if (this.closeOnClick) {
      this.open = false;
    }
  },
  /**
   * Fires 'cells-middle-modal-click-close' event before hiding the modal.
   */
  _onClickClose: function() {
    /**
     * Fired when `cells-middle-modal` is closed using the close button.
     * @event cells-middle-modal-close
     */
    this.fire('cells-middle-modal-click-close');
    this.open = false;
  },

  _clickHandler: function(e) {
    var target = Polymer.dom(e).localTarget;
    if (target && target.dataset.action === 'hide') {
      this.open = false;
    }
  },

  _manageTabFocusout: function(event) {
    if (this.open && event.keyCode === 9) {
      var target = Polymer.dom(event).rootTarget;
      var focusElements = Polymer.dom(this).querySelectorAll('a[href],area[href],input:not([disabled]),select:not([disabled]),textarea:not([disabled]),button:not([disabled]),[tabindex]:not([tabindex="-1"])');
      if (target === focusElements[focusElements.length - 1] && !event.shiftKey) {
        event.preventDefault();
        this.$.dialog.focus();
      } else if (target === this.$.dialog && event.shiftKey) {
        event.preventDefault();
        focusElements[focusElements.length - 1].focus();
      }

    }
  }


});
