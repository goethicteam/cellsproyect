(function() {

  'use strict';

  Polymer({
    is: 'cells-skeleton-cards-page',

    properties: {

      /*
      * Hide skeleton header
      */
      hideHeader: {
        type: Boolean,
        value: false,
        reflectToAttribute: true
      },

      /*
      * Hide skeleton card title
      */
      hideTitle: {
        type: Boolean,
        value: false,
        reflectToAttribute: true
      },

      /*
      * Hide skeleton card item content
      */
      hideItem: {
        type: Boolean,
        value: false,
        reflectToAttribute: true
      },

      /**
       * Number of cards to show
       */
      repeat: {
        type: Number,
        value: 4
      },

      /**
       * Displays to full screen
       */
      visible: {
        type: Boolean,
        value: false,
        reflectToAttribute: true
      }
    },

    _count: function(repeat) {
      return Array.apply(undefined, { length: repeat });
    }

  });

}());
