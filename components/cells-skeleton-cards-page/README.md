![Component Screenshot](cells-skeleton-cards-page.png)

# cells-skeleton-cards-page

[![Certificated](https://img.shields.io/badge/certificated-yes-brightgreen.svg)](http://bbva-files.s3.amazonaws.com/cells/bbva-catalog/index.html)
[Demo of component in Cells Catalog](http://bbva-files.s3.amazonaws.com/cells/bbva-catalog/index.html#/elements/cells-skeleton-cards-page)

`<cells-skeleton-cards-page>` is a loading status layout consisting of multiple cards and optional header.

Examples:

```html
<cells-skeleton-cards-page></cells-skeleton-cards-page>
```

Without header:
```html
<cells-skeleton-cards-page hide-header></cells-skeleton-cards-page>
```

## Styling

The following custom properties and mixins are available for styling:

| Custom property                                    | Description                                    | Default |
|:---------------------------------------------------|:-----------------------------------------------|:-------:|
| --cells-skeleton-cards-page                        | Empty mixin for component                      |   {}    |
| --cells-skeleton-cards-page-visible                | Empty mixin for component when visible         |   {}    |
| --cells-skeleton-cards-page-wrapper-anim           | Empty mixin for wrapper animation layer        |   {}    |
| --cells-skeleton-cards-page-wrapper                | Empty mixin for wrapper                        |   {}    |
| --cells-skeleton-cards-page-list                   | Empty mixin for list                           |   {}    |
| --cells-skeleton-cards-page-list-item              | Empty mixin for list item                      |   {}    |
| --cells-skeleton-cards-page-list-item-delay        | List item standard showing delay               |  70ms   |
| --cells-skeleton-cards-page-visible-anim-list-item | Empty mixin for visible and animated list item |   {}    |
| --cells-skeleton-cards-page-medium-list-item       | Empty mixin for list item on medium screen     |   {}    |
| --cells-skeleton-cards-page-large-list-item        | Empty mixin for list item on large screen      |   {}    |
| --cells-skeleton-cards-page-visible-list-item      | Empty mixin for visible list item              |   {}    |
| --cells-skeleton-cards-page-card-bg-color          | Card background color                          | #004481 |
| --cells-skeleton-cards-page-card-anim              | Empty mixin for card animation layer           |   {}    |
| --cells-skeleton-cards-page-card                   | Empty mixin for card                           |   {}    |
| --cells-skeleton-cards-page-box-bg-color           | Box background color                           | #1464A5 |
| --cells-skeleton-cards-page-box                    | Empty mixin for box                            |   {}    |
| --cells-skeleton-cards-page-blocks                 | Empty mixin for blocks                         |   {}    |
| --cells-skeleton-cards-page-header-box-bg-color    | Header box background color                    | #E9E9E9 |
| --cells-skeleton-cards-page-header-box             | Empty mixin for header box                     |   {}    |
| --cells-skeleton-cards-page-header                 | Empty mixin for header                         |   {}    |
| --cells-skeleton-cards-page-title-box              | Empty mixin for title box                      |   {}    |
| --cells-skeleton-cards-page-title                  | Empty mixin for title                          |   {}    |
| --cells-skeleton-cards-page-content-top-box        | Empty mixin for content top box                |   {}    |
| --cells-skeleton-cards-page-content-top            | Empty mixin for content top                    |   {}    |
| --cells-skeleton-cards-page-content-bottom-box     | Empty mixin for content bottom box             |   {}    |
| --cells-skeleton-cards-page-content-bottom         | Empty mixin for content bottom                 |   {}    |
| --cells-skeleton-cards-page-content                | Empty mixin for content                        |   {}    |
