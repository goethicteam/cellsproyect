(function() {

  'use strict';

  Polymer({

    is: 'cells-date-range-picker',

    behaviors: [
      window.CellsBehaviors.i18nBehavior
    ],
    properties: {
      /*
       * Has "from date" value
       */
      from: {
        value: '',
        type: String,
        observer: '_fromObserver',
        notify: true
      },
      /*
       * Has "to date" value
       */
      to: {
        value: '',
        type: String,
        observer: '_toObserver',
        notify: true
      },
      /*
       * If is true allows select past date
       */
      allowPastDate: {
        type: Boolean
      },
      /*
       * If is true allows select weekends
       */
      availableWeekends: {
        type: Boolean
      },
      /*
       * Set minDate for "from date"
       */
      minDate: {
        type: String
      },
      /*
       * Set maxDate for "to date"
       */
      maxDate: {
        type: String
      },
      /*
       * Icon code
       */
      icon: {
        type: String
      },
      /*
       * Icon size
       */
      iconSize: {
        type: String
      },
      /*
       * Date format for placeholder
       */
      format: {
        type: String
      },
      /*
       * Placeholder if date is not selected
       */
      placeholder: {
        type: String
      },
      /*
       * locale for molecule date input
       */
      _lang: {
        type: String,
        value: function() {
          return window.I18nMsg.lang;
        }
      }
    },

    attached: function() {
      this.listen(this.$.fromCalendar, 'input-date-changed', '_dateFromChanged');
      this.listen(this.$.toCalendar, 'input-date-changed', '_dateToChanged');
    },

    /**
     * Sets from property
     * @param {date} date info received
     */
    _dateFromChanged: function(dateInfo) {
      this.from = new Date(dateInfo.detail.date);
    },

    /**
     * Sets to property
     * @param {date} date info received
     */
    _dateToChanged: function(dateInfo) {
      this.to = new Date(dateInfo.detail.date);
    },

    /**
     * resets invalid date
     * @param {Object} new date received
     */
    _fromObserver: function(newValue) {
      if (newValue == 'Invalid Date') {
        this.from = '';
        this.$.fromCalendar.reset();
      }
    },

    /**
     * resets invalid date
     * @param {Object} new date received
     */
    _toObserver: function(newValue) {
      if (newValue == 'Invalid Date') {
        this.to = '';
        this.$.toCalendar.reset();
      }
    },

    /**
     * formats date with utc 
     * @param {date} new date received
     */
    _changeToDateFormat: function(date) {
      return (date !== '' ? moment(date, 'YYYY-MM-DDTHH:mm:ssZ').utc() : '');
    },

    /*
     * Clean component
     */
    reset: function() {
      this.from = '';
      this.to = '';
      this.$.fromCalendar.reset();
      this.$.toCalendar.reset();
    },

    /**
     * set maxDate
     * @param {date} maxDate received
     * @param {date} to date received
     */
    _computeMaxDate: function(maxDate, to) {
      var maxDateComputed;
      if (!to) {
        maxDateComputed = maxDate;
      } else if (to && maxDate) {
        if (new Date(to) < new Date(maxDate)) {
          maxDateComputed = to;
        } else {
          maxDateComputed = maxDate;
        }
      }

      return maxDateComputed;
    },

    /**
     * set minDate
     * @param {date} minDate received
     * @param {date} to date received
     */
    _computeMinDate: function(minDate, from) {
      var minDateComputed;
      if (!from) {
        minDateComputed = minDate;
      } else if (from && minDate) {
        if (new Date(from) > new Date(minDate)) {
          minDateComputed = from;
        } else {
          minDateComputed = minDate;
        }
      }

      return minDateComputed;
    },


    /**
     * removed hh:mm:ss from date
     * @param {String} date received
     * @return {String} Date String without hour:min:secs
     */
    _formatDateToDateInput: function(date) {
      var stringToReplace = date.slice(date.indexOf('T'));
      return date.replace(stringToReplace, '');
    },


    /**
     * set date in native datepicker
     * @param {String} date received
     * @return {String} Date String without hour:min:secs
     */
    setDate: function() {
      var from;
      var to;
      /* istanbul ignore else */
      if (this.from !== '' && typeof this.from === 'string') {
        from = this._formatDateToDateInput(this.from);
        this.$.fromCalendar.setDate(from);
      }

      /* istanbul ignore else */
      if (this.to !== '' && typeof this.to === 'string') {
        to = this._formatDateToDateInput(this.to);
        this.$.toCalendar.setDate(to);
      }
    }

  });

}());
