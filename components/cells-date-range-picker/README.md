# cells-date-range-picker

`<cells-date-range-picker>` shows two calendar to select range of dates.

Example:

```html
<cells-date-range-picker
  allow-past-date
  available-weekends
  icon="coronita:calendar"
  icon-size="18"
  format="DD MMMM YYYY"
></cells-date-range-picker>
```

## Styling

The following custom properties and mixins are available for styling:

| Custom property | Description     | Default        |
|:----------------|:----------------|:--------------:|
| --cells-date-range-picker-scope      | scope description | default value  |
| --cells-date-range-picker  | empty mixin     | {}             |           |
| --cells-date-range-picker-wrapper | empty mixin |             |           |
| --cells-date-range-picker-item | empty mixin |                |           |
| --cells-date-range-picker-description | empty mixin |         |           |
| --cells-date-range-picker-description-focus | empty mixin |   |           |
| --cells-date-range-picker-divider | empty mixin |             |           |
