# cells-menu-web
## Description

This component it's a example de menu of web. This component has styles of responsive web design, and you can disable the behavior by default, if you can used the 'disabledActionMobile' property.

Example:

```html
<cells-menu-web id="menu" items="[[menu]]"></cells-menu-web>
```

__Dependencies__:

1. __cells-menu-web-item__: 
  
  Description: This component it's the simple button of cells-menu-web. This component are included in this component.
  
  ```
  {
    "key": "awards",                      // Key to translate, ex. 'cells-menu-web-my-accounts'
    "link": true,                         // Boolean, it's a external link (Optional)
    "href": "https://www.bancomer.com",   // Url to redirect (Page or url)
    "target": "_blank",                   // Redirect options, ex. '_blank' (Optional)
    "icon": {                             // Icon to show in the link (Optional)
      "code": "icons:A05",                // Icon code of cells-icons component
      "size": 10                          // Icon size
    }
  }
  ```


## Data model

Component own DM example:

items property must be this structure:
```json
[
  {
    "key": "my-account",                // Key to translate, ex. 'cells-menu-web-my-accounts'
    "link": true,                       // Boolean, it's a external link (Optional) 
    "href": "https://www.bancomer.com", // Url to redirect (Page or url)
    "target": "_blank",                 // Redirect options, ex. '_blank' (Optional)
    "icon": {                           // Icon to show in the link (Optional)
      "code": "A01",                    // Icon code of cells-icons component
      "size": 24                        // Icon size
    },
    "subMenus": [                       // Array of items that show on form to submenu, used the same properties that item
      {
        "title": "This is a test",      // Title of submenu
        "items": [
          {
            "key": "my-account",
            "href": "urlToMovements",   // Page redirect
            "icon": {
              "code": "icons:B47",
              "size": 8
            }
          },
          {
            "key": "my-account",
            "link": true,
            "icon": {
              "code": "icons:B47",
              "size": 8
            },
            "href": "https://www.bancomer.com",
            "target": "_blank"
          }
        ]
      }
    ]
  }
]
```

## Styling

The following custom properties and mixins are available for styling:

| Custom property | Description     | Default        |
|:----------------|:----------------|:--------------:|
| --cells-menu-web-scope      | scope description | default value  |
| --cells-menu-web  | empty mixin  | {}             |
| --cells-menu-web__nav | empty mixin for .nav | {} |
| --cells-menu-web__nav--mobile | empty mixin for .nav--mobile | {} |
| --cells-menu-web__nav-bar--mobile | empty mixin for .nav-bar--mobile | {} |
| --cells-menu-web__li--mobile  | empty mixin for li in mobile | {} |
| --cells-menu-web__nav-bar | empty mixin for .nav-bar | {} |
| --cells-menu-web__nav-bar__sub  | empty mixin for .nav-bar__sub | {} |
| --cells-menu-web__nav-bar__sub--mobile  | empty mixin for .nav-bar__sub--mobile | {} |
| --cells-menu-web__nav-bar__item | empty mixin for .nav-bar__item | {} |
| --cells-menu-web__nav-bar__item--active | empty mixin for .nav-bar__item.active | {} |
| --cells-menu-web__nav-bar__item--opened | empty mixin for .nav-bar__item.opened | {} |
| --cells-menu-web__nav-bar__item--hover  | empty mixin for .nav-bar__item:hover | {} |
| --cells-menu-web__nav-bar-mobile__title  | empty mixin for .nav-bar-mobile__title | {} |
| --cells-menu-web__subTitle  | empty mixin for h4 | {} |
| --cells-menu-web__button  | empty mixin for button | {} |
| --cells-menu-web__item--view  | empty mixin for .item--view | {} |