var animMock = {
  addContext: function() {},
  play: function() {},
  rewind: function() {},
  removeContext: function() {},
  "width": 480,
  "height": 400,
  "numPlays": 0,
  "playTime": 1416.666666666667,
  "frames": [
    {
      "width": 480,
      "height": 400,
      "left": 0,
      "top": 0,
      "delay": 41.666666666666664,
      "disposeOp": 1,
      "blendOp": 0,
      "img": {},
      "iData": null
    },
    {
      "width": 319,
      "height": 295,
      "left": 72,
      "top": 54,
      "delay": 41.666666666666664,
      "disposeOp": 1,
      "blendOp": 0,
      "img": {},
      "iData": null
    }
  ]
};
