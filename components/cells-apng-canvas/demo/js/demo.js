var mocks = {
  default: {
    autoplay: true
  },
  noAutoplay: {
    autoplay: false
  }
};

function setMock(ui, mockKey) {
  var mockSelected = mocks[mockKey];
  Object.keys(mockSelected).forEach(function(prop) {
    ui.set(prop, mockSelected[prop]);
  });
}

document.addEventListener('WebComponentsReady', function() {
  var demo = document.getElementById('demo');
  var ui = demo.$.ui;

  demo.playing = true;
  demo.selected = 'default';

  demo.setMock = function(e) {
    setMock(ui, demo.selected);
    if (demo.selected === 'noAutoplay') {
      this.playing = false;
    }
  };

  demo.stop = function() {
    ui.stop();
    this.playing = false;
  };

  demo.play = function() {
    ui.play();
    this.playing = true;
  };

  setMock(ui, 'default');
});
