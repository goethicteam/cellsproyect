document.addEventListener('WebComponentsReady', function() {
  var component = document.querySelector('cells-myb-period-balances');
  var data = {
    'data': [
      {
        'beginDate': '2017-12-31T00:00:00Z',
        'endDate': '2017-12-31T00:00:00Z',
        'sales': [
          {
            'totalElements': 19,
            'amount': 4925.67,
            'currency': 'EUR'
          }
        ],
        'refunds': [
          {
            'totalElements': 6,
            'amount': 1696.2,
            'currency': 'EUR'
          }
        ],
        'fees': [
          {
            'amount': 35.11,
            'currency': 'EUR'
          }
        ]
    },
    {
      'beginDate': '2017-12-30T00:00:00Z',
      'endDate': '2017-12-30T00:00:00Z',
      'sales': [
        {
          'totalElements': 13,
          'amount': 4008.05,
          'currency': 'EUR'
        }
      ],
      'refunds': [
        {
          'totalElements': 2,
          'amount': 413.77,
          'currency': 'EUR'
        }
      ],
      'fees': [
        {
          'amount': 105.38,
          'currency': 'EUR'
        }
      ]
    }]
  };
  var subheaderData = {
    sectionBalance: {
      'date': {
        'start': '2016-01-01T00:00:00Z',
        'end': '2016-12-31T23:59:59Z'
      },
      'format': {
        'start': 'MMM',
        'end': 'MMM YYYY'
      },
      'separator': '-'
    },
    dateRange: {
      'totalElements': 3575,
      'amount': 14365.26,
      'currency': 'EUR'
    },
    sectionStats: {
      "statPercentValue": "+7.02",
      "fromDateText": "año pasado",
      "stats": [{
        "total": "3.815",
        "amount": 972759.82,
        "currency": "EUR",
        "literal": "cells-myb-subheader-statics-total-sales",
        "class": "subheader-sales"
      }, {
        "total": "1.219",
        "amount": -308655.06,
        "currency": "EUR",
        "literal": "cells-myb-subheader-statics-total-refunds",
        "class": "subheader-refund"
      }]
    }
  };
  component.set('listData', data);
  component.set('subheaderData', subheaderData);
})();