(function() {

  'use strict';
  /* global moment */

  Polymer({

    is: 'cells-myb-period-balances',
    behaviors: [
      CellsBehaviors.i18nBehavior
    ],

    properties: {
      /*
       * Data show in the transaction list
       * @type {Object}
       */
      listData: {
        type: Object,
        observer: '_newListDataReceived'
      },
      /*
       * Data to show in the subheader
       * @type {Object}
       */
      subheaderData: {
        type: Object,
        observer: '_newSubheaderDataReceived'
      },
      /*
       * Controls witch part of the component is visible, date salector or results
       * @type {Boolean}
       */
      showResults: {
        type: String,
        value: 'hidden'
      },
      /*
       * To control subheader open status
       * @type {Object}
       */
      resumeOpened: {
        type: Boolean,
        value: false
      },
      /*
       * Start date for request
       * @type {String}
       */
      fromDate: {
        type: String
      },
      /*
       * End date for request
       * @type {String}
       */
      toDate: {
        type: String
      },
      /*
       * Start date for selection and reset control
       * @type {String}
       */
      tempFromDate: {
        type: String,
        observer: '_rangeChanged'
      },
      /*
       * End date for selection and reset control
       * @type {String}
       */
      tempToDate: {
        type: String,
        observer: '_rangeChanged'
      },
      /*
       * Minimum date for range calendar
       */
      minDate: {
        type: String,
        value: function() {
          return moment().subtract(3, 'y').startOf('year');
        }
      },
      /*
       * Maximum date for range calendar
       */
      maxDate: {
        type: String,
        value: function() {
          return new Date(new Date().setHours('23', '59', '59'));
        }
      },
      /*
       * Reflects the visible state for bottom loading animation
       */
      bottomLoading: {
        type: Boolean,
        value: false
      },
      noDataImage: {
        type: String,
        value: ''
      },
      noDataHeaderText: {
        type: String
      },
      noDataMainText: {
        type: String
      },
      noDataPeriodLinkText: {
        type: String
      },
      hasDataRange: {
        type: Boolean,
        value: true
      }
    },

    // attached: function() {
    //   this.$.period_balances_buttons.set('buttons', [
    //     {
    //       class: 'btn-clear btn--full btn--primary',
    //       text: 'cells-myb-period-balances-change-range-button-restore',
    //       disabled: true
    //     }, {
    //       class: 'btn-apply btn--full btn--primary',
    //       text: 'cells-myb-period-balances-change-range-button-apply',
    //       disabled: true
    //     }]
    //   );
    //   this.listen(this.$.period_balances_buttons, 'button-0-pressed', 'resetRange');
    //   this.listen(this.$.period_balances_buttons, 'button-1-pressed', 'applyRange');
    // },
    ready: function(){
      if (sessionStorage.getItem("accounts")){
        var myaccounts = JSON.parse(sessionStorage.getItem('accounts'));
        var mycards = JSON.parse(sessionStorage.getItem('cards'));
          this.listData=myaccounts[0];
      };
    },

    load: function(data){
      var myaccounts = JSON.parse(sessionStorage.getItem('accounts'));
      var mycards = JSON.parse(sessionStorage.getItem('cards'));
      for(var i in myaccounts){
        mycards.push(myaccounts[i]);
      }
      var finded=false;
      var i=0;
      while (!finded){
        if (mycards[i].number==data.productId){
          this.listData=mycards[i];
          finded=true;
        }
        i++;
      }

    },

    reset: function() {
      this.data = undefined;
      this.showResults = false;
      this.fromDate = undefined;
      this.toDate = undefined;
      this.$.subheader.hideLoading();
      this.$.list.hideLoading();
    },

    _rangeChanged: function() {
      this.$.dateRangePicker.setDate();
      if (this.tempToDate || this.tempFromDate) {
        this.$.period_balances_buttons.changeStatus({buttonIndex: 0, buttonActive: true});
        if (this.tempToDate && this.tempFromDate) {
          this.$.period_balances_buttons.changeStatus({buttonIndex: 1, buttonActive: true});
        }
      }
    },

    setCustomer: function(datos){
      console.log('setCustomer');
      var mockLinks = [{
        label: 'Notificaciones',
        icon: 'coronita:alarm',
        link: '#notifications',
        action: 'notifications-event',
        count: 10,
        status: {
          showNoLogged: false,
          showLogged: true
        }
      }, {
        label: 'Posicion global',
        icon: 'coronita:titleview',
        link: '#routerExample',
        status: {
          showNoLogged: false,
          showLogged: true
        },
      }, {
        label: 'Mis productos',
        icon: 'coronita:supermarket',
        link: '#/products',
        status: {
          showNoLogged: false,
          showLogged: true
        }
      }, {
        label: 'Contratar producto',
        icon: 'coronita:settings',
        action: 'settings-event',
        status: {
          showNoLogged: true,
          showLogged: true
        }
      }, {
        label: 'Cajeros y oficinas',
        icon: 'coronita:mapa',
        link: '#atms',
        status: {
          showNoLogged: true,
          showLogged: true
        }
      }, {
        label: 'Videos',
        icon: 'coronita:titleview',
        link: '#apps',
        action: 'apps-event',
        status: {
          showNoLogged: false,
          showLogged: true
        }
      }, {
        label: 'Acerca de',
        icon: 'coronita:info',
        link: '#about',
        action: 'about-event',
        status: {
          showNoLogged: true,
          showLogged: true
        }
      },
      {
        label: 'login',
        icon: 'coronita:info',
        action: 'login-event',
        status: {
          showNoLogged: true,
          showLogged: false
        }
      }];
      //this.$.menu.availableSections = mockLinks;
      if (sessionStorage.getItem('customer')){
        var customer = JSON.parse(sessionStorage.getItem('customer'));
        var mockUser = {
        firstName: customer.firstName+' '+customer.surname,
        avatar: {
          url: customer.avatar
        }
      };
      // this.$.menu.userLogged = true;
      // this.$.menu.user = mockUser;
      // this.fire("customer-finish");
    }
    this.fire("customer-finish");
    },
    /**
    *
    */

    setCards: function() {
      console.log('setCards');
      this.fire("cards-finish");
    },

    setAccounts: function() {
      console.log("setAccounts");
      var myaccounts = JSON.parse(sessionStorage.getItem('accounts'));
      var mycards = JSON.parse(sessionStorage.getItem('cards'));

        this.listData=myaccounts[0];
        this.fire("account-finish");
    },

    _setAccounts: function() {
      var myaccounts = JSON.parse(sessionStorage.getItem('accounts'));
      var mycards = JSON.parse(sessionStorage.getItem('cards'));


            var products = [];
            console.log(myaccounts);

            for (var i in myaccounts){
              console.log(myaccounts[i]);
             var product =
                {
                  "id": myaccounts[i].number,
                  "name": myaccounts[i].alias,
                  "description": {
                    "value": myaccounts[i].id,
                    "masked": true
                  },
                  "primaryAmount": {
                    "amount": myaccounts[i].balance,
                    "currency": myaccounts[i].currency,
                    "label": "Saldo"
                  },
                  "scale": 0,
                  "status": {
                    "id": "ACTIVATED",
                    "description": "ACTIVATED"
                  }
                }
                products.push(product);
            };

            for (var i in mycards){
              console.log(mycards[i]);
             var product =
                {
                  "id": mycards[i].number,
                  "name": mycards[i].description,
                  "description": {
                    "value": mycards[i].id,
                    "masked": true
                  },
                  "primaryAmount": {
                    "amount": mycards[i].availableBalance,
                    "currency": mycards[i].currency,
                    "label": "Saldo"
                  },
                  "scale": 0,
                  "status": {
                    "id": "ACTIVATED",
                    "description": "ACTIVATED"
                  }
                }
                products.push(product);
            };
              this.$.cards.items=products;

            },

    loadMoreData: function() {
      this.$.list.loadMoreData();
    },

    resetRange: function() {
      this.fromDate = undefined;
      this.toDate = undefined;
      this.$.dateRangePicker.reset();
      this.sendRangePicked();
      this.$.period_balances_buttons.changeStatus({buttonIndex: 0, buttonActive: false});
      this.$.period_balances_buttons.changeStatus({buttonIndex: 1, buttonActive: false});
      this.fire('reset-range-picked');
    },

    applyRange: function() {
      var isValidateRange = this._validateRange();
      var toDate = (this.toDate ? this.toDate.toString() : '');
      var fromDate = (this.fromDate ? this.fromDate.toString() : '');

      if (this.tempToDate && this.tempFromDate && isValidateRange) {
        this.viewResults();

        if (moment(this.tempToDate.toString()).utc().format('DD-MM-YYYY') !== moment(toDate).utc().format('DD-MM-YYYY') ||
            moment(this.tempFromDate.toString()).utc().format('DD-MM-YYYY') !== moment(fromDate).utc().format('DD-MM-YYYY')) {
          this.hasDataRange = true;
          this.saveDates();
          this.sendRangePicked();
          this.$.subheader.showLoading();
          this.$.list.showLoading();
          this.fire('apply-range-picked');
        }
      }
    },

    saveDates: function() {
      this.set('fromDate', this.tempFromDate);//'2016-11-27T00:00:00Z');//
      this.set('toDate', this.tempToDate);//'2016-12-31T23:59:59Z');//
    },

    sendRangePicked: function() {
      this.fire('new-range-picked', {
        period: 'range',
        from: this.fromDate,
        to: this.toDate
      });
    },

    _newListDataReceived: function(newVal) {
      this._setAccounts();
      var transaccions =[];
      if(newVal.type=="ACCOUNT"){
        var trans = newVal.accountTransactions;
        for (var i in trans){
          var transaction = {
         'name': trans[i].description,
         'description': {
           'content': trans[i].category.name,
           'date': trans[i].operationDate
         },
         'primaryAmount': {
           'amount': trans[i].amount,
           'currency': trans[i].currency
          }
        };
        transaccions.push(transaction);
      };

      }
      else{
        var trans = newVal.cardTransactions;
        for (var i in trans){
          var transaction = {
         'name': trans[i].shop.name,
         'description': {
           'content': trans[i].subCategory.name,
           'date': trans[i].date
         },
         'primaryAmount': {
           'amount': trans[i].amount,
           'currency': trans[i].currency
          }
        };
        transaccions.push(transaction);
      };

      };

      this.$.transactionsList0.transactions=transaccions;
      this.$.transactionsList0.onData();
      this._gastos(newVal);
      this._transactionCalendar(transaccions);
      // this.$.transactionsList1.groupByMonth=true;
    },
    /**
    **/
    _gastos: function(newVal){
      var compras=0;
    	var ocio=0;
    	var efectivo=0;
    	var transporte=0;
    	var moda=0;
      var otros=0;
      var educacion=0;
      if(newVal.type!="ACCOUNT"){
        var trans = newVal.cardTransactions;
        for (var i in trans){
          switch(trans[i].category.name) {
            case "Compras":
              compras=compras +trans[i].amount;
              break;
            case "Ocio":
              ocio=ocio +trans[i].amount;
              break;
            case "Efectivo":
              efectivo=efectivo +trans[i].amount;
              break;
            case "Transporte y viajes":
              transporte=transporte +trans[i].amount;
              break;
            case "Moda y belleza":
              moda=moda +trans[i].amount;
              break;
            default:
              otros=otros +trans[i].amount;
            }
        };
      }
      else{
        var trans = newVal.accountTransactions;
        for (var i in trans){
          switch(trans[i].category.name) {
            case "PURCHASES":
              compras=compras +trans[i].amount;
              break;
            case "SHOPPING":
              ocio=ocio +trans[i].amount;
              break;
            case "EDUCATION":
              educacion=educacion +trans[i].amount;
              break;
            case "TRANSPORT AND TRAVEL":
              transporte=transporte +trans[i].amount;
              break;
            case "FASHION AND BEAUTY":
              moda=moda +trans[i].amount;
              break;
            default:
              otros=otros +trans[i].amount;
          }
        };
      };
      // Compras=
    	// Ocio=
    	// Efectivo=
    	// Transporte y viajes=
    	// Moda y belleza=
      var gastos= [
    {
      "label": "Compras",
      "value": compras
    },
    {
      "label": "Ocio",
      "value": ocio
    },
    {
      "label": "Efectivo",
      "value": efectivo
    },
    {
      "label": "transporte",
      "value": transporte
    },
    {
      "label": "moda",
      "value": moda
    },
    {
      "label": "educacion",
      "value": educacion
    },
    {
      "label": "otros",
      "value": otros
    }
  ];
  this.$.donut.data=gastos;
},

/**
*
*
**/

  _transactionCalendar: function(newVal) {
    var data=[];
    for (var i in newVal){
      var dat = { "eventName": newVal[i].name.toLowerCase()+"  "+newVal[i].primaryAmount.amount+"€",
      "category": newVal[i].description.content,
      "color": "red",
      "date": Date.parse(newVal[i].description.date) };
      data.push(dat);
    };
    this.$.calevents.data=data;
  },

    toggle: function() {
      console.log('toggle');
    this.$.men.togglePanel();
  },


    addTransactions: function(data) {
      var actualTransactions = this.$.list.transactions;
      var newTransactions = actualTransactions.concat(data);
      this.$.list.set('transactions', newTransactions);
    },

    _newSubheaderDataReceived: function(newVal) {
      if (newVal && newVal.operationStats) {
        this.async(function() {
          this.hasDataRange = !!newVal.operationStats.totalOperations;
          this.$.list.set('totalElements', newVal.operationStats.totalOperations);
          this.$.subheader.set('mainAmount', newVal.mainAmount);
          this.$.subheader.set('dateRange', newVal.dateRange);
          this.$.subheader.set('operationStats', newVal.operationStats);
          this.$.subheader.hideLoading();
          this.subheaderData = undefined;
        }.bind(this), 1000); // CSS animation + some delay
      }
    },

    showLoadings: function() {
      this.$.subheader.showLoading();
      this.$.list.showLoading();
      this.hasDataRange = true;
    },

    hideLoadings: function() {
      this.$.subheader.hideLoading();
      this.$.list.hideLoading();
    },

    hideListLoading: function() {
      this.$.list.hideLoading();
    },

    hideSubheaderLoading: function() {
      this.$.subheader.hideLoading();
    },

    viewResults: function() {
      this.set('showResults', 'visible');
      this.fire('showing-results');
    },

    viewPicker: function() {
      this.$.subheader.closeSeeMore();
      this.set('showResults', 'hidden');
      this.fire('choose-new-range-clicked');
    },

    showNoData: function() {
      this.set('noData', true);
    },

    _showErrorMessage: function(message) {
      this.fire('wrong-date-range', {
        code: '422',
        icon: 'coronita:error',
        noBackdrop: false,
        content: message,
        type: 'error',
        firstButtonLabel: this.getTranslation('cells-myb-period-balances-wrong-period-button-retry')
      });
    },

    _validateRange: function() {
      var message;
      var format = 'YYYY-MM-DD';
      var tempFromDateFormatted = moment(this.tempFromDate).utc().format(format);
      var tempToDateFormatted = moment(this.tempToDate).utc().format(format);
      var minDateFormatted = moment(this.minDate).utc().format(format);
      var maxDateFormatted = moment(this.maxDate).utc().add(2, 'h').format(format);
      var MAX_MONTHS_RANGE = 6;
      if (moment(tempToDateFormatted).isAfter(maxDateFormatted)) {
        message = this.getTranslation('cells-myb-period-balances-error-to-greater-than-maximum') + moment(this.maxDate).format('DD MMMM YYYY') + '.';
        this._showErrorMessage(message);
        return false;
      } else if (moment(tempFromDateFormatted).isAfter(tempToDateFormatted)) {
        message = this.getTranslation('cells-myb-period-balances-error-to-minor-than-from');
        this._showErrorMessage(message);
        return false;
      } else if (moment(tempFromDateFormatted).isBefore(minDateFormatted)) {
        message = this.getTranslation('cells-myb-period-balances-error-from-smaller-than-minimum') +  moment(this.minDate).format('DD MMMM YYYY') + '.';
        this._showErrorMessage(message);
        return false;
      } else if (moment(this.tempToDate).diff(moment(this.tempFromDate), 'months') >= MAX_MONTHS_RANGE) {
       message = this.getTranslation('cells-myb-period-balances-max-range-part-1') + ' ' + MAX_MONTHS_RANGE + ' ' + this.getTranslation('cells-myb-period-balances-max-range-part-2');
       this._showErrorMessage(message);
       return false;
      } else {
        return true;
      }
    },

    closeSubheader: function() {
      var _operationSubheaderDom =
      (this.shadowRoot)
      ? this.shadowRoot.querySelector('cells-myb-subheader')
      : this.querySelector('cells-myb-subheader');

      if (_operationSubheaderDom && _operationSubheaderDom.attributes['is-opened-see-more']) {
        _operationSubheaderDom.removeAttribute('is-opened-see-more');
      }
    }

  });

}());
