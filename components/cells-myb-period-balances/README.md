# cells-myb-period-balances

Your component description.

Example:
```html
<cells-myb-period-balances></cells-myb-period-balances>
```

```

## Styling

The following custom properties and mixins are available for styling:

| Custom property | Description     | Default        |
|:----------------|:----------------|:--------------:|
| --cells-myb-period-balances-scope      | scope description | default value  |
| --cells-myb-period-balances  | empty mixin     | {}             |
