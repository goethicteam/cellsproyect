var transactions = [
  {
    'transactionId': '500A7778',
    'localAmount': {
      'amount': 200,
      'currency': 'EUR'
    },
    'availableDate': '2015-12-08',
    'name': 'Vips'
  },
  {
    'transactionId': '999A1328',
    'localAmount': {
      'amount': 90.546,
      'currency': 'EUR'
    },
    'availableDate': '2015-12-07',
    'name': 'Cash withdrawals at ATM servired'
  },
  {
    'transactionId': '301C9073',
    'localAmount': {
      'amount': 80,
      'currency': 'EUR'
    },
    'availableDate': '2015-12-07',
    'name': 'Bill Aviatur'
  },
  {
    'transactionId': '500A7778',
    'localAmount': {
      'amount': 500,
      'currency': 'EUR'
    },
    'availableDate': '2016-11-08',
    'name': 'income by check'
  },
  {
    'transactionId': '500A7778',
    'localAmount': {
      'amount': 500,
      'currency': 'EUR'
    },
    'availableDate': '2016-12-08',
    'name': 'income by check'
  }
];

document.addEventListener('WebComponentsReady', function() {
  var demo = document.getElementById('demo');
  var el = demo.$.el;
  el.set('groupByMonth', true);
  el.set('transactions', transactions);

  demo.addTransactions = function(e) {
    e.preventDefault();
    el.push('transactions', {
      'transactionId': '500A7779',
      'localAmount': {
        'amount': 200,
        'currency': 'EUR'
      },
      'availableDate': '2015-12-05',
      'name': 'income by check'
    });
  };
});
