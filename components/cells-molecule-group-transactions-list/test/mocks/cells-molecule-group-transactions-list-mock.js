var transactionsMock = [
  {
    'transactionId': '500A7778',
    'localAmount': {
      'amount': 200,
      'currency': 'EUR'
    },
    'availableDate': '2015-12-08',
    'name': 'Vips'
  },
  {
    'transactionId': '999A1328',
    'localAmount': {
      'amount': 90.546,
      'currency': 'EUR'
    },
    'availableDate': '2015-12-07',
    'name': 'Cash withdrawals at ATM servired'
  },
  {
    'transactionId': '301C9073',
    'localAmount': {
      'amount': 80,
      'currency': 'EUR'
    },
    'availableDate': '2015-12-07',
    'name': 'Bill Aviatur'
  },
  {
    'transactionId': '500A7778',
    'localAmount': {
      'amount': 500,
      'currency': 'EUR'
    },
    'availableDate': '2015-12-08',
    'name': 'income by check'
  },
  {
    'transactionId': '500FRGE8',
    'localAmount': {
      'amount': 1000,
      'currency': 'EUR'
    },
    'name': 'no date transaction'
  }
];

var transactionMock = {
  'transactionId': '500A7779',
  'localAmount': {
    'amount': 200,
    'currency': 'EUR'
  },
  'availableDate': '2015-12-05',
  'name': 'income by check'
}
