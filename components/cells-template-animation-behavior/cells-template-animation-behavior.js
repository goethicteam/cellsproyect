'use strict';

/*global WeakMap*/

/**
 * `Polymer.CellsTemplateAnimationBehavior` is a behavior to manage template animations.
 *
 * 1) Import the behavior in your component.
 *
 * ```html
 * <link rel="import" href="../cells-template-animation-behavior/cells-template-animation-behavior.html">
 * ```
 *
 * 2) Add `Polymer.CellsTemplateAnimationBehavior` to the behaviors list in the JS file or script of your component:
 *
 * ```js
 * behaviors: [
 *   Polymer.CellsTemplateAnimationBehavior
 * ]
 * ```
 * 3) Add `cells-template-animation-behavior-styles` in tag style.
 *
 *```html
 * <style include="name-your-component-styles cells-template-animation-behavior-styles"></style>
 * ```
 * @polymerBehavior Polymer.CellsTemplateAnimationBehavior
 *
 * @demo demo/index.html
 * @hero cells-template-animation-behavior.jpg
 */

Polymer.CellsTemplateAnimationBehavior = {

  /**
   * Fired when template is cached.
   * @event 'template-cached'
   */

  /**
   * Fired when template animation ends.
   * @event this.animationCompleteEvent ('default value: template-active')
   */

  /**
   * Fired from outgoing template when forward animation.
   * @event 'animation-forward'
   */

  /**
   * Fired from outgoing template when backward animation.
   * @event 'animation-backward'
   */
  properties: {

    /**
     * Template name.
     */
    name: {
      type: String
    },

    /**
     * Can be 'cached', 'inactive', 'active'.
     */
    state: {
      type: String,
      reflectToAttribute: true,
      observer: '_stateChanged'
    },

    /**
     * Name of the event that will be fired when the animation ends.
     */
    animationCompleteEvent: {
      type: String,
      value: 'template-active'
    },

    /**
     * Can be 'horizontal' (default), 'horizontalEver', 'staticEver', 'static', 'verticalDownForwards', 'verticalDownBackwards' or 'verticalUp'.
     */
    animationType: {
      type: String,
      value: 'horizontal',
      notify: true,
      observer: '_animationTypeChanged'
    },

    /**
     * An object that configurates the class name used for each direction for the 'horizontal' animationType.
     */
    horizontal: {
      type: Object,
      value: {
        forwardsIn: 'template-animation-moveFromRight',
        forwardsOut: 'template-animation-moveToLeft',
        backwardsIn: 'template-animation-moveFromLeft',
        backwardsOut: 'template-animation-moveToRight'
      }
    },

    /**
     * An object that configurates the class name used for each direction for the 'horizontalEverForwards' animationType.
     */
    horizontalEverForwards: {
      type: Object,
      value: {
        forwardsIn: 'template-animation-moveFromRight',
        forwardsOut: 'template-animation-moveToLeft',
        backwardsIn: 'template-animation-moveFromRight',
        backwardsOut: 'template-animation-moveToLeft'
      }
    },

    /**
     * An object that configurates the class name used for each direction for the 'horizontalEverBackwards' animationType.
     */
    horizontalEverBackwards: {
      type: Object,
      value: {
        forwardsIn: 'template-animation-moveFromLeft',
        forwardsOut: 'template-animation-moveToRight',
        backwardsIn: 'template-animation-moveFromLeft',
        backwardsOut: 'template-animation-moveToRight'
      }
    },

    /**
     * An object that configurates the class name used for each direction for the 'verticalDownForwards' animationType.
     */
    verticalDownForwards: {
      type: Object,
      value: {
        forwardsIn: 'template-animation-static',
        forwardsOut: 'template-animation-moveToBottom',
        backwardsIn: 'template-animation-moveFromLeft',
        backwardsOut: 'template-animation-moveToRight'
      }
    },

    /**
     * An object that configurates the class name used for each direction for the 'verticalDownBackwards' animationType.
     */
    verticalDownBackwards: {
      type: Object,
      value: {
        forwardsIn: 'template-animation-moveFromRight',
        forwardsOut: 'template-animation-moveToLeft',
        backwardsIn: 'template-animation-static',
        backwardsOut: 'template-animation-moveToBottom'
      }
    },

    /**
     * An object that configurates the class name used for each direction for the 'verticalUp' animationType.
     */
    verticalUp: {
      type: Object,
      value: {
        forwardsIn: 'template-animation-moveFromBottom',
        forwardsOut: 'template-animation-static',
        backwardsIn: 'template-animation-static',
        backwardsOut: 'template-animation-moveToBottom'
      }
    },

    /**
     * An object that configurates the class name used for each direction for the 'static' animationType.
     */
    static: {
      type: Object,
      value: {
        forwardsIn: 'template-animation-static',
        forwardsOut: 'template-animation-static',
        backwardsIn: 'template-animation-static',
        backwardsOut: 'template-animation-static'
      }
    }
  },

  _animationTypeChanged: function _animationTypeChanged(animationType) {
    this.classList.toggle('static', animationType === 'static');
  },

  _findTemplateByState: function _findTemplateByState(state) {
    return this.parentNode.querySelector('[state="' + state + '"]');
  },

  /**
   * @param newState state template ('cached', 'inactive', 'active')
   */
  _stateChanged: function _stateChanged(newState) {
    if (newState === 'active') {
      var inactiveTemplate = this._findTemplateByState('inactive');
      if (inactiveTemplate) {
        this._animateWith(inactiveTemplate);
      } else {
        this.classList.add('template-is-visible');
        this.dispatchEvent(new CustomEvent(this.animationCompleteEvent, { bubbles: true, composed: true }));
      }
    }
  },

  _directAnimationForwards: function _directAnimationForwards(templateIn, templateOut, animationType) {
    templateIn.classList.add(animationType.forwardsIn);
    templateOut.classList.add(animationType.forwardsOut);

    templateOut.dispatchEvent(new CustomEvent('animation-forward', { bubbles: true, composed: true }));
  },

  _directAnimationBackwards: function _directAnimationBackwards(templateIn, templateOut, animationType) {
    templateIn.classList.add(animationType.backwardsIn);
    templateOut.classList.add(animationType.backwardsOut);

    templateOut.dispatchEvent(new CustomEvent('animation-backward', { bubbles: true, composed: true }));
  },

  _configureAnimationTypeActive: function _configureAnimationTypeActive(templateB) {
    var _this = this;

    var animationTypeIn = this.animationType;
    var animationTypeOut = templateB.animationType;

    var map = {
      'verticalDownBackwards': 'verticalDownBackwards',
      'verticalDownForwards': 'verticalDownForwards',
      'verticalUp': 'verticalUp',
      'staticEver': 'static',
      'horizontalEver': 'horizontal'
    };

    if (animationTypeOut === animationTypeIn) {
      [animationTypeOut, animationTypeIn].forEach(function (anim) {
        if (anim in map) {
          return _this[map[anim]];
        }
      });
    }

    if (animationTypeIn === 'horizontalEver' && animationTypeOut === 'static') {
      return this.horizontalEverForwards;
    }

    if (animationTypeOut === 'horizontalEver' && animationTypeIn === 'static') {
      return this.horizontalEverBackwards;
    }

    if (map[animationTypeOut]) {
      return this[map[animationTypeOut]];
    }

    if (map[animationTypeIn]) {
      return this[map[animationTypeIn]];
    }

    return this.horizontal;
  },

  _animateWith: function _animateWith(templateOut) {
    var reverse = false;
    var animationEndEvent = window.webkitAnimationEnd ? 'webkitAnimationEnd' : 'animationend';
    var animationTypeActive;

    if (this._templateN && this._templateN.get(templateOut)) {
      reverse = true;
    } else {
      templateOut._templateN = new WeakMap();
      templateOut._templateN.set(this, true);
    }

    this.listen(this, animationEndEvent, '_resetTemplate');
    this.listen(templateOut, animationEndEvent, '_resetTemplate');

    this.classList.add('template-animation-is-animating');
    templateOut.classList.add('template-animation-is-animating');
    animationTypeActive = this._configureAnimationTypeActive(templateOut);

    setTimeout(function () {
      if (!reverse) {
        this._directAnimationForwards(this, templateOut, animationTypeActive);
      } else {
        this._directAnimationBackwards(this, templateOut, animationTypeActive);
      }

      this.classList.add('template-is-visible');
    }.bind(this), 1000 / 60);
  },

  _resetTemplate: function _resetTemplate(evt) {
    if (evt.eventPhase !== Event.AT_TARGET) {
      return;
    }
    var node = evt.target;
    var regClsTemplate = /\btemplate-animation\S+/g;

    this.unlisten(node, evt.type, '_resetTemplate');
    node.className = (node.className || '').replace(regClsTemplate, '');
    if (node !== this) {
      var templateOut = node;
      templateOut.setAttribute('state', 'cached');
      templateOut.classList.remove('template-is-visible');
      templateOut.dispatchEvent(new CustomEvent('template-cached', { bubbles: true, composed: true }));
    } else {
      this.async(function () {
        this.dispatchEvent(new CustomEvent(this.animationCompleteEvent, { bubbles: true, composed: true }));
      }, 116);
    }
  }
};