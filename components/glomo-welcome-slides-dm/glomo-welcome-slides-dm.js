(function() {

  'use strict';

  Polymer({

    is: 'glomo-welcome-slides-dm',

    properties: {
      slides: {
        notify: true,
        type: Array,
        value: function() {
          return [
            {
              'animation': 'videos/welcome/card.apng',
              'title': 'Cuentas y tarjetas bajo control',
              'text': 'Gestiona todas tus cuentas y tarjetas de forma rápida y sencilla.',
              'backgroundColor': '#003E7B',
              'backgroundImage': 'https://bbva-files.s3.amazonaws.com/cells/assets/glomo/images/access/fractal-bbva-core-blue.svg',
              'delay': 6920
            },
            {
              'animation': 'videos/welcome/alert.apng',
              'title': 'Al tanto de todo',
              'text': 'Entérate al instante de cualquier movimiento importante en tus cuentas o tarjetas.',
              'backgroundColor': '#00CDCD',
              'backgroundImage': 'https://bbva-files.s3.amazonaws.com/cells/assets/glomo/images/access/fractal-bbva-teal.svg',
              'delay': 6360
            }
          ];
        }
      }
    }
  });
}());