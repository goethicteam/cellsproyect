(function() {
  /* global moment */
  'use strict';

  Polymer({

    is: 'cells-atom-date',

    properties: {

      /**
      * Selected date.
      * @type {String}
      */
      date: {
        type: String
      },

      /**
      * Selected date format.
      * @type {String}
      */
      format: {
        type: String,
        value: 'DD/MM/YYYY'
      },

      /**
       * Change localization of date
       * @type {String}
       */
      locale: {
        type: String,
        value: document.documentElement.lang
      },

      /**
       * Remove UTC from date
       * (It maintains in moment object)
       * @type {Boolean}
       */
      utc: {
        type: Boolean,
        observer: '_utcObserver'
      },

      /**
       * Set the utc Offset to moment Object
       * (Remove utc from moment object)
       * @type {Number}
       */
      utcOffset: {
        type: Number,
        observer: '_utcOffsetObserver'
      },

      /**
      * Formatted data.
      * @type {String}
      */
      _showDate: {
        type: String
      }
    },

    observers: [ '_formatDate(date, format, locale)' ],

    /**
    * To format the date. Modify the _showDate property to show the formatted data.
    * @param {String} date Date to format.
    * @param {String} format Type format.
    * @param {String} locale Force date with a specific locale.
    */
    _formatDate: function(date, format, locale) {
      var _date = moment(new Date(date));

      if (_date.isValid()) {
        if (locale) {
          _date = _date.locale(locale);
        }

        if (typeof(this.utcOffset) === 'number') {
          _date = this._applyUtcOffset(_date);
        }

        _date = _date.format(format);
      } else {
        _date = '';
      }

      this.set('_showDate', _date);
    },

    _utcObserver: function(newVal, oldVal) {
      /* istanbul ignore else */
      if (newVal === true) {
        this.set('_showDate', moment(this.date).utc().format(this.format));
      }
    },

    _utcOffsetObserver: function(newVal, oldVal) {
      /* istanbul ignore else */
      if (this.date && typeof(newVal) === 'number') {
        this.set('_showDate', this._applyUtcOffset(this.date).format(this.format));
      }
    },

    _applyUtcOffset: function(date) {
      return moment(date).utcOffset(this.utcOffset);
    }
  });

}());
