# cells-atom-date

[![Certificated](https://img.shields.io/badge/certificated-yes-brightgreen.svg)](http://bbva-files.s3.amazonaws.com/cells/bbva-catalog/index.html)

The `<cells-atom-date>` component draws a date in a specific format.

Example:

```html
<cells-atom-date date="2016-08-18" format="MM/YYYY" locale="es"></cells-atom-date>
```
The `date` attribute is required.

```html
<cells-atom-date date="2016-08-18"></cells-atom-date>
```
If the `format` attribute is not used, the format used is `DD/MM/YYYY`. The values of this attribute are the admitted by `moment.js (see <http://momentjs.com/docs/#/displaying/format/>)

You can set the `locale` attribute to other language in short format (ex: 'en', 'es'). By default, the system get the CELLS language settings.

In addition, set the `utc` attribute to show date without depending on locale timezone.

## Styling

The following custom properties and mixins are available for styling:

| Custom property | Description     | Default        |
|:----------------|:----------------|:--------------:|
| --cells-atom-date-scope      | scope description | default value  |
| --cells-atom-date  | empty mixin     | {}             |
| --cells-atom-date-color | atom date color | #003f8C
