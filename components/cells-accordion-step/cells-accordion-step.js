(function() {

  'use strict';

  Polymer({

    /**
     * Fired when the component content is opened
     * @event cells-accordion-step-opened
     */

    /**
     * Fired when the component content is closed
     * @event cells-accordion-step-closed
     */

    /**
     * Fired when the validation state is valid
     * @event cells-accordion-step-valid
     */

    /**
     * Fired when the validation state is invalid
     * @event cells-accordion-step-invalid
     */

    /**
     * Fired when the component open transition finishes
     * @event cells-accordion-step-opened-animated
     */

    /**
     * Fired when the component close transition finishes
     * @event cells-accordion-step-closed-animated
     */

    is: 'cells-accordion-step',

    listeners: {
      'tap': '_toggle',
      'keydown': '_toggleKeyPress',
      'collapse.iron-resize': '_onIronAnimationFinish'
    },

    properties: {
      /**
       * Use 'has-validation' boolean attribute if the component has validation.
       */
      hasValidation: {
        type: Boolean,
        value: false
      },

      /**
       * Use 'is-valid' boolean attribute in a component that also has 'has-validation'
       * attribute to indicate its inital validation state. This attribute is changed
       * automatically when the validation state changes.
       */
      isValid: {
        type: Boolean,
        value: false,
        reflectToAttribute: true,
        notify: true,
        observer: '_onValidStateChange'
      },

      /**
       * Boolean attribute to set the accordion initially opened.
       */
      opened: {
        type: Boolean,
        value: false,
        notify:true,
        observer: '_onOpenedChange',
        reflectToAttribute: true
      },

      /**
       * Title of the step.
       */
      title: {
        type: String,
        value: ''
      },

      /**
       * Secondary text in the step placed at the right side.
       */
      message: {
        type: String,
        value: ''
      },

      /**
       * Use 'is-amount' boolean attribute to indicate that the secondary
       * text is an amount with currency symbol.
       */
      isAmount: {
        type: Boolean,
        value: false
      },

      /**
       * 'Amount' number attribute allows to enter the amount quantity when 'is-amount' is true.
       */
      amount: Number,

      /**
       * Use 'disabled' boolean attribute to disable collapsible behavior of the component.
       */
      disabled: {
        type: Boolean,
        value: false,
        reflectToAttribute: true
      },

      /**
       * Use 'disabledKeyPress' boolean attribute to disable collapsible behavior of the component from keyboard.
       */
      disabledKeyPress: {
        type: Boolean,
        value: false
      },

      /**
       * ISO 4217 code for the currency when 'is-amount' attribute is used.
       */
      currencyCode: String,

      /**
       * Default step icon
       */
      iconMid: {
        type: String,
        value: ''
      },
      /**
       * First step icon
       */
      iconFirst: {
        type: String,
        value: ''
      },
      /**
       * Last step icon
       */
      iconLast: {
        type: String,
        value: ''
      },

      /**
       * Icon right
       */
      iconRight: {
        type: String,
        value: ''
      },

      _disabledFocus: {
        type: Number,
        value: 0,
        computed: '_checkDisabledFocus(disabled)'
      }
    },

    _toggle: function(ev) {
      var e = Polymer.dom(ev);
      var eTarget = this.shadowRoot.querySelector('.js-content');
      if (!this.disabled && e.path.indexOf(eTarget) < 0) {
        this._action('toggle');
      }
    },

    _toggleKeyPress: function(ev) {
      var e = Polymer.dom(ev);
      var eTarget = this.shadowRoot.querySelector('.js-content');
      if (!this.disabled && e.path.indexOf(eTarget) < 0 && !this.disabledKeyPress && (e.event.keyCode === 13 || e.event.keyCode === 32)) {
        ev.preventDefault();
        this._action('toggle');
      }
    },

    _checkDisabledFocus: function(elem) {
      var dis = elem ? -1 : 0;
      return dis;
    },

    /**
     * Opens the collapsible content
     */
    open: function() {
      this._action('show');
    },

    /**
     * Closes the collapsible content
     */
    close: function() {
      this._action('hide');
    },

    _action: function(action) {
      /* action === (show | hide | toggle)*/
      var ironCollapse = this.shadowRoot.querySelector('iron-collapse');
      ironCollapse[action]();
      this.opened = ironCollapse.opened;
    },

    _onValidStateChange: function() {
      var classNameSuccess = 'is-valid';

      if (!this.hasValidation) {
        this.classList.toggle(classNameSuccess, false);
        return;
      }

      if (this.isValid) {
        this.classList.toggle(classNameSuccess, true);
        this.fire('cells-accordion-step-valid');
      } else {
        this.classList.toggle(classNameSuccess, false);
        this.fire('cells-accordion-step-invalid');
      }
    },

    _onOpenedChange: function(newValue, oldValue) {
      if (newValue !== undefined) {
        this.toggleClass('is-open', newValue, this);
        this.transition = true;
        if (newValue) {
          this.fire('cells-accordion-step-opened');
          this.shadowRoot.querySelector('.js-button').setAttribute('aria-expanded', 'true');
        } else {
          this.fire('cells-accordion-step-closed');
          this.shadowRoot.querySelector('.js-button').setAttribute('aria-expanded', 'false');
        }
      }
    },

    _onIronAnimationFinish: function() {
      if (this.transition && this.opened) {
        this.fire('cells-accordion-step-opened-animated');
      } else if (this.transition) {
        this.fire('cells-accordion-step-closed-animated');
      }
      this.transition = false;
    }

  });
}());
