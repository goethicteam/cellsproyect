'use strict';

Polymer({

  /**
   * @event template-content-scroll
   * Fired when the content has been scrolled.
   */

  is: 'cells-template-zones',
  hostAttributes: {
    'data-template': true
  },

  behaviors: [window.CellsBehaviors.TemplateZonesAnimationBehavior],

  properties: {

    /**
     * If true, swipe to open/close the drawer is disabled.
     * Default:false
     */
    disableEdgeSwipe: {
      type: Boolean,
      value: false
    },

    /**
     * If true, main content animation scale.
     * Default:false
     */
    scaleContent: {
      type: Boolean,
      value: false
    },

    /**
     * Width of the drawer panel.
     */
    drawerWidth: {
      type: String
    },

    /**
     * Hides the header.
     */
    headerHidden: {
      type: Boolean,
      value: false
    },

    /**
     * How many pixels on the side of the screen are sensitive to edge swipes and peek.
     */
    edgeSwipeSensitivity: {
      type: Number,
      value: 20
    },

    /**
     * Choose add css attribute position:relative in main content ('false', or 'true').
     * Default:false
     */
    relativeMain: {
      type: Boolean,
      value: false,
      reflectToAttribute: true
    },

    /**
     * If true, the header is fixed to the top and never moves away.
     */
    headerFixed: {
      type: Boolean,
      value: false
    },

    /**
     * If true, the footer is fixed to the bottom and never moves away.
     */
    footerFixed: {
      type: Boolean,
      value: false
    },

    /**
     * Component responsible for managing the scroll of their children
     */
    scrollerControl: {
      type: String,
      value: 'cells-app-scroll'
    },

    /**
     * Footer height in px.
     * Can be set either by using a custom CSS property (--app__footer-height) or by setting
     * footerHeight. Default value for --app__footer-height is 60px.
     */
    footerHeight: {
      type: Number,
      observer: '_updateFooterHeight'
    },

    /**
     * Set to true if the template has footer.
     */
    hasFooter: {
      type: Boolean,
      value: false,
      reflectToAttribute: true
    },

    /**
     * Selected panel: 'drawer' or 'main'.
     */
    selected: {
      type: String,
      value: 'main',
      notify: true,
      readOnly: true
    },

    /**
     * Remove header height.
     */
    floatingHeader: {
      type: Boolean,
      reflectToAttribute: true,
      value: false
    },

    /**
     * Notifies if the menu is opened or not.
     */
    menuOpened: {
      type: Boolean,
      value: false,
      notify: true,
      readOnly: true,
      reflectToAttribute: true
    },

    /**
     * Sets wheter to disable scrolling when the menu is opened.
     */
    disableScrollLock: {
      type: Boolean,
      value: false
    },

    /**
     * Notifies number of configurations items
     */
    configurationsNumber: {
      type: Number,
      value: 0,
      notify: true
    },

    /**
     * Notifies that configurations must show
     */
    showConfigurations: {
      type: Boolean
    },

    /**
     * Binds cells-sidebar-wow-panel's peeking property
     * @type {Boolean}
     */
    peeking: {
      type: Boolean,
      notify: true
    },

    /**
     * Binds cells-sidebar-wow-panel's dragging property
     * @type {Boolean}
     */
    dragging: {
      type: Boolean,
      notify: true
    },

    /**
     * Sets a scroll margin bottom. By default 5
     * @type {Number}
     */
    scrollBottomMarginError: {
      type: Number,
      value: 5
    },

    /**
     *Sets if _isScrolledToBottom has been launched
     * for the first time
     */
    isScrollInit: {
      type: Boolean,
      value: false
    }
  },

  listeners: {
    'overlay-opening': '_removeOverflow',
    'overlay-closed': '_addOverflow',
    'toggle-menu': 'toggleMenu',
    'state-no-visible': '_resetLayerScroll',
    'change-template-animation': '_onChangeAnimation'
  },

  observers: ['_setupFooter(hasFooter, footerFixed)'],

  /**
   * toggle menu
   */
  toggleMenu: function toggleMenu() {
    Polymer.dom(this.root).querySelector('#app-container').togglePanel();
  },

  /**
   * Sets configurations number
   * @param {Number}
   */
  checkConfigurationsNumber: function checkConfigurationsNumber(configurations) {
    this.set('configurationsNumber', configurations);
  },

  /**
   * @event set-scroll-bottom-event
   * Fires event when scroll is bottom
   * @param {Event} scroll event
   */
  _isScrolledToBottom: function _isScrolledToBottom(ev) {
    this.set('isScrollInit', true);
    var scrolledHeight = ev.detail.target.scrollHeight - ev.detail.target.scrollTop;
    var clientHeight = ev.detail.target.clientHeight;
    if (scrolledHeight <= clientHeight + this.scrollBottomMarginError) {
      this.dispatchEvent(new Event('set-scroll-bottom-event', {
        bubbles: true,
        composed: true
      }));
    }
  },

  _onSelectedChanged: function _onSelectedChanged(e) {
    this._setSelected(e.detail.value);

    if (e.detail.value === 'drawer') {
      this._setMenuOpened(true);
      if (!this.disableScrollLock) {
        this._removeOverflow();
      }
    } else {
      this._setMenuOpened(false);
      if (!this.disableScrollLock) {
        this._addOverflow();
      }
    }
  },

  /**
   * Add scroll bar
   */
  _addOverflow: function _addOverflow() {
    this.classList.remove('overflow-initial');
  },

  /**
   * Remove scroll bar
   */
  _removeOverflow: function _removeOverflow() {
    this.classList.add('custom-transition');
    this.classList.add('overflow-initial');
  },

  /**
   * Move scroll to 0
   */
  _resetLayerScroll: function _resetLayerScroll() {
    Polymer.RenderStatus.afterNextRender(this, function () {
      this.set('isScrollInit', false);
    });
  },

  _onChangeAnimation: function _onChangeAnimation(ev) {
    ev.stopPropagation();
    this.animationType = ev.detail.animationType;
  },

  /**
   * @event template-content-scroll
   * Fires event on change scroll
   * calls _isScrolledToBottom  method to calculate
   * scroll position
   * launched directly the first time
   * the following times function will be launched
   * after 100 milliseconds
   * @param {Event} scroll event
   */
  _onContentScroll: function _onContentScroll(ev) {
    ev.stopPropagation();
    if (this.isScrollInit) {
      this.debounce('on-change-scroll', function () {
        this._isScrolledToBottom(ev);
        this.dispatchEvent(new CustomEvent('template-content-scroll', {
          bubbles: true,
          composed: true,
          detail: {
            scrollTop: ev.detail.positionScroll,
            target: ev.detail.target
          }
        }));
      }, 100);
    } else {
      this._isScrolledToBottom(ev);
    }
  },

  _updateFooterHeight: function _updateFooterHeight(footerHeight) {
    if (footerHeight) {
      this.customStyle['--app__footer-height'] = footerHeight + 'px';
      this.updateStyles();
    }
  },

  _setupFooter: function _setupFooter(hasFooter, footerFixed) {
    if (!hasFooter) {
      this.unlisten(this.$.app__section, 'cells-app-scroll-transform', '_onHeaderTransform');
      return;
    }

    if (!footerFixed) {
      this.listen(this.$.app__section, 'cells-app-scroll-transform', '_onHeaderTransform');
    } else {
      this.unlisten(this.$.app__section, 'cells-app-scroll-transform', '_onHeaderTransform');
    }

    this.classList.toggle('footer-fixed', footerFixed);
  },

  _onHeaderTransform: function _onHeaderTransform(e) {
    this._footer = this._footer || this.$.app__footer;
    this.transform('translate3d(0, ' + Math.abs(e.detail.y) + 'px, 0)', this._footer);
  }

});