# Changelog

All notable changes to this project will be documented in this file.

The format is based on [Keep a Changelog](http://keepachangelog.com/)
and this project adheres to [Semantic Versioning](http://semver.org/).

## [Unreleased]

## [0.1.0] - 2017-10-02

### Added

- `getLastResponse` public method to able to retrieve last response received
- `path` public property to set request path.
- `method` public property to indicate request method.
- `queryParams` public property to set request query params.
- `body` public property to set request body.
- `headers` public property to set request headers.
- A functional demo of this DM.
- Unit tests to reach a good coverage (100%)
- Documentation of all the properties, methods and events.


[Unreleased]: https://globaldevtools.bbva.com/bitbucket/projects/BBVACELLSAPI/repos/cells-psv-generic-dp/compare/commits?targetBranch=refs%2Fheads%2Fmaster&sourceBranch=refs%2Fheads%2Fdevelop
[0.1.0]: https://globaldevtools.bbva.com/bitbucket/projects/BBVACELLSAPI/repos/cells-psv-generic-dp/commits?until=refs%2Fheads%2Frelease%2F0.1.0
