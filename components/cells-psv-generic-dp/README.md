# cells-psv-generic-dp

[![v0.1.0](https://img.shields.io/badge/version-0.1.0-brightgreen.svg)](https://globaldevtools.bbva.com/bitbucket/projects/BBVACELLSAPI/repos/cells-psv-generic-dp/browse?at=refs%2Ftags%2F0.1.0)

> Allows to perform an AJAX request for PSV application.

Example:

```html
<cells-psv-generic-dp
  id="myDataProvider"
  endpoint="http://some_host"
  path="some_endpoint_path"
  query-params="{'key':'value'}"
  body="{'key':'value'}"
  method="GET">
</cells-psv-generic-dp>
```

```javascript
myEl.$.myDataProvider.generateRequest().then(
  function(response) {
    //this is for success
    //to get lastResponse object, parse `request` argument or use `myEl.getLastResponse()`
  },
  function(err) {
    //this is for errors
    //to get request payload, use `myEl.getLastResponse()`
  }
);
```

## Dependencies

* [cells-ajax-behavior](https://globaldevtools.bbva.com/bitbucket/projects/CBH/repos/cells-ajax-behavior/browse)

## Styling

This component has no visual representation.

## API

Properties:

| NAME | Description | Type | Binding |
|:---|:---|:---:|:---|
| path | Service endpoint path | String | IN |
| queryParams | Query params to be sent into URI request. | Object | IN |
| method | HTTP Request Method | String | IN |
| body | Request payload to be sent | Object | IN |
| headers | Additional headers to be sent as part of XMLHttpRequest | Object | IN |
| getLastResponse() | Returns the last response provided by the API | Object | OUT |

Default value for **method** property is *"GET"*; other available methods [here](https://www.w3.org/Protocols/rfc2616/rfc2616-sec9.html).

URI request is computed from **endpoint** and **path** properties. *endpoint* is provided by [cells-ajax-behavior](https://globaldevtools.bbva.com/bitbucket/projects/CBH/repos/cells-ajax-behavior/browse).

Default headers to be sent are:

```javascript
_headers = {
  'Accept': 'application/json, text/javascript, */*; q=0.01',
  'Content-Type': 'application/json'
}
```

If **tsec** is stored in `window.sessionStorage`, then default headers are:

```javascript
_headers = {
  'Accept': 'application/json, text/javascript, */*; q=0.01',
  'Content-Type': 'application/json',
  tsec: window.sessionStorage.getItem('tsec')
}
```

To override or add new headers, just populate **headers** property, in example:

```html
<cells-psv-generic-dp headers="{'myHeader':'myHeaderValue'}"></cells-psv-generic-dp>
```

```javascript
_headers = {
  'Accept': 'application/json, text/javascript, */*; q=0.01',
  'Content-Type': 'application/json',
  myHeader: 'myHeaderValue'
}
```


