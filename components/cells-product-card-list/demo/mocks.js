/* eslint-disable */
var accountsMock = [
  {
    "id": "2002",
    "name": "Cuenta personal C",
    "description": {
      "value": "050400010100001604",
      "masked": true
    },
    "primaryAmount": {
      "amount": 70000,
      "currency": "CLP",
      "label": "Available"
    },
    "scale": 0,
    "status": {
      "id": "ACTIVATED",
      "description": "ACTIVATED"
    }
  },
  {
    "id": "2003",
    "name": "Cuenta en dólares",
    "description": {
      "value": "050400010100001605",
      "masked": true
    },
    "primaryAmount": {
      "amount": 1700000,
      "currency": "CLP",
      "label": "Available"
    },
    "scale": 0,
    "status": {
      "id": "ACTIVATED",
      "description": "ACTIVATED"
    }
  },
  {
    "id": "2004",
    "name": "Cuenta divisa",
    "description": {
      "value": "050400010100001606",
      "masked": true
    },
    "primaryAmount": {
      "amount": 1350000,
      "currency": "CLP",
      "label": "Available"
    },
    "scale": 0,
    "status": {
      "id": "ACTIVATED",
      "description": "ACTIVATED"
    }
  },
  {
    "id": "2001",
    "name": "Cuenta corriente lcred",
    "description": {
      "value": "050400010100001603",
      "masked": true
    },
    "primaryAmount": {
      "amount": 1900000,
      "currency": "CLP",
      "label": "Available"
    },
    "scale": 0,
    "status": {
      "id": "ACTIVATED",
      "description": "ACTIVATED"
    }
  },
  {
    "id": "2006",
    "name": "Cuenta corriente",
    "description": {
      "value": "050400010100001608",
      "masked": true
    },
    "primaryAmount": {
      "amount": 1800000,
      "currency": "CLP",
      "label": "Available"
    },
    "scale": 0,
    "status": {
      "id": "ACTIVATED",
      "description": "ACTIVATED"
    }
  },
  {
    "id": "2005",
    "name": "Cuenta bono",
    "description": {
      "value": "050400010100001607",
      "masked": true
    },
    "primaryAmount": {
      "amount": 12000,
      "currency": "CLP",
      "label": "Available"
    },
    "scale": 0,
    "status": {
      "id": "ACTIVATED",
      "description": "ACTIVATED"
    }
  }
];
