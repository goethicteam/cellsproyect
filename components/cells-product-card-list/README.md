![Component Screenshot](cells-product-card-list.png)

# cells-product-card-list

[![Certificated](https://img.shields.io/badge/certificated-yes-brightgreen.svg)](http://bbva-files.s3.amazonaws.com/cells/bbva-catalog/index.html)
[Demo of component in Cells Catalog](http://bbva-files.s3.amazonaws.com/cells/bbva-catalog/index.html#/elements/cells-product-card-list)

`<cells-product-card-list>` displays a list of accounts or movements using `<cells-product-card>`.

The skeleton or loader element can be shown by setting the boolean property `loading` to `true`.
Also, the default skeleton component can be replaced by a custom one provided in a slot with name `loader`.

Example:

```html
<cells-product-card-list></cells-product-card-list>
```

Example with a custom skeleton and a custom error:

```html
<cells-product-card-list loading items='[...]'>
  <custom-skeleton slot="loader"></custom-skeleton>
  <custom-message-error slot="error-content"></custom-message-error>
</cells-product-card-list>
```

## Icons

Since this component uses icons, it will need an [iconset](https://bbva.cellsjs.com/guides/best-practices/cells-icons.html) in your project as an [application level dependency](https://bbva.cellsjs.com/guides/advanced-guides/application-level-dependencies.html). In fact, this component uses an iconset in its demo.

## Styling

The following custom properties and mixins are available for styling:

| Custom property                            | Description                                 |             Default              |
|:-------------------------------------------|:--------------------------------------------|:--------------------------------:|
| --cells-product-card-list-card-color       | Card text color                             |               #fff               |
| --cells-product-card-list-heading-color    | List heading color                          |               #fff               |
| --cells-product-card-list-subheader-color  | Subheader text color                        |               #fff               |
| --cells-product-card-list-amount-color     | Amount text color                           |               #fff               |
| --cells-product-card-list-amount           | Mixin applied to amount                     |                {}                |
| --cells-product-card-list                  | Mixin applied to :host                      |                {}                |
| --cells-product-card-list-header           | Mixin applied to the header                 |                {}                |
| --cells-product-card-list-error-message    | Mixin applied to error message              |                {}                |
| --cells-product-card-list-list             | Mixin applied to the list                   |                {}                |
| --cells-product-card-list-list-item        | Mixin applied to list li items              |                {}                |
| --cells-product-card-list-item-bg-color    | Background color for list                   |             #004481              |
| --cells-product-card-list-item-box-shadow  | Box shadow for card                         | 0px 4px 8px 0 rgba(0, 0, 0, 0.3) |
| --cells-product-card-list-item             | Mixin applied to list item                  |                {}                |
| --cells-product-card-list-item-bg-color    | List item background color                  |             #1464A5              |
| --cells-product-card-list-item-active      | Mixin applied to list item when active      |                {}                |
| --cells-product-card-list-default-skeleton | Mixin applied to the default skeleton       |                {}                |
| --cells-product-card-medium-list-item      | Mixin applied to list item on medium screen |                {}                |
| --cells-product-card-large-list-item       | Mixin applied to list item on large screen  |                {}                |
