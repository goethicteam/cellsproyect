(function() {

  'use strict';

  Polymer({

    is: 'cells-button-group',

    behaviors: [
      window.CellsBehaviors.i18nBehavior
    ],

    properties: {
      buttons: Array
    },

    _fireAction: function(e) {
      var action = e.target.attributes['custom-event'].value;
      this.fire(action);
    },

    changeStatus: function(data) {
      if (data.buttonActive) {
        this.set('buttons.' + data.buttonIndex + '.disabled', false);
      } else {
        this.set('buttons.' + data.buttonIndex + '.disabled', true);
      }
    }
  });

}());