/* global slides */

document.addEventListener('WebComponentsReady', function() {
  var demo = document.getElementById('demo');
  var ui = demo.$.ui;
  demo.slides = slides;

  function onMessage(e) {
    Object.keys(e.data).forEach(function(prop) {
      ui.set(prop, e.data[prop]);
    });
  }

  window.addEventListener('message', onMessage, false);
});
