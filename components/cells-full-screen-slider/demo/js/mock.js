var slides = [{
  animation: 'images/mosquito.png',
  title: 'Slider Title 1',
  text: 'Lorem ipsum dolor sit amet, consectetur adipisicing elit. Ipsam, eum?',
  backgroundColor: '#E96062'
}, {
  image: 'images/1476324248_cash-money-wallet.svg',
  title: 'Slider Title 2',
  text: 'Lorem ipsum dolor sit amet, consectetur adipisicing elit. Eius, nihil, ducimus.',
  backgroundColor: '#409F7C'
}, {
  animation: 'images/firefox.png',
  title: 'Slider Title 3',
  text: 'Lorem ipsum dolor sit amet, consectetur adipisicing elit. Itaque reprehenderit, eligendi esse!',
  backgroundColor: '#17B7B5'
}, {
  image: 'images/1476324243_diamond.svg',
  title: 'Slider Title 4',
  text: 'Lorem ipsum dolor sit amet, consectetur adipisicing elit.',
  backgroundColor: '#98A7B2'
}];
