# cells-myb-ui-theme

Your component description.

Example:
```html
<cells-myb-ui-theme></cells-myb-ui-theme>
```

```

## Styling

The following custom properties and mixins are available for styling:

| Custom property | Description     | Default        |
|:----------------|:----------------|:--------------:|
| --cells-myb-ui-theme-scope      | scope description | default value  |
| --cells-myb-ui-theme  | empty mixin     | {}             |
