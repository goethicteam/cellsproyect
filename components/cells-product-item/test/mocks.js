var mocks = {
  name: 'Cuenta de Ahorro',
  imgSrc: 'url/to/image.jpg',
  description: {
    value: 'abcdefg',
    masked: true
  },
  primaryAmount: {
    label: 'Disponible',
    amount: 288,
    currency: 'EUR'
  },
  secondaryAmount: {
    label: 'Dispuesto',
    amount: 13,
    currency: 'EUR'
  }
}
