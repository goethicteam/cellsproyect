# Changelog

## v3.0.0

### Breaking changes

| Removed mixins |
|:---------------|
| --bbva-core-blue-fractal-background |
| --box-rounded |
| --box-rounded-shadow |

### New color names

Vars named `--bbva-teal-*` are now `--bbva-aqua-*`. The old var names are mapped to the new ones for compatibility but they might be removed at some time in the future.

