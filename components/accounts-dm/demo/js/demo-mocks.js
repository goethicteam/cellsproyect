/**
 * Auxiliary functions for the demo.
 * It holds common functions that are used by mode than one demo case
 */
var customFunctions = {

  showSuccessMessage: function(event) {
    this.feedbackType = 'success';
    this.feedbackIcon = 'coronita:correct';
    this.feedbackMessage = 'User <b>' + event.detail + '</b> logged with success';
  },

  showErrorMessage: function(event) {
    this.feedbackType = 'error';
    this.feedbackIcon = 'coronita:error';

    if (!event.detail.data) {
      event.detail.data = {
        'error-code': '-1',
        'error-message': 'Error calling the server'
      };
    }

    this.feedbackMessage = 'Error when login user <b>' + event.detail.userId +
      '</b><br/><br/> Error: ' + event.detail.data['error-code'] + ' - ' + event.detail.data['error-message'];
  },

  showLogoutMessage: function(event) {
    this.feedbackType = 'info';
    this.feedbackIcon = 'coronita:info';
    this.feedbackMessage = 'User <b>' + event.detail + '</b> logged out with success';
  },

  resetFeedback: function(scope) {
    scope.feedbackType = null;
    scope.feedbackIcon = null;
    scope.feedbackMessage = null;
  },

  setLoadingTrue: function(event, spinner, scope) {
    this.resetFeedback(scope);
    spinner.show();
  },

  setLoadingFalse: function(event, spinner, scope) {
    spinner.hide();
  },

  isLogged: function(ev) {
    this.isLoggedValue = ev.detail.userId ? true : false;
  }
};
