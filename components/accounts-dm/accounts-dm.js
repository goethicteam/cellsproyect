'use strict';

(function () {
  'use strict';

  var tokenProperty = 'token';
  var userIdProperty = 'userId';

  Polymer({

    /**
     * The event indicating a login was performed successfully.
     * @event login-success
     * @param {string} userId - The userId of the user logged.
     */ /**
        * The event indicating a login have failed.
        * @event login-error
        * @param {object} data - The error data returned by the server.
        * @param {object} userId - The userId of the user logged.
        * @param {string} consumerId - The {@link consumerId} object with consumer Id.
        * @param {string} authenticationType - The {@link authenticationType} object with authentication type.
        * @param {string} idAuthenticationData - The {@link idAuthenticationData} object with id authentication data.
        */ /**
           * The event indicating a logout was performed successfully.
           * @event logout-success
           * @param {string} userId - The userId of the user logged out.
           */ /**
              * The event indicating a that userId was changed.
              * @event user-id-changed
              * @param {string} userId - The new userId
              */

    is: 'accounts-dm',

    properties: {

      /**
       * The base URL for the api to request
       * @type {String}
       */
      baseUrl: {
        type: String,
        value: ''
      },

      /**
       * The base Path to the resource
       * @type {String}
       */
      basePath: {
        type: String,
        value: ''
      },

      /**
       * The login type to be used to login the user. There are 2 values supported:
       * `webseal` that uses JWT + Granting Ticket and 'gt' that uses only Granting Ticket
       * @type {string}
       * @default "password"
       */
      loginType: {
        type: String,
        value: 'password'
      },
      /**
       * Authentication type.
       * Required to login.
       * @type {string}
       * @default 'password'
       */
      authenticationType: {
        type: String,
        value: 'password'
      },

      /**
       * The user id of the user logged
       * @type {string}
       */
      userId: {
        type: Object
      },

      /**
       * JWT payload request must be sent as a plain text
       * @type {object}
       * @private
       */
      _jwtRequestHeaders: {
        type: Object,
        readOnly: true,
        value: function value() {
          return {
            'Accept': 'application/json, text/javascript, */*; q=0.01',
            'Content-Type': 'application/x-www-form-urlencoded'
          };
        }
      }
    },

    /**
     * Function that removes the tsec and userId stored in the session store
     * and also set the userId to null, which will fire the event of userId changed
     * @emits user-id-changed
     */
    resetAccounts: function resetAccounts() {
      sessionStorage.removeItem('accounts');
    },


    /**
     * Generate the payload to send in the request message to the login service
     * according to the {@link grantingTicketType}
     * @param {object} user - The {@link user} object with userId and password
     * @param {string} consumerId - The {@link consumerId} object with consumer id
     * @param {string} authenticationType - The {@link authenticationType} object with authentication type
     * @param {string} idAuthenticationData - The {@link idAuthenticationData} object with id authentication data.
     * @param {Payload} The payload to be sent to the login server with the user information
     * @private
     */
    _generatePayload: function _generatePayload(user, consumerId, authenticationType, idAuthenticationData) {
      var data = {
        password: user.password,
        username: user.userId,
        grant_type: "password",
        client_id: null,
        client_secret: null
      };
      return data;
    },

    /**
     */
    accounts: function accounts() {
      this._accounts(sessionStorage.getItem('userId'));
    },
    /*
    *
    */
    updateAccount: function updateAccount(user, amount, account){
      console.log('updateAccount');
      var accounts= JSON.parse(sessionStorage.getItem('accounts'));
      var status;
      var find=false;
      var i=0;
      while(!find){
        if (accounts[i].id==account){
          find=true;
          status = accounts[i].balance-amount;
        }
        i=i+1;
      }
      var actual={
        balance:status
      };
      console.log('updateAccount2');
      this._updateAccount(sessionStorage.getItem('userId'),actual,account);
    },
    /**
    *
    */
    addAccountTransaction: function addAccountTransaction(data){
      console.log("add transaccions "+data.id);
      var category= {
        id: 6,
        name: "TRANSFER"
      };

      var subCategory = {
        id: 62,
        name: "SENT TRANSFER"
      };

      var reqdata= {
        id: data.id,
        amount: data.amount*-1,
        currency: "EUR",
        category: {
          id: 6,
          name: "TRANSFER"
        },
        subCategory: {
          id: 62,
          name: "SENT TRANSFER"
        },
        description: "Transferencia",
        operationDate: new Date().toJSON().slice(0,35),
        valueDate: new Date().toJSON().slice(0,35)

      };
      console.log(reqdata);

      this._addAccountTransaction(sessionStorage.getItem('userId'),reqdata, data.account);
      this.updateAccount(sessionStorage.getItem('userId'),data.amount,data.account);
    },
    /**
    * obtain user accounts
    */
    _accounts: function _accounts(user) {
      var requestData = this._generatePayload(user);
      //
      var gtDP = this.$.gtRequest;
      this._request(gtDP, {
        processData: false,
        provider: 'gt',
        base: this.baseUrl,
        path: '' + this.basePath,
        data: requestData,
        method: 'GET',
        success: function (data, response) {
          this._onAccountsSuccess(data, response, user);
        }.bind(this),
        error: function (data) {
          this._onAccountsError(data, user);
        }.bind(this)
      });
    },

    /*
    *
    */
    _updateAccount: function _updateAccount(user, requestData, account) {
      //var requestData = this._generatePayload(user);
      //
      var gtDP = this.$.gtRequest;
      this._request(gtDP, {
        provider: 'gt',
        base: this.baseUrl,
        path: '' + this.basePath+'/'+account,
        data: requestData,
        method: 'PUT',
        success: function (data, response) {
          this._onUpdateAccountSuccess(data, response, user);
        }.bind(this),
        error: function (data) {
          this._onAccountsError(data, user);
        }.bind(this)
      });
    },
    /*
    *_addAccountTransaction
    */
    _addAccountTransaction: function _addAccountTransaction(user, requestData, account) {
      console.log("addAccount");
      var auth='';
      var user='';
      if (sessionStorage.getItem('token')){
          auth = 'Authorization: Bearer '+ sessionStorage.getItem('token');
          user = sessionStorage.getItem('userId');
      }

      this.$.accountsdp.host = this.baseUrl+this.basePath+'/'+account;
      this.$.accountsdp.method = "PATCH";
      this.$.accountsdp.headers = {
        'Authorization': auth+'',
        'UserID': user+'',
        'Accept':"*/*", "Content-Type": "application/json"
      };
        this.$.accountsdp.addEventListener('request-success', function() {
          this.fire('transfer-ok', account);
      });
        this.$.accountsdp.addEventListener('request-error', function() {


      });

      this.$.accountsdp.set('body', requestData);
      this.$.accountsdp.generateRequest();


      // var gtDP = this.$.gtRequest;
      // this._request(gtDP, {
      //   provider: 'gt',
      //   base: this.baseUrl,
      //   path: '' + this.basePath+'/'+account,
      //   data: requestData,
      //   method: 'PATCH',
      //   success: function (data, response) {
      //     this._onAddAccountTransacctionSuccess(data, response, user, account);
      //   }.bind(this),
      //   error: function (data) {
      //     this._onAccountsError(data, user);
      //   }.bind(this)
      // });
    },
    /**
     *
     */
    _onAccountsSuccess: function _onAccountsSuccess(data, response, userId) {
      sessionStorage.setItem('accounts',JSON.stringify(response.response));
      this.fire('accounts-ok', response);
    },

    /*
    *
    */
    _onUpdateAccountSuccess: function _onUpdateAccountSuccess(data, response, userId) {
      //sessionStorage.setItem('accounts',JSON.stringify(response.response));
      //this.fire('accounts-ok', response);
    },
    /*
    *
    */
    _onAddAccountTransacctionSuccess: function _onAddAccountTransacctionSuccess(data, userId, account) {
      this.fire('transfer-ok', account);
      this._accounts(userId);
      //this.fire('accounts-ok', response);
    },
    /**
     *
     */
    removeAccount: function removeAccount(id) {
      var user = this.user;
      var gtDP = this.$.gtRequest;
      this._request(gtDP, {
        base: this.baseUrl,
        path: '' + this.basePath,
        method: 'DELETE',
        success: function (data) {
          this._onDeleteSuccess(data, id);
        }.bind(this)
      });
    },

    /**
     *
     */
    _onDeleteSuccess: function _onLogoutSuccess(id) {
      this.accounts();
    },

    /**
    **/

    _request: function _request(dp) {
      var config = arguments.length > 1 && arguments[1] !== undefined ? arguments[1] : {};
      config.data = config.data || {};
      this._prepareDataProvider(dp, config);
      var request = this._generateRequest(dp, config);
      this.fire('request-start', {
        request: request,
        config: config
      });
    },

    /**
     * Updates the **data provider** properties to perform the request.
     * @param {RequestConfig} config - The configuration object passed to generate the request.
     * @private
     */
    _prepareDataProvider: function _prepareDataProvider(dp, config) {
      dp.endpoint = config.base;
      dp.path = config.path;
      dp.queryParams = config.search;
      console.log(config.data);
      dp.body = config.data;
      dp.method = config.method || dp.method;
    },

    /**
     * Gets the request when it is done and manage the response
     * @param {iron-request} resp - The `<iron-request>` element used to perform the request.
     * @param {RequestConfig} config - The configuration object passed to generate the request.
     * @private
     */
    _manageResponse: function _manageResponse(resp, config, isError) {
      var respData = resp.parseResponse();

      var callback = isError ? config.error : config.success;
      if (callback && typeof callback === 'function') {
        callback(respData, resp.xhr);
      }
      if (config.complete && typeof config.complete === 'function') {
        config.complete(resp.xhr);
      }
      (isError ? this._onRequestError : this._onRequestSuccess).call(this, resp.xhr, respData);
      this._onRequestComplete(resp.xhr);
    },

    /**
     * Use the DP to create the request with the given config
     * @param {cells-psv-generic-dp} dp - `<cellls-psv-generic-dp>` used to perform the request.
     * @param {RequestConfig} config - The configuration object passed to generate the request.
     * @return {iron-request} The final `<iron-request>` element that performs the request.
     * @private
     */
    _generateRequest: function _generateRequest(dp, config) {
      var request = dp.generateRequest();
      request.then(function (resp) {
        this._manageResponse(resp, config, false);
      }.bind(this), function (error) {
        var lastResponse = dp.lastRequest;
        this._manageResponse(lastResponse, config, true);
      }.bind(this));
      return request;
    },

    /**
     * Callback invoked on request returns with a success code (2XX)
     * @param {XMLHttpRequest} request - The XMLHttpRequest used to perform the request.
     * @param {object} data - The parsed data from the request.
     * @emits request-success
     * @private
     */
    _onRequestSuccess: function _onRequestSuccess(request, data) {
      this.fire('request-success', {
        resp: request,
        data: data
      });
    },

    /**
     * Callback invoked on request returns with a error code (4XX or 5XX). It verifies if it is a authorization
     * request error (403) and if so, emits the event
     * @param {XMLHttpRequest} request - The XMLHttpRequest used to perform the request.
     * @param {object} data - The parsed data from the request.
     * @emits request-error
     * @private
     */
    _onRequestError: function _onRequestError(request, data) {

      this.fire('request-error', {
        resp: request,
        data: data
      });
    },

    /**
     * Callback invoked on request completes
     * @param {XMLHttpRequest} request - The XMLHttpRequest used to perform the request.
     * @emits request-complete
     * @private
     */
    _onRequestComplete: function _onRequestComplete(request) {
      this.fire('request-complete', {
        resp: request
      });
    }
  });
})();
