# cells-myb-transaction-history

` This component shows the transaction history`

Example:
```html
<cells-myb-transaction-history></cells-myb-transaction-history>
```

```

## Styling

The following custom properties and mixins are available for styling:


| Custom property | Description     | Default        |
|:----------------|:----------------|:--------------:|
| --cells-myb-transaction-history  | empty mixin for your own styles on :host | {}             |
| --cells-myb-transaction-history-total-operaciones  | empty mixin for h4 .total-operaciones | {}             |
| --cells-myb-transaction-history-ui-header  | empty mixin  .transaction-history-header | {}             |
