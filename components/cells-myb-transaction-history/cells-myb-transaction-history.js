(function() {

  'use strict';
  /* global moment */

  Polymer({

    is: 'cells-myb-transaction-history',

    behaviors: [
      CellsBehaviors.i18nBehavior
    ],

    listeners: {
      'cells-myb-transaction-item-clicked': '_itemClicked'
    },

    properties: {
      /**
       * Date or dates to send
       * @type {String}
       */
      period: {
        type: String
      },

      /**
       * Has more results
       * @type {Boolean}
       */
      hasMoreResults: {
        type: Boolean
      },

      /**
       * Has transaction property
       * @type {Boolean}
       */
      _hasTransactions: {
        type: Boolean
      },

      /**
       * Transactions Array which contains all transactions
       * @type {Array}
       */
      transactions: {
        type: Array,
        observer: '_transactionsObserver'
      },

      /**
       * Total operations in the list
       * @type {Number}
       */
      totalElements: {
        type: Number,
        observer: '_formatAmount'
      },
      /**
       * Show skeletons when loadingState is true
       */
      loadingState: {
        type: Boolean,
        value: false
      },

      _isLoadingMore: {
        type: Boolean,
        value: true
      },

      _isLoadingZoneVisible: {
        type: Boolean,
        computed: '_getIfLoadingZoneIsVisible(hasMoreResults, _isLoadingMore)'
      },

      transactionDatePropName: String,

      groupByMonth: Boolean,

      reverseOrder: Boolean
    },
    showLoading: function() {
      this.set('loadingState', true);
    },
    hideLoading: function() {
      this.set('loadingState', false);
    },

    _getIfLoadingZoneIsVisible: function(hasMoreResults, _isLoadingMore) {
      return hasMoreResults && !_isLoadingMore;
    },

    _formatAmount: function() {
      this.set('totalElements',
        this.totalElements.toLocaleString(document.documentElement.lang));

      if (this.transactions && this.transactions.length) {
        this.hideLoading();
      }
    },
    /**
    * Obtains if begin date is the last date of the month
    * @param  {OBject} current Transaction
    * @return {Boolean} true if is the last day of the month
    */
    _isACompletedMonth: function(transaction) {
      /* istanbul ignore else */
      if (transaction.beginDate && transaction.endDate) {
        return moment(transaction.beginDate).endOf('month').format('DD') === moment(transaction.endDate).utc().format('DD');
      }
    },
    /**
     * set _hasTransactions porperty if there are any transactions
     * change localAmount if transaction _isRefund
     * @param {Array} group of transactions
     * @function
     */
    _transactionsObserver: function(transactions) {
      var _this = this;
      var isACompletedMonth;
      this._isLoadingMore = false;
      this._hasTransactions = this.transactions.length > 0;
      transactions.forEach(function(transaction, index, array) {
        isACompletedMonth = _this._isACompletedMonth(transaction);
        if (transaction._isRefund && transaction.localAmount.amount > 0) {
          transaction.localAmount.amount = transaction.localAmount.amount * -1;
        }
        if (_this.period === 'monthly') {
          transaction.hiddenRange = (index !== 0 || isACompletedMonth);
        } else {
          transaction.hiddenRange = isACompletedMonth;
        }
      });
      if (transactions.length) {
        if (_this.period === 'range') {
          this.async(function() {
            this.hideLoading();
          }.bind(this), 1000); // CSS animation + some delay
        } else {
          this.hideLoading();
        }
      }
    },

    /**
     * Event launched when list item is pressed
     * @event cells-myb-transaction-history-item-clicked
     */
    _itemClicked: function(item) {
      this.fire('cells-myb-transaction-history-item-clicked', item.detail);
    },

    /**
     * Function that loads more data when getting to the end of page
     * @function
     */
    loadMoreData: function() {
      /* istanbul ignore else */
      if (this.hasMoreResults) {
        this._isLoadingMore = true;
        this.fire('cells-myb-transaction-history-more');
      }
    }
  });

}());
