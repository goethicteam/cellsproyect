(function() {

  'use strict';

  Polymer({

    is: 'cells-welcome-middle-modal',

    behaviors: [
      CellsBehaviors.i18nBehavior
    ],

    properties: {
      /**
       * Slides.
       * @type {Array}
       * ```
       * [{
       *   animation: '<path-to-apng-image>',
       *   title: 'Slide title',
       *   text: 'Slide text',
       *   backgroundColor: 'Slide background color',
       *   backgroundImage: 'Slide background image',
       *   delay: 'Slide duration'
       * }]
       * ```
       */
      slides: {
        notify: true,
        type: Array,
        value: function() {
          return [
          ];
        }
      },
      /**
       * If true, makes modal visible
       * @type {Boolean}
       */
      open: {
        type: Boolean
      },
      /**
       * Shows an automatic slideshow.
       * @type {Boolean}
       */
      autoplay: Boolean,
      /**
      * Disable swipe in slides
      * @type {Boolean}
      */
      disabledSwipe: Boolean,
      /**
       * Show arrows in sides to swipe
       * @type {Boolean}
       */
      showArrows: Boolean,
      /**
       * Show register button
       * @type {Boolean}
       */
      showRegisterButton: Boolean,
       /**
       * Show legal terms link.
       * @type {Boolean}
       */
      showLegalTerms: Boolean
    },

    openModal: function() {
      this.open = true;
    },

    closeModal: function() {
      this.open = false;
    }
  });

}());
