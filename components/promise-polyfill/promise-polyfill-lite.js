"use strict";

if (!window.Promise) {
  window.Promise = MakePromise(Polymer.Base.async);
};