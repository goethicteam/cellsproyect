# cells-router-behavior

[![Certificated](https://img.shields.io/badge/certificated-yes-brightgreen.svg)](http://bbva-files.s3.amazonaws.com/cells/bbva-catalog/index.html)

 Use `Polymer.CellsRouterBehavior` to provide the component navigation methods.
 This behavior is implemented by all components which require urls to navigate.
 It exposes a function to resolve urls based on url IDs and parameters.
