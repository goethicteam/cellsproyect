(function() {
 /* global moment */
  'use strict';

  Polymer({

    is: 'cells-date-range',

    behaviors: [
      window.CellsBehaviors.i18nBehavior
    ],

    properties: {
      /**
       * Selected dates
       * @type {Object}
       */
      dateMultiple: {
        type: Object
      },

      /**
       * Format for each date in dateMultiple
       * @type {Object}
       */
      formatMultiple: {
        type: Object
      },

      /**
       * Character (or string) inserted between dates
       * @type {String}
       */
      separator: {
        type: String
      },

      /**
       * Change localization of date
       * @type {String}
       */
      locale: {
        type: String,
        value: document.documentElement.lang
      },

      _dates: {
        type: Array,
        computed: '_computedDates(dateMultiple, formatMultiple)'
      }
    },

    _computedDates: function(dates, formats) {
      var tmpDates = [];
      for (var date in dates) {
        /* istanbul ignore else  */
        if (moment(dates[date]).isValid()) {
          tmpDates.push({
            date: dates[date],
            format: formats[date]
          });
        }
      }
      return tmpDates;
    }
  });

}());
