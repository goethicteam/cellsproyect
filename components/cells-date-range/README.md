# cells-date-range

Your component description.

Example:
```html
<cells-date-range></cells-date-range>
```

```

## Styling

The following custom properties and mixins are available for styling:

| Custom property | Description     | Default        |
|:----------------|:----------------|:--------------:|
| --cells-date-range-scope      | scope description | default value  |
| --cells-date-range  | empty mixin     | {}             |
