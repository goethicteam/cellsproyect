(function(window) {
  'use strict';
  window.AppEnvironments = {
    'selected': {
      'app': 'default',
      'environment': 'composer-mock-local'
    },
    'availableApps': {
      'default': {
        'composer-mock-local': {
          'composerEndpoint': './composerMocks/',
          'coreCache': true
        }
      }
    }
  };
}(window));