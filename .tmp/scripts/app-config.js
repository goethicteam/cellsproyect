/* eslint-disable */(function(window) {
  'use strict';
  window.AppConfig = {
    'active': true,
    'target': '.tmp',
    'deployEndpoint': '',
    'i18nPath': 'locales/',
    'componentsPath': './components/',
    'composerEndpoint': './composerMocks/',
    'appId': '',
    'debug': true,
    'mocks': true,
    'coreCache': true,
    'routerLog': false,
    'cordovaScript': 'cordova.js',
    'prplLevel': 1,
    'initialBundle': [
      'login.json'
    ],
    'transpile': true,
    'transpileExclude': [
      'cells-rxjs',
      'polymer*',
      'moment',
      'd3',
      'bgadp*'
    ]
  };
}(window));