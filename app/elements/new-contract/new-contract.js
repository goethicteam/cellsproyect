(function() {

  'use strict';
  /* global moment */

  Polymer({

    is: 'new-contract',
    behaviors: [
      CellsBehaviors.i18nBehavior
    ],

    properties: {
      /*
       * Data show in the transaction list
       * @type {Object}
       */
      listData: {
        type: Object,
        observer: '_newListDataReceived'
      },
      /*
       * Data to show in the subheader
       * @type {Object}
       */
      subheaderData: {
        type: Object,
        observer: '_newSubheaderDataReceived'
      },
      /*
       * Controls witch part of the component is visible, date salector or results
       * @type {Boolean}
       */
      showResults: {
        type: String,
        value: 'hidden'
      },
      /*
       * To control subheader open status
       * @type {Object}
       */
      resumeOpened: {
        type: Boolean,
        value: false
      },
      /*
       * Start date for request
       * @type {String}
       */
      fromDate: {
        type: String
      },
      /*
       * End date for request
       * @type {String}
       */
      toDate: {
        type: String
      },
      /*
       * Start date for selection and reset control
       * @type {String}
       */
      tempFromDate: {
        type: String,
        observer: '_rangeChanged'
      },
      /*
       * End date for selection and reset control
       * @type {String}
       */
      tempToDate: {
        type: String,
        observer: '_rangeChanged'
      },
      /*
       * Minimum date for range calendar
       */
      minDate: {
        type: String,
        value: function() {
          return moment().subtract(3, 'y').startOf('year');
        }
      },
      /*
       * Maximum date for range calendar
       */
      maxDate: {
        type: String,
        value: function() {
          return new Date(new Date().setHours('23', '59', '59'));
        }
      },
      /*
       * Reflects the visible state for bottom loading animation
       */
      bottomLoading: {
        type: Boolean,
        value: false
      },
      noDataImage: {
        type: String,
        value: ''
      },
      noDataHeaderText: {
        type: String
      },
      noDataMainText: {
        type: String
      },
      noDataPeriodLinkText: {
        type: String
      },
      hasDataRange: {
        type: Boolean,
        value: true
      }
    },

    // attached: function() {
    //   this.$.period_balances_buttons.set('buttons', [
    //     {
    //       class: 'btn-clear btn--full btn--primary',
    //       text: 'cells-myb-period-balances-change-range-button-restore',
    //       disabled: true
    //     }, {
    //       class: 'btn-apply btn--full btn--primary',
    //       text: 'cells-myb-period-balances-change-range-button-apply',
    //       disabled: true
    //     }]
    //   );
    //   this.listen(this.$.period_balances_buttons, 'button-0-pressed', 'resetRange');
    //   this.listen(this.$.period_balances_buttons, 'button-1-pressed', 'applyRange');
    // },

    reset: function() {
      this.data = undefined;
      this.showResults = false;
      this.fromDate = undefined;
      this.toDate = undefined;
      this.$.subheader.hideLoading();
      this.$.list.hideLoading();
    },

    toggle: function() {
      console.log('toggle');
    this.$.men.togglePanel();
    },

    ready: function(){

      // class="card-item"
      // on-tap="_onItemTap"
      // heading-level="[[_listItemHeadingLevel]]"
      // heading="[[item.name]]"
      // balances='[[_getBalance(item)]]'
      // currency-code="[[_getCurrencyCode(item)]]"
      // local-currency="[[localCurrency]]"
      // min-width-for-row-layout="600"
      // product-iban="[[item.description.value]]"
      // image-src="[[item.imgSrc]]">

      var accountsMock = [
        {
          "id": "2002",
          "name": "Cuenta personal",
          "imgSrc": "../../images/cards/card0.png",
          "primaryAmount": {
            "amount": 1350000,
            "currency": "CLP",
            "label": "Available"
          }
        },
        {
          "id": "2002",
          "name": "Cuenta personal",
          "imgSrc": "../../images/cards/card0.png",
          "primaryAmount": {
            "amount": 1350000,
            "currency": "CLP",
            "label": "Available"
          }
        },
        {
          "id": "2004",
          "name": "Cuenta divisa",
          "description": {
            "value": "050400010100001606",
            "masked": true
          },
          "primaryAmount": {
            "amount": 1350000,
            "currency": "CLP",
            "label": "Available"
          },
          "scale": 0,
          "status": {
            "id": "ACTIVATED",
            "description": "ACTIVATED"
          }
        },
        {
          "id": "2001",
          "name": "Cuenta corriente lcred",
          "description": {
            "value": "050400010100001603",
            "masked": true
          },
          "primaryAmount": {
            "amount": 1900000,
            "currency": "CLP",
            "label": "Available"
          },
          "scale": 0,
          "status": {
            "id": "ACTIVATED",
            "description": "ACTIVATED"
          }
        }
      ];

      var accountsMock2=[
      {
        "id": "2006",
        "name": "Cuenta corriente",
        "description": {
          "value": "050400010100001608",
          "masked": true
        },
        "primaryAmount": {
          "amount": 1800000,
          "currency": "CLP",
          "label": "Available"
        },
        "scale": 0,
        "status": {
          "id": "ACTIVATED",
          "description": "ACTIVATED"
        }
      },
      {
        "id": "2006",
        "name": "Cuenta corriente",
        "description": {
          "value": "050400010100001608",
          "masked": true
        },
        "primaryAmount": {
          "amount": 1800000,
          "currency": "CLP",
          "label": "Available"
        },
        "scale": 0,
        "status": {
          "id": "ACTIVATED",
          "description": "ACTIVATED"
        }
      },
      {
        "id": "2005",
        "name": "Cuenta bono",
        "description": {
          "value": "050400010100001607",
          "masked": true
        },
        "primaryAmount": {
          "amount": 12000,
          "currency": "CLP",
          "label": "Available"
        },
        "scale": 0,
        "status": {
          "id": "ACTIVATED",
          "description": "ACTIVATED"
        }
      }];

      this.$.cards.items=accountsMock;
      this.$.cards2.items=accountsMock2

      this.$.contract.set('mainText',"hola dses");

    }



  });

}());
