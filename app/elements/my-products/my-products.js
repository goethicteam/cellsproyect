(function() {

  'use strict';
  /* global moment */

  Polymer({

    is: 'my-products',
    behaviors: [
      CellsBehaviors.i18nBehavior
    ],

    properties: {
      /*
       * Data show in the transaction list
       * @type {Object}
       */
      listData: {
        type: Object,
        observer: '_newListDataReceived'
      },
      /*
       * Data to show in the subheader
       * @type {Object}
       */
      subheaderData: {
        type: Object,
        observer: '_newSubheaderDataReceived'
      },
      /*
       * Controls witch part of the component is visible, date salector or results
       * @type {Boolean}
       */
      showResults: {
        type: String,
        value: 'hidden'
      },
      /*
       * To control subheader open status
       * @type {Object}
       */
      resumeOpened: {
        type: Boolean,
        value: false
      },
      /*
       * Start date for request
       * @type {String}
       */
      fromDate: {
        type: String
      },
      /*
       * End date for request
       * @type {String}
       */
      toDate: {
        type: String
      },
      /*
       * Start date for selection and reset control
       * @type {String}
       */
      tempFromDate: {
        type: String,
        observer: '_rangeChanged'
      },
      /*
       * End date for selection and reset control
       * @type {String}
       */
      tempToDate: {
        type: String,
        observer: '_rangeChanged'
      },
      /*
       * Minimum date for range calendar
       */
      minDate: {
        type: String,
        value: function() {
          return moment().subtract(3, 'y').startOf('year');
        }
      },
      /*
       * Maximum date for range calendar
       */
      maxDate: {
        type: String,
        value: function() {
          return new Date(new Date().setHours('23', '59', '59'));
        }
      },
      /*
       * Reflects the visible state for bottom loading animation
       */
      bottomLoading: {
        type: Boolean,
        value: false
      },
      noDataImage: {
        type: String,
        value: ''
      },
      noDataHeaderText: {
        type: String
      },
      noDataMainText: {
        type: String
      },
      noDataPeriodLinkText: {
        type: String
      },
      hasDataRange: {
        type: Boolean,
        value: true
      }
    },

    // attached: function() {
    //   this.$.period_balances_buttons.set('buttons', [
    //     {
    //       class: 'btn-clear btn--full btn--primary',
    //       text: 'cells-myb-period-balances-change-range-button-restore',
    //       disabled: true
    //     }, {
    //       class: 'btn-apply btn--full btn--primary',
    //       text: 'cells-myb-period-balances-change-range-button-apply',
    //       disabled: true
    //     }]
    //   );
    //   this.listen(this.$.period_balances_buttons, 'button-0-pressed', 'resetRange');
    //   this.listen(this.$.period_balances_buttons, 'button-1-pressed', 'applyRange');
    // },


    reset: function() {
      this.data = undefined;
      this.showResults = false;
      this.fromDate = undefined;
      this.toDate = undefined;
      this.$.subheader.hideLoading();
      this.$.list.hideLoading();
    },
    _openDialog: function(ev) {
      this.$.datepicker.open();
    },
    togglecollapse: function() {
  this.$.collapse.toggle();
},

      ready: function(){
        if (sessionStorage.getItem("accounts")){
          var myaccounts = JSON.parse(sessionStorage.getItem('accounts'));
          var mycards = JSON.parse(sessionStorage.getItem('cards'));
            this.listData=myaccounts[0];
        };
        var steps = this.$.closingSteps;
        steps.addEventListener('cells-accordion-step-opened', function(e) {
          e.target.iconRight = 'remove';
          for (var i = 0; i < steps.children.length; i++) {
            if (e.target !== steps.children[i]) {
              steps.children[i].close();
            }
          }
        });
        steps.addEventListener('cells-accordion-step-closed', function(e) {
          e.target.iconRight = 'coronita:expand';
        });
      },

      uuidv4: function() {
        return 'xxxxxxxxxxxxxxxxx'.replace(/[xy]/g, function(c) {
          var r = Math.random() * 16 | 0, v = c == 'x' ? r : (r & 0x3 | 0x8);
          return v.toString(16);
        });
      },

      sendTransfer: function(){
        //añadir validacion de campos
        console.log('sendTransfer: '+this.$.origen.value+this.$.destino.value+this.$.cantidad.value);
        var id = this.uuidv4();
        var data = {
          account: this.$.origen.value,
          to: this.$.destino.value,
          amount: this.$.cantidad.value,
          id: id
        };

        this.fire('send-transfer', {
          account: this.$.origen.value,
          to: this.$.destino.value,
          amount: this.$.cantidad.value,
          id: id
        });
        // this.$.mensaje.open=true;
      },
      sendTransferOk: function(account){
        var myaccounts = JSON.parse(sessionStorage.getItem('accounts'));
        this.$.mensaje.open=true;
        var finded=false;
        var i=0;
        while (!finded && i<myaccounts.length){
          if (myaccounts[i].id==account){
            this.listData=myaccounts[i];
            finded=true;
          }
          i++;
        }
        this.fire('update-accounts');
      },

      mio2: function(){
      var mockUser = {
        firstName: 'Iria Dominguez',
        avatar: {
          url: 'https://randomuser.me/api/portraits/women/3.jpg'
        }
      };

      var accountsMock = [
        {
          "id": "2002",
          "name": "Cuenta personal C",
          "description": {
            "value": "050400010100001604",
            "masked": true
          },
          "primaryAmount": {
            "amount": 70000,
            "currency": "EUR",
            "label": "disponible"
          },
          "scale": 0,
          "imgSrc": "../../images/cards/cardFrontDefault.png",
          "status": {
            "id": "ACTIVATED",
            "description": "ACTIVATED"
          }
        },
        {
          "id": "2003",
          "name": "Cuenta en dólares",
          "description": {
            "value": "050400010100001605",
            "masked": true
          },
          "primaryAmount": {
            "amount": 1700000,
            "currency": "CLP",
            "label": "Available"
          },
          "scale": 0,
          "imgSrc": "../../images/cards/cardFrontDefault.png",
          "status": {
            "id": "ACTIVATED",
            "description": "ACTIVATED"
          }
        },
        {
          "id": "2004",
          "name": "Cuenta divisa",
          "description": {
            "value": "050400010100001606",
            "masked": true
          },
          "primaryAmount": {
            "amount": 1350000,
            "currency": "CLP",
            "label": "Available"
          },
          "scale": 0,
          "imgSrc": "../../images/account/account.jpg",
          "status": {
            "id": "ACTIVATED",
            "description": "ACTIVATED"
          }
        },
        {
          "id": "2001",
          "name": "Cuenta corriente lcred",
          "description": {
            "value": "050400010100001603",
            "masked": true
          },
          "primaryAmount": {
            "amount": 1900000,
            "currency": "CLP",
            "label": "Available"
          },
          "scale": 0,
          "status": {
            "id": "ACTIVATED",
            "description": "ACTIVATED"
          }
        }
      ];

      this.$.productlist.items = accountsMock;
      this.fire("set-menu-user", mockUser);
  //     var data =[{'title': 'Cuenta *5894'}, {'title': 'Cuenta *4785'}, {'title': 'Cuenta *6734'}];
  // this.$.selectAc.options=data;
  // var submenu = [{
  //   'label': 'cells-navigation-bar-home',
  //   'icon': 'coronita:home',
  //   'id': 'home'
  // }, {
  //   'label': 'cells-navigation-bar-pay',
  //   'icon': 'coronita:wallet',
  //   'id': 'payments'
  // }, {
  //   'label': 'cells-navigation-bar-notifications',
  //   'icon': 'coronita:alarm',
  //   'id': 'notifications'
  // }, {
  //   'label': 'cells-navigation-bar-transfer',
  //   'icon': 'coronita:chargecard',
  //   'id': 'transfer'
  // }];
  // this.$.navigation.items=submenu;
    },
    _rangeChanged: function() {
      this.$.dateRangePicker.setDate();
      if (this.tempToDate || this.tempFromDate) {
        this.$.period_balances_buttons.changeStatus({buttonIndex: 0, buttonActive: true});
        if (this.tempToDate && this.tempFromDate) {
          this.$.period_balances_buttons.changeStatus({buttonIndex: 1, buttonActive: true});
        }
      }
    },

    mio: function(datos){
      console.log('mio');
      var data = {
        'data': [
          {
            'beginDate': '2017-12-31T00:00:00Z',
            'endDate': '2017-12-31T00:00:00Z',
            'sales': [
              {
                'totalElements': 19,
                'amount': 4925.67,
                'currency': 'EUR'
              }
            ],
            'refunds': [
              {
                'totalElements': 6,
                'amount': 1696.2,
                'currency': 'EUR'
              }
            ],
            'fees': [
              {
                'amount': 35.11,
                'currency': 'EUR'
              }
            ]
        },
        {
          'beginDate': '2017-12-30T00:00:00Z',
          'endDate': '2017-12-30T00:00:00Z',
          'sales': [
            {
              'totalElements': 13,
              'amount': 4008.05,
              'currency': 'EUR'
            }
          ],
          'refunds': [
            {
              'totalElements': 2,
              'amount': 413.77,
              'currency': 'EUR'
            }
          ],
          'fees': [
            {
              'amount': 105.38,
              'currency': 'EUR'
            }
          ]
        }]
      };



      var subheaderData = {
        sectionBalance: {
          'date': {
            'start': '2016-01-01T00:00:00Z',
            'end': '2016-12-31T23:59:59Z'
          },
          'format': {
            'start': 'MMM',
            'end': 'MMM YYYY'
          },
          'separator': '-'
        },
        dateRange: {
          'totalElements': 3575,
          'amount': 14365.26,
          'currency': 'EUR'
        },
        sectionStats: {
          "statPercentValue": "+7.02",
          "fromDateText": "año pasado",
          "stats": [{
            "total": "3.815",
            "amount": 972759.82,
            "currency": "EUR",
            "literal": "cells-myb-subheader-statics-total-sales",
            "class": "subheader-sales"
          }, {
            "total": "1.219",
            "amount": -308655.06,
            "currency": "EUR",
            "literal": "cells-myb-subheader-statics-total-refunds",
            "class": "subheader-refund"
          }]
        }
      };
      this.listData=data;
      this.subheaderData=subheaderData;
      this.$.cards.items=datos;



      var mockLinks = [{
        label: 'Notificaciones',
        icon: 'coronita:alarm',
        link: '#notifications',
        action: 'notifications-event',
        count: 10,
        status: {
          showNoLogged: false,
          showLogged: true
        }
      }, {
        label: 'Posicion global',
        icon: 'coronita:titleview',
        link: '#routerExample',
        status: {
          showNoLogged: false,
          showLogged: true
        },
      }, {
        label: 'Productos para mí',
        icon: 'coronita:supermarket',
        link: '#/products',
        count: 2,
        status: {
          showNoLogged: false,
          showLogged: true
        }
      }, {
        label: 'Ajustes',
        icon: 'coronita:settings',
        action: 'settings-event',
        status: {
          showNoLogged: true,
          showLogged: true
        }
      }, {
        label: 'Cajeros y oficinas',
        icon: 'coronita:mapa',
        link: '#atms',
        status: {
          showNoLogged: true,
          showLogged: true
        }
      }, {
        label: 'Aplicaciones',
        icon: 'coronita:titleview',
        link: '#apps',
        action: 'apps-event',
        status: {
          showNoLogged: false,
          showLogged: true
        }
      }, {
        label: 'Acerca de',
        icon: 'coronita:info',
        link: '#about',
        action: 'about-event',
        status: {
          showNoLogged: true,
          showLogged: true
        }
      },
      {
        label: 'login',
        icon: 'coronita:info',
        action: 'login-event',
        status: {
          showNoLogged: true,
          showLogged: false
        }
      }];

      var mockUser = {
        firstName: 'Iria Dominguez',
        avatar: {
          url: 'https://randomuser.me/api/portraits/women/3.jpg'
        }
      };
      this.$.menuSide.userLogged = true;
      this.$.menuSide.availableSections = mockLinks;
      this.$.menuSide.user = mockUser;


    },

    loadMoreData: function() {
      this.$.list.loadMoreData();
    },

    resetRange: function() {
      this.fromDate = undefined;
      this.toDate = undefined;
      this.$.dateRangePicker.reset();
      this.sendRangePicked();
      this.$.period_balances_buttons.changeStatus({buttonIndex: 0, buttonActive: false});
      this.$.period_balances_buttons.changeStatus({buttonIndex: 1, buttonActive: false});
      this.fire('reset-range-picked');
    },

    applyRange: function() {
      var isValidateRange = this._validateRange();
      var toDate = (this.toDate ? this.toDate.toString() : '');
      var fromDate = (this.fromDate ? this.fromDate.toString() : '');

      if (this.tempToDate && this.tempFromDate && isValidateRange) {
        this.viewResults();

        if (moment(this.tempToDate.toString()).utc().format('DD-MM-YYYY') !== moment(toDate).utc().format('DD-MM-YYYY') ||
            moment(this.tempFromDate.toString()).utc().format('DD-MM-YYYY') !== moment(fromDate).utc().format('DD-MM-YYYY')) {
          this.hasDataRange = true;
          this.saveDates();
          this.sendRangePicked();
          this.$.subheader.showLoading();
          this.$.list.showLoading();
          this.fire('apply-range-picked');
        }
      }
    },

    saveDates: function() {
      this.set('fromDate', this.tempFromDate);//'2016-11-27T00:00:00Z');//
      this.set('toDate', this.tempToDate);//'2016-12-31T23:59:59Z');//
    },

    sendRangePicked: function() {
      this.fire('new-range-picked', {
        period: 'range',
        from: this.fromDate,
        to: this.toDate
      });
    },

    _newListDataReceived: function(newVal) {
      this._setAccounts();
      console.log(newVal);
      var customer = JSON.parse(sessionStorage.getItem('customer'));

      var balance;
      if(newVal.type=="ACCOUNT"){
          this.$.sumary.imgSrc="../../images/account/account.jpg";
          balance=newVal.balance;
          this.$.sumary.heading=newVal.alias;
      }
      else{
        this.$.sumary.imgSrc="../../images/cards/card1.png";
        balance=newVal.availableBalance;
        this.$.sumary.heading=newVal.description;
      }
      this.$.sumary.localCurrency="EUR";
      this.$.sumary.progressValue=20;

      var optionalItems=[{
        key: "Tirular: "+customer.firstName+" "+customer.surname+" "+customer.secondSurname,
        value: ""
      }, {
        key: "IBAN: "+ newVal.id,
        value: ""
      },
      {
        key: "Divisa: EUR",
        value: ""
      },{
        localCurrency: "EUR",
        key: "Gastos del mes",
        value: [{
          amount: 3000.20,
          currencyCode: "EUR",
          class: "value-italic"}]
        },
        {
          localCurrency: "EUR",
          key: "Saldo disponible",
          value: [{
            amount: balance,
            currencyCode: "EUR",
            class: "value-italic"}]
          }];

      this.$.sumary.optionalItems=optionalItems;
      this.$.sumary.onData();

      this._setTransaccions(newVal);
      this.setTranfer(newVal.id);
    },

    setTranfer: function(id){
      this.$.origen.value=id;
      this.$.destino.value="";
      this.$.cantidad.value="";
    },

    fastTransfer: function(e){
      this.$.destino.value="ES0182061600000000500000000000010124";
    },

    _setAccounts: function() {
      var myaccounts = JSON.parse(sessionStorage.getItem('accounts'));
      var mycards = JSON.parse(sessionStorage.getItem('cards'));
            var products = [];
            console.log(myaccounts);

            for (var i in myaccounts){
              console.log(myaccounts[i]);
              var product =
                {
                  "id": myaccounts[i].number,
                  "name": myaccounts[i].alias,
                  "description": {
                    "value": myaccounts[i].id,
                    "masked": true
                  },
                  "primaryAmount": {
                    "amount": myaccounts[i].balance,
                    "currency": myaccounts[i].currency,
                    "label": "saldo"
                  },
                  "scale": 0,
                  "imgSrc": "../../images/account/account.jpg",
                  "status": {
                    "id": "ACTIVATED",
                    "description": "ACTIVATED"
                  }
                }
                products.push(product);
            };

            for (var i in mycards){
              console.log(mycards[i]);
              var product =
                {
                  "id": mycards[i].number,
                  "name": mycards[i].description,
                  "description": {
                    "value": mycards[i].id,
                    "masked": true
                  },
                  "primaryAmount": {
                    "amount": mycards[i].availableBalance,
                    "currency": mycards[i].currency,
                    "label": "saldo"
                  },
                  "scale": 0,
                  "imgSrc": "../../images/cards/card"+i+".png",
                  "status": {
                    "id": "ACTIVATED",
                    "description": "ACTIVATED"
                  }
                }
                products.push(product);
            };
              this.$.productlist.items = products;

            },

    toggle: function() {
      console.log('toggle');
    this.$.men.togglePanel();
  },

    _setTransaccions: function(newVal){
      var transaccions =[];
      if(newVal.type=="ACCOUNT"){
        var trans = newVal.accountTransactions;
        for (var i in trans){
          var transaction = {
         'name': trans[i].description,
         'description': {
           'content': trans[i].category.name,
           'date': trans[i].operationDate
         },
         'primaryAmount': {
           'amount': trans[i].amount,
           'currency': trans[i].currency
          }
        };
        transaccions.push(transaction);
      };

      }
      else{
        var trans = newVal.cardTransactions;
        for (var i in trans){
          var transaction = {
         'name': trans[i].shop.name,
         'description': {
           'content': trans[i].subCategory.name,
           'date': trans[i].date
         },
         'primaryAmount': {
           'amount': trans[i].amount,
           'currency': trans[i].currency
          }
        };
        transaccions.push(transaction);
      };

      };

      this.$.transactionsList0.transactions=transaccions;
      this.$.transactionsList0.onData();
    },

    addTransactions: function(data) {
      var actualTransactions = this.$.list.transactions;
      var newTransactions = actualTransactions.concat(data);
      this.$.list.set('transactions', newTransactions);
    },

    _newSubheaderDataReceived: function(newVal) {
      if (newVal && newVal.operationStats) {
        this.async(function() {
          this.hasDataRange = !!newVal.operationStats.totalOperations;
          this.$.list.set('totalElements', newVal.operationStats.totalOperations);
          this.$.subheader.set('mainAmount', newVal.mainAmount);
          this.$.subheader.set('dateRange', newVal.dateRange);
          this.$.subheader.set('operationStats', newVal.operationStats);
          this.$.subheader.hideLoading();
          this.subheaderData = undefined;
        }.bind(this), 1000); // CSS animation + some delay
      }
    },

    load: function(data){
      console.log("load");
      var myaccounts = JSON.parse(sessionStorage.getItem('accounts'));
      var mycards = JSON.parse(sessionStorage.getItem('cards'));
      for(var i in myaccounts){
        mycards.push(myaccounts[i]);
      }
      var finded=false;
      var i=0;
      while (!finded){
        if (mycards[i].number==data.productId){
          this.listData=mycards[i];
          finded=true;
        }
        i++;
      }

    },

    showLoadings: function() {
      this.$.subheader.showLoading();
      this.$.list.showLoading();
      this.hasDataRange = true;
    },

    hideLoadings: function() {
      this.$.subheader.hideLoading();
      this.$.list.hideLoading();
    },

    hideListLoading: function() {
      this.$.list.hideLoading();
    },

    hideSubheaderLoading: function() {
      this.$.subheader.hideLoading();
    },

    viewResults: function() {
      this.set('showResults', 'visible');
      this.fire('showing-results');
    },

    viewPicker: function() {
      this.$.subheader.closeSeeMore();
      this.set('showResults', 'hidden');
      this.fire('choose-new-range-clicked');
    },

    showNoData: function() {
      this.set('noData', true);
    },

    _showErrorMessage: function(message) {
      this.fire('wrong-date-range', {
        code: '422',
        icon: 'coronita:error',
        noBackdrop: false,
        content: message,
        type: 'error',
        firstButtonLabel: this.getTranslation('cells-myb-period-balances-wrong-period-button-retry')
      });
    },

    _validateRange: function() {
      var message;
      var format = 'YYYY-MM-DD';
      var tempFromDateFormatted = moment(this.tempFromDate).utc().format(format);
      var tempToDateFormatted = moment(this.tempToDate).utc().format(format);
      var minDateFormatted = moment(this.minDate).utc().format(format);
      var maxDateFormatted = moment(this.maxDate).utc().add(2, 'h').format(format);
      var MAX_MONTHS_RANGE = 6;
      if (moment(tempToDateFormatted).isAfter(maxDateFormatted)) {
        message = this.getTranslation('cells-myb-period-balances-error-to-greater-than-maximum') + moment(this.maxDate).format('DD MMMM YYYY') + '.';
        this._showErrorMessage(message);
        return false;
      } else if (moment(tempFromDateFormatted).isAfter(tempToDateFormatted)) {
        message = this.getTranslation('cells-myb-period-balances-error-to-minor-than-from');
        this._showErrorMessage(message);
        return false;
      } else if (moment(tempFromDateFormatted).isBefore(minDateFormatted)) {
        message = this.getTranslation('cells-myb-period-balances-error-from-smaller-than-minimum') +  moment(this.minDate).format('DD MMMM YYYY') + '.';
        this._showErrorMessage(message);
        return false;
      } else if (moment(this.tempToDate).diff(moment(this.tempFromDate), 'months') >= MAX_MONTHS_RANGE) {
       message = this.getTranslation('cells-myb-period-balances-max-range-part-1') + ' ' + MAX_MONTHS_RANGE + ' ' + this.getTranslation('cells-myb-period-balances-max-range-part-2');
       this._showErrorMessage(message);
       return false;
      } else {
        return true;
      }
    },

    closeSubheader: function() {
      var _operationSubheaderDom =
      (this.shadowRoot)
      ? this.shadowRoot.querySelector('cells-myb-subheader')
      : this.querySelector('cells-myb-subheader');

      if (_operationSubheaderDom && _operationSubheaderDom.attributes['is-opened-see-more']) {
        _operationSubheaderDom.removeAttribute('is-opened-see-more');
      }
    }

  });

}());
