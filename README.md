# goepro

## Getting started

As you need **CELLS CLI** to build, serve, publish, and so on, your application,
you must ensure that you have NodeJS installed. If not, follow the steps below:

1) Get **[nvm](https://github.com/creationix/nvm)**. Follow the instructions in
  the given link:

2) Using **nvm**, install **node** (We recomend *node v7* 20/sept/2017):


    nvm install node 7

3) Install the latest version of CELLS command line interface (CELLS CLI) using npm:



    npm install -g cells-cli

4) Run the application on the root of it:


    cd path/to/goepro
    cells serve


**NOTE:** The **first time** you run CELLS CLI command it will take some time because of
its lazy dependencies installation.
